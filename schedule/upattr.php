<?php
/* upattr.php --- 
 * 
 * Author: czt
 * Created: 2010-01-21 13:18:38 +0800
 * Version: $Id: upattr.php 143M 2010-01-24 15:01:20Z (本地) $
 */

require (dirname(__FILE__) . "/sphinxapi.php" );
require (dirname(__FILE__) . "/sched_config.php");

///// TODO multi index support
$idx_id = 2;

$linkID = @mysql_connect($db_host, $db_user, $db_pass)
    or die("Could not connect to MySQL sever!");
mysql_select_db($db_name, $linkID);
printf("[TRACE] db connect success!\n");

$index = "spider";
$cl = new SphinxClient ();
$cl->setServer($sph_host, $sph_port);

if($result = mysql_query("SELECT delta_current_mtime,delta_max_mtime FROM sph_counter WHERE counter_id = $idx_id")){
    printf("[TRACE] step 1!\n");
    $a = mysql_num_rows($result);
    printf("Number of records :%d\n", mysql_num_rows($result));
    $cols = mysql_num_fields($result);
    $row = mysql_fetch_row($result);
    mysql_free_result($result);
}
printf("[TRACE] rows = %s,%s\n",$row[0],$row[1]);

$sql = "SELECT id FROM spider_items WHERE nid >=row[0] AND nid <= $row[1]";
if ($result_s = mysql_query($sql)) {
    printf("[TRACE] step 2!\n");
    $a = mysql_num_rows($result_s);
    printf("Number of records :%d\n", mysql_num_rows($result_s));
    // TODO  这个循环可以合并，减少UpdateAttributes方法的次数
    while ($row_s = mysql_fetch_row($result_s)){
        printf("[TRACE] row id = %s\n",$row_s[0]);
        $res = $cl->UpdateAttributes ($index, array("sched_deleted"), array($row_s[0] => array(1)), false);
        printf("[TRACE] res = %d\n", $res);
        if ($res === -1) {
            print "ID {$row_s[0]}: UpdateAttributes error!\n" . $cl->GetLastError();
        }
    }
    mysql_free_result($result_s);
}

printf("[TRACE] step 2!\n");
mysql_close($linkID);

// flush attribute to disk
$status = $cl->FlushAttributes();
if ($status < 0) {
    echo "Flush error: " . $cl->GetLastError() . "\n";
}
$cl->Close();

?>
