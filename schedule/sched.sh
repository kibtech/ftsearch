#!/bin/bash

# sched.sh --- 
# 
# Created: 2010-01-08 10:39:25 +0800
# Version: $Id: sched.sh 142 2010-01-21 08:54:55Z liuguangzhao $
# 

# set -x

# CONFIGFILE=$(dirname "$0")/config
# echo "CONFIGFILE is : $CONFIGFILE"
# sphinxBinDir:/home/ftsearch/local/sphinx/bin
# field separator, default is :, you can use blank space or 
# other character, if you have more than one blak space in 
# input line then use awk utility and not the cut :) 
# FS=":"
# cat $CONFIGFILE | while read LINE
# do
# 	KEY=$(echo $LINE|cut -d$FS -f1)
# 	if [ "$KEY" == "thresholdTime" ]
# 	then
# 		thresholdTime=$(echo $LINE|cut -d$FS -f2)
# 		echo "[Debug] thresholdTime is $thresholdTime"
# 	fi
# 	if [ "$KEY" == "sphinxPath" ]
# 	then
# 		sphinxPath=$(echo $LINE|cut -d$FS -f2)
# 		echo "[Debug] sphinxPath is $sphinxPath"		
# 	fi
# done

## Preconfigurated variables
baseDirForScriptSelf=$(cd "$(dirname "$0")"; pwd)
baseDirForScriptSelf=`readlink -f $baseDirForScriptSelf`  # to absolute dirname

## Variable set section
thresholdTime=30
#sphinxPath=/home/ftsearch      #  /home/ftsearch/local/sphinx
sphinxPath=$HOME/local/sphinx
FilePIDPath=$baseDirForScriptSelf/states/sched.pid     #  "123456"
#sphinxConfFile=$baseDirForScriptSelf/sphinx.conf
sphinxConfFile=$sphinxPath/etc/csft.conf
MYSQL_ARGS=" -h127.0.0.1 -uroot -A drupal "
# MYSQL="/home/ftsearch/local/mysql/bin/mysql -h127.0.0.1 -uftsearch -pfts123 -A ftsearch"
MYSQL_EXE=/usr/bin/mysql
if [ ! -f $MYSQL_EXE ] ; then
    MYSQL_EXE=
    GUESS_PATHS="/usr/local/mysql-5.0.90/bin/mysql"
    for ep in $GUESS_PATHS
    do
        if [ -f $ep ] ; then
            MYSQL_EXE=$ep
        fi
    done
    if [ ! -f "$MYSQL_EXE" ] ; then
        echo "Can not guess mysql path IN ($GUESS_PATHS)";
        exit;
    else
        echo "Guessed mysql path: $MYSQL_EXE.";        
    fi
fi

#notes: must have ""
# searchdPID=29262		# We have no idea about what is the pid of the searchd, enter here manually 
############################################################################

PID=$$ 
if [ -f $FilePIDPath ] ## Judge if file exists
then
	CUR_PID=`cat $FilePIDPath`
#	echo "[Debug] The PID read from $FilePIDPath is $CUR_PID"
	TEST=` ps ax | grep $CUR_PID | grep -v grep `; 
	if [[ $TEST != "" ]]
	then
		echo "[INFO] Schedule script is running, exit..."
		exit;
	else
		echo "[INFO] Schedule script is not runing, begin schedule..."
	fi
else
	echo "[INFO] Schedule script is not runing, begin schedule..."
fi
echo $PID > $FilePIDPath

## shell signal handler
function cleanup()
{
    echo "Remove shell pid file..."
    rm -vf $FilePIDPath
    echo "Exit."
    exit;
}

trap cleanup INT


## Shell entry point
PHP=/usr/local/php.5.2.10/bin/php
INDEXER_NAME=$sphinxPath/bin/indexer
SEARCHD_NAME=$sphinxPath/bin/searchd
PROG_NAME=$0  ##the script program name, no change 

## check if indexer is running
TARGET_NAME=indexer
TEST=` ps ax | grep "$TARGET_NAME" | grep -v "$PROG_NAME" | grep -v grep `; 
if [ x"$TEST" != x"" ] ; then
    ## the indexer is running, exit
    echo "[INFO] Indexer is running, exit..."
    exit ;
fi

############################################################################
while [ true ]
do
    beginTime=`date +%s`

    ## check if searchd is running
    TARGET_NAME=searchd 
    SEARCHD_STATUS=""
    INDEXER_ROTATE=""

    TEST=` ps ax | grep "$TARGET_NAME" | grep -v "$PROG_NAME" | grep -v grep `;  
    if [ x"$TEST" != x"" ] ; then
        echo   "[INFO] $TARGET_NAME found, using \"--rotate\" mode..."
	    SEARCHD_STATUS="RUNNING"
        INDEXER_ROTATE=" --rotate "
    else  
	    SEARCHD_STATUS="NOT_RUNNING"
        echo "[INFO] $TARGET_NAME not found..."  
    fi  

    echo "===================== Indexing delta ..."
	$sphinxPath/bin/indexer --config $sphinxConfFile $INDEXER_ROTATE  spider_delta
	# php index_d.php			#Run the sync php script 
    $PHP -d memory_limit=256M $baseDirForScriptSelf/upattr.php
	# kill -s SIGHUP $searchdPID	#give a SIGHUP signal to searchd to tell it to write data back to disk
    # flushing updated attr in main index, is this needed???
    $baseDirForScriptSelf/sphinxctl.sh flush
    echo "===================== Index delta Done."

########################################################################
## check to do merge work

    # temporary merge per hour for test
    TIMESTAMP="merged_`date +%Y%m%d%H`"    ## We run the merge work for only once everyday/everyhour/everyminute
    TMP_FILENAME=$baseDirForScriptSelf/states/$TIMESTAMP    ##TMP_FILENAME=/tmp/${TIMESTAMP}
    echo "[Debug] TMP_FILENAME is $TMP_FILENAME"
    if [ -f $TMP_FILENAME ] #[ check /tmp/mergerLog{todayDate} ] 
    then	## file exists
	    echo "[INFO]: Merge work DONE today, skip..."
    else
        echo "Changing main/delta progress status value..."
        $MYSQL_EXE $MYSQL_ARGS  < "$baseDirForScriptSelf/merge_counter_updater.sql"
        # assert mysql ok.

	    echo "============== Merging delta -> main ..."
	    $sphinxPath/bin/indexer --config $sphinxConfFile $INDEXER_ROTATE --merge spider spider_delta --merge-dst-range sched_deleted 0 0 #delete 0 0
	    echo "============== Merge delta -> main Done."
        touch $TMP_FILENAME     #put a flag here to denote we have merged the index today
        # TODO how to do when index merge faild
    fi
########################################################################

    endTime=`date +%s`
    deltaTime=`expr $endTime - $beginTime` ## count in seconds
    echo "[Debug] beginTime is: $beginTime"
    echo "[Debug] endTime is: $endTime"

#deltaTime=`expr $beginTime - $endTime` ## count in seconds
    echo "[Debug] Time elapsed: $deltaTime"

    if [ "$deltaTime" -gt "$thresholdTime" ] ; then
        continue
    else
        echo "[INFO]: Waiting `expr $thresholdTime - $deltaTime` sec (Now is `date`)..."
        sleep `expr $thresholdTime - $deltaTime`
    fi
done ##while
########################################################################################

