#!/bin/sh

# sphinxctl.sh --- 
# 
# Author: liuguangzhao
# Created: 2010-01-21 15:21:58 +0800
# Version: $Id: sphinxctl.sh 142 2010-01-21 08:54:55Z liuguangzhao $
# 

# Usage: 
#    sphinxctl.sh start|stop|restart|flush
#

SEARCHD=searchd

MYDIR=`readlink -f $0 | xargs dirname`
SPHDIR=$HOME/local/sphinx
SPHCNF=$SPHDIR/etc/csft.conf

function usage()
{
    echo "Usage:"
    echo "    /path/to/sphinxctl.sh <start|stop|restart|flush|status|help>"
    exit;
}

ARGC=$#
if [ $ARGC -ne 1 ] ; then
    echo "Params error."
    usage;
fi

case $1 in
    "start")
        echo "Starting searchd ...";
        $SEARCHD -c $SPHCNF
        ;;
    "stop")
        echo "stoping searchd ...";
        $SEARCHD -c $SPHCNF --stop
        ;;
    "restart")
        echo "Restarting searchd ...";
        $SEARCHD -c $SPHCNF --stopwait
        $SEARCHD -c $SPHCNF
        ;;
    "status")
        $SEARCHD -c $SPHCNF --status
        ;;
    "flush")
        # no use, must restart searchd for this problem
        echo "Flushing searchd ...";
        PIDFILE=`cat $SPHCNF | grep pid_file | awk -F\# '{print $1}' | grep -v \# | grep -v grep | awk -F= '{print $2}' `
        echo $PIDFILE
        SPHPID=""
        if [ -f $PIDFILE ] ; then
            SPHPID=`cat $PIDFILE`
            # echo $SPHPID
            kill -s HUP $SPHPID
        else
            echo "Maybe searchd is not running."
        fi
        ;;
    "help")
        usage;
        ;;
    *)
        echo "Unknown params: $@";
        usage;
        ;;
esac

echo "$0 done."
