-- merge_counter_updater.sql --- 
--
-- Author: liuguangzhao
-- Created: 2010-01-08 10:40:47 +0800
-- Version: $Id: merge_counter_updater.sql 128 2010-01-20 06:52:50Z liuguangzhao $
--
-- used before index merge step
-- counter_id, max_doc_id, delta_doc_id

USE drupal;

REPLACE INTO sph_counter (counter_id,master_max_mtime, delta_current_mtime, delta_max_mtime)    \
         SELECT 2,                                                                              \
         (SELECT delta_max_mtime FROM sph_counter WHERE counter_id=2) AS mmid,                  \
         (SELECT delta_max_mtime FROM sph_counter WHERE counter_id=2) AS dcid,                  \
         (SELECT delta_max_mtime FROM sph_counter WHERE counter_id=2) AS dmid                   ;
--         WHERE counter_id=1;


SELECT * FROM sph_counter;

-- REPLACE INTO sph_counter (counter_id, delta_doc_id) SELECT 1, MAX(id) FROM wupin;

-- After UpdateAttributes call, run this sql:
-- REPLACE INTO sph_counter (counter_id,master_max_mtime, delta_current_mtime, delta_max_mtime)    \
--         SELECT 1,                                                                              \
--         (SELECT master_max_mtime FROM sph_counter WHERE counter_id=1) AS mmid,                  \
--         (SELECT delta_max_mtime FROM sph_counter WHERE counter_id=1) AS dcid,                  \
--         (SELECT delta_max_mtime FROM sph_counter WHERE counter_id=1) AS dmid                   \
--         WHERE counter_id=1;

