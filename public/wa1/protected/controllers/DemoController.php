<?php

require_once(Yii::app()->getBasePath() . '/vendors/sphinxapi.php');

class DemoController extends Controller
{
	private $sph_host = "202.108.15.6";
	private $sph_port = 9312;
	private $sph_idxs = array();

	/**
	 * Declares class-based actions.
	 */
	// public function actions()
	// {
	// 	return array(
	// 		// captcha action renders the CAPTCHA image displayed on the contact page
	// 		'captcha'=>array(
	// 			'class'=>'CCaptchaAction',
	// 			'backColor'=>0xFFFFFF,
	// 		),
	// 		// page action renders "static" pages stored under 'protected/views/site/pages'
	// 		// They can be accessed via: index.php?r=site/page&view=FileName
	// 		'page'=>array(
	// 			'class'=>'CViewAction',
	// 		),
	// 	);
	// }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        // print_r(Yii::app());
        // Yii::app()->name = 'aaaaaaaa'; // 设置页面标题
        $this->layout = 'mylayout1';   // 指定页面的layout，否则为main.php/main.html
        // 如果找不到指定的layout并且不为空，则不使用layout
        // 如果layout为空，则使用默认的main.php layout。
		$this->render('index', array('va'=> 1, 'vb'=> 2));
        // Yii::app()->smarty->display('index');
	}

	public function actionSearch($q)
	{
        // $request = $this->getRequest();
        // $params = $request->getParams();
	    // $params = Yii::app()->getParams();		

	    $params = $_GET;
        // print_r($params);
	    if ($params['q'] == '') {
	        $this->render('index');
            return;
	    }	    

        $btime = microtime(true);

        $sphc = new SphinxClient();
        // $sphc->SetServer('127.0.0.1', 9312);
	    $sphc->SetServer($this->sph_host, $this->sph_port);
                 
        $rc = $sphc->open();
        if (!$rc) {
            echo "open sphinx search faild.\n";
            echo $sphc->GetLastError() . "\n";
            exit;
        }

        // pager
        $offset_start = $params['page'] * 20;
        $offset_end = ($params['page'] + 1) * 20;

        $sphc->SetFieldWeights(array('title' => 3));
        // $sphc->SetIndexWeights(array('idx_cms_drupal_delta' => 5));
        // $sphc->setFilter('deleted', array(1), true);
        // $sphc->setFilter('sched_deleted', array(1), true);
        $sphc->SetLimits($offset_start, 20, 1000);
        $res = $sphc->Query($params['q']);
            
	    if (!$res) {
            echo $sphc->getLastError();
            var_dump($res);
	    }     
        // print_r($res);
        $sphc->Close();

        $words = array();
        if (count($res['words']) > 0)
            foreach ($res['words'] as $word => $vs) {
                $vs['word'] = $word;
                $words[] = $vs;
            }
        
        $ids = array();
        // $docs = array_keys($res['matches']); // the content
        $docs = array();
        $seq = 0;
        if (count($res['matches']) > 0)
            foreach ($res['matches'] as $id => $mat) {
                $docs[$seq] = strip_tags($mat['attrs']['body']);
                $ids[$id] = $seq;
                $seq ++;
            }
        // $excerpt_docs = $sphc->BuildExcerpts($docs, "qtchina_cms", $words);
	    if (is_array($res['words']))
            $word_list = implode(' ', array_keys($res['words']));
        else 
	        $word_list = '';
        // print_r($word_list);
        $opts = array('around' => 12);
        $excerpt_docs = $sphc->BuildExcerpts($docs, "qtchina_cms", $word_list, $opts);
        // print_r($excerpt_docs);
        // print_r($docs);
	    $sphc->Close();

        $matches = array();
	    if (count($res['matches']) > 0)
            foreach ($res['matches'] as $id => $mat) {
                $mat['attrs']['id'] = $id;
                $mat['attrs']['body'] = strip_tags($mat['attrs']['body']);
                // $mat['attrs']['body_digest'] = mb_substr($mat['attrs']['body'], 0, 200, "utf-8");
                // $mat['attrs']['excerpt'] = $excerpt_docs[$ids[$id]];
                $mat['attrs']['body_digest'] = mb_substr($excerpt_docs[$ids[$id]], 0, 200, "utf-8");
                
                if ($id >= 3000) {
                    $mat['attrs']['no_out_link'] = 1;
                }
		
                foreach (array_keys($res['words']) as $seq => $word) {
                    $mat['attrs']['title']= str_replace($word, "<FONT COLOR=red><B>{$word}</B></FONT>", $mat['attrs']['title']);
                }
                $matches[] = $mat['attrs'];
            }

        $etime = microtime(true);
                 
        // print_r($matches);
        $cost_time = $res['time'] . ", " . ($etime - $btime);
        $total_found = $res['total_found'];
                 
        // $smarty = Zend_Registry::get('smarty');
        // $smarty = $this->getViewRenderer();
        // $smarty->assign('matches', $matches);
        // $smarty->assign('words', $words);
        // $smarty->assign('word_list', $word_list);
        // $smarty->assign('cost_time', $cost_time);
        // $smarty->assign('total_found', $total_found);

        $arr = array('matches'=> $matches,
                     'words'=> $words,
                     'word_list'=> $word_list,
                     'cost_time'=> $cost_time,
                     'total_found'=> $total_found);
        $arr1 = $this->_calc_pager($total_found);
	    $arr2 = array_merge($arr, $arr1);
	    
        // $this->indexAction();

	    // echo count($arr) . count($arr1) . count($arr2);				
	    // $this->layout = 'empty';
	    // $this->render('index_p');
	    // $this->layout = '';
        $this->render('index', $arr2);
	    // $this->render('index', array('va'=> 1, 'vb'=> 2));
	}

    private function _calc_pager($total_found, $page_size = 20)
    {
        // $request = $this->getRequest();
        // $params = $request->getParams();
	    $params = $_GET;

        $max_page_no = intval($total_found / 20);
        $curr_page_no = $params['page'];
        $prev_page_no = 0;
        $next_page_no = 0;
        if ($max_page_no == 0) {
            $prev_page_no = 0;
            $next_page_no = 0;
        } else {
            if ($curr_page_no == 0) {
                $prev_page_no = 0;
                $next_page_no = $curr_page_no + 1;
            } else if ($curr_page_no == $max_page_no) {
                $prev_page_no = $curr_page_no - 1;
                $next_page_no = $max_page_no;
            } else {
                $prev_page_no = $curr_page_no - 1;
                $next_page_no = $curr_page_no + 1;
            }                
        }

        // $smarty = Zend_Registry::get('smarty');
        // $smarty->assign('max_page_no', $max_page_no);
        // $smarty->assign('page_count', $max_page_no + 1);
        // $smarty->assign('prev_page_no', $prev_page_no);
        // $smarty->assign('next_page_no', $next_page_no);

	    $arr = array('max_page_no' => $max_page_no,
                     'page_count' => ($max_page_no + 1),
                     'prev_page_no' => $prev_page_no,
                     'next_page_no'=> $next_page_no);

        // print_r($arr);

        return $arr;
    }


    public function ActionDisplaycontent()
    {
        // $request = $this->getRequest();
        // $params = $request->getParams();
        $params = $_GET;
        // print_r($params);

        $sphc = new SphinxClient();
        $sphc->SetServer($this->sph_host, $this->sph_port);
                 
        $rc = $sphc->open();
        if (!$rc) {
            echo "open sphinx search faild.\n";
            echo $sphc->GetLastError() . "\n";
            exit;
        }

        $sphc->SetIDRange($params['id'], $params['id']);
        $res = $sphc->Query('');

        // print_r($res);
	    $article_url = $res['matches'][$params['id']]['attrs']['referer'];
        $article_title = $res['matches'][$params['id']]['attrs']['title'];
        $article_body = $res['matches'][$params['id']]['attrs']['body'];
        $article_body = trim($article_body);
        $article_body = str_replace("<BR><BR><BR><BR>", "<BR>", $article_body);
        $article_body = str_replace("<BR><BR><BR>", "<BR>", $article_body);
        $article_body = str_replace("<BR><BR>", "<BR>", $article_body);
        $article_body = trim($article_body);
        if (strpos($article_body, "<BR>") === 0) {
            $article_body = substr($article_body, 4);
        }
        $words = explode(' ', $params['word_list']);
        foreach ($words as $seq => $word) {
            $article_title = str_replace($word, "<FONT COLOR=red><B>{$word}</B></FONT>", $article_title);
            $article_body = str_replace($word, "<FONT COLOR=red><B>{$word}</B></FONT>", $article_body);
            $article_body = nl2br($article_body);
        }
        /* $docs[0] = $article_body; */
        /* $opts = array('around' => 100000, 'chunk_separator'=>''); */
        /* $docs_excerpts = $sphc->BuildExcerpts($docs, "idx_cms_drupal_main", $params['word_list'], $opts); */
        $cost_time = $res['time'];
        $total_found = $res['total_found'];

        // $smarty = Zend_Registry::get('smarty');
              
        // $smarty->assign('article_title', $article_title);
        // $smarty->assign('article_body', $article_body);
        // $smarty->assign('article_excerpts', $docs_excerpts[0]);
        // $smarty->assign('cost_time', $cost_time);
        // $smarty->assign('total_found', $total_found);

        $tarr = array('article_title'=> $article_title,
	                  'article_body'=> $article_body,
                      'article_excerpts'=> $docs_excerpts[0],
                      'cost_time'=> $cost_time,
                      'total_found'=> $total_found,
                      'url'=> $article_url);

        // $smarty->display('default/detail.html');

        $sphc->close();

        $this->render('detail', $tarr);
    }


	public function actionStatus()
	{

        $sphc = new SphinxClient();
        // $sphc->SetServer('127.0.0.1', 9312);
	    $sphc->SetServer($this->sph_host, $this->sph_port);
                 
        $rc = $sphc->open();
        if (!$rc) {
            echo "open sphinx search faild.\n";
            echo $sphc->GetLastError() . "\n";
            exit;
        }

        $sphc->SetFieldWeights(array('title' => 3));
        $sphc->SetIndexWeights(array('idx_cms_drupal_delta' => 5));
        // $sphc->setFilter('deleted', array(1), true);
        $sphc->setFilter('sched_deleted', array(1), true);
        // $sphc->setSelect("@count");
        $res = $sphc->query(null); // '' 或者 null 等效
        // var_dump($res);
	    if (!$res) {
	       
        }

	    $total_found = $res['total_found'];
        // echo "索引中总文档数: {$res['total_found']}\n<BR>";

        $res2 = $sphc->query("*"); // 这个与 '' 或者 null 等效.
        // print_r($res);
	    // var_dump($res);
                                 
        $sts = $sphc->Status();
                 
        // print_r($sts);
                 
        foreach ($sts as $idx => $value_pair) {
            // echo "{$value_pair[0]} = {$value_pair[1]}\n<br>"; 
        }
                 
        $sphc->Close();

	    $tarr = array('sts'=> $sts,
	    	    	  'total_count'=> $total_found);

	    $this->render('status', $tarr);		
    }

	public function actionSplitter()
	{
		// $rstr = exec("ls /", $output, $ret_val);
		// print_r($output);
		// var_dump($rstr);
        // var_dump($ret_val);
		// echo dirname(__FILE__);
		$ecmd = dirname(__FILE__)."/mmseg -d " . dirname(__FILE__) . "/etc/ ". dirname(__FILE__)  . "/abc.txt";
		$ecmd2 = dirname(__FILE__)."/mmseg -d " . dirname(__FILE__) . "/etc/ ";
		// echo $ecmd;
		// $rstr = exec($ecmd, $output, $ret_val);
        // print_r($output);
		// var_dump($rstr);
        // var_dump($ret_val);
		// print_r($_SERVER);
				

		$tarr = array();

		if (count($_POST)) {
		    $params = $_POST;
            // print_r($params);

		    if (strstr($_SERVER['HTTP_HOST'], "phpcloud.com")) {
                $sphc = new SphinxClient();
                // $sphc->SetServer('127.0.0.1', 9312);
                $sphc->SetServer($this->sph_host, $this->sph_port);
                 
                $rc = $sphc->open();
                if (!$rc) {
                    echo "open sphinx search faild.\n";
                    echo $sphc->GetLastError() . "\n";
                    exit;
		        }

                $sphc->setIDRange(0, 0);
                $res = $sphc->Query(trim($params['stext']), 'qtchina_cms');
            
                if (!$res) {
                    echo $sphc->getLastError();
                    var_dump($res);
                }     
                // print_r($res);
		    
                $sphc->Close();

                $splitted_words = '';
                if (count($res['words']))
                    foreach ($res['words'] as $word => $val) {
                        $splitted_words .= $word . "  ";
                    }

            } else {
                $tmpfname = tempnam("/tmp", "sph");
                $handle = fopen($tmpfname, "w");
                fwrite($handle, $params['stext']);
                fclose($handle);
                // do here something
                $cmd = $ecmd2 . " " . $tmpfname;
                $rstr = exec($cmd, $output, $ret_val);
                unlink($tmpfname);
                $splitted_words = implode(' ', $output);
            }

		}


		$tarr = array('splitted_words' => $splitted_words);

		$this->render('splitter', $tarr);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
            {
                $model->attributes=$_POST['ContactForm'];
                if($model->validate())
                    {
                        $headers="From: {$model->email}\r\nReply-To: {$model->email}";
                        mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
                        Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                        $this->refresh();
                    }
            }
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

		// collect user input data
		if(isset($_POST['LoginForm']))
            {
                $model->attributes=$_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}