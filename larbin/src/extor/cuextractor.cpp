
#include <boost/algorithm/string.hpp>

#include "tidy.h"
#include "buffio.h"

#include "stdlog.h"

#include "cuextractor.h"

namespace ba = boost::algorithm;
namespace bt = boost::tuples;

CuExtractor::CuExtractor()
    : Extractor()
{
    this->init();
}

CuExtractor::~CuExtractor()
{

}

bool CuExtractor::init()
{
    this->title_selector = "td#class=Ftext1";
    this->content_selector = "div#id=artibody, td#class=F14"; // or td#class=F14
    // this->category_selector = "title";
    this->category_selector = "td#valign=bottom";
    this->view_count_selector = "span#id=hits";
    this->comment_count_selector = "span#id=comments_top, span#id=postcount";

    this->ctime_selector = this->author_selector = "span#id=hits, span#id=postcount"; // 's parent node

    return true;
}

std::string CuExtractor::get_author()
{
    std::string rstr;

    qLogx()<<""<<this->author_selector;

    int rlevel = 0;
    TidyNode html_node = NULL, auth_node = NULL, tmp_node = NULL;
    TidyBuffer tbuf;
    CssSelector *selector = this->create_selector(this->author_selector);
    std::string tstr;
    std::vector<std::string> strs, strs2;

    html_node = tidyGetHtml(this->mdoc);
    if (html_node) {
        for (int i = 0; i < selector->selectors.size(); ++i) {
            selector->setn(i);
            tmp_node = this->find_node(this->mdoc, html_node, selector, &rlevel);
            if (tmp_node) {
                break;
            }
        }
    }

    if (tmp_node) {
        auth_node = tidyGetParent(tmp_node);
        if (auth_node) {
            tidyBufInit(&tbuf);
            tstr = this->get_node_plain_text(this->mdoc, auth_node, &tbuf);
            tstr = std::string((char*)tbuf.bp, tbuf.size);
            qLogx()<<"Got author text:"<<tstr<<tbuf.size;
            tidyBufFree(&tbuf);

            if (!tstr.empty()) {
                boost::algorithm::replace_all(tstr, "　", " ");
                boost::algorithm::split(strs, tstr, boost::algorithm::is_any_of(" ："), boost::algorithm::token_compress_on);
                qLogx()<<"author split:"<<strs.size();
                if (strs.size() > 1) {
                    rstr = strs.at(1);
                    boost::algorithm::trim_if(rstr, boost::algorithm::is_any_of(this->trim_chars));
                }
            }
        }
    }

    delete selector;

    return rstr;
}

std::string CuExtractor::get_ctime()
{
    std::string rstr;

    qLogx()<<""<<this->ctime_selector;

    int rlevel = 0;
    TidyNode html_node = NULL, auth_node = NULL, tmp_node = NULL;
    TidyBuffer tbuf;
    CssSelector *selector = this->create_selector(this->ctime_selector);
    std::string tstr;
    std::vector<std::string> strs, strs2;

    html_node = tidyGetHtml(this->mdoc);
    if (html_node != NULL) {
        for (int i = 0; i < selector->selectors.size(); ++i) {
            selector->setn(i);
            tmp_node = this->find_node(this->mdoc, html_node, selector, &rlevel);
            if (tmp_node) {
                break;
            }
        }
    }

    if (tmp_node) {
        auth_node = tidyGetParent(tmp_node);
        if (auth_node) {
            tidyBufInit(&tbuf);
            tstr = this->get_node_plain_text(this->mdoc, auth_node, &tbuf);
            tstr = std::string((char*)tbuf.bp, tbuf.size);
            qLogx()<<"Got ctime text:"<<tstr<<tbuf.size;
            tidyBufFree(&tbuf);

            if (!tstr.empty()) {
                boost::algorithm::replace_all(tstr, "　", " ");
                boost::algorithm::split(strs, tstr, boost::algorithm::is_any_of(" ："), boost::algorithm::token_compress_on);
                qLogx()<<"ctime split:"<<strs.size();
                for (int i = 0; i < strs.size(); ++i) {
                    qLogx()<<"ctime parts:"<<i<<strs.at(i);
                }
                if (strs.size() > 4) {
                    rstr = strs.at(3) + " " + strs.at(4);
                    boost::algorithm::trim_if(rstr, boost::algorithm::is_any_of(this->trim_chars));
                    if (rstr.length() == strlen("2009.11.06 08:05")) {
                        rstr += ":07";
                        boost::algorithm::replace_all(rstr, ".", "-");
                    }
                }
            }
        } else {
            qLogx()<<"Not found pnode.";
        }
    } else {
        qLogx()<<"Not found. ";
    }

    delete selector;

    return rstr;
}

std::string CuExtractor::get_category()
{
    std::string rstr;

    std::string tstr;
    std::vector<std::string> strs;

    tstr = Extractor::get_category();

    if (!tstr.empty()) {
        boost::algorithm::replace_all(tstr, "&nbsp;", "");
        boost::algorithm::split(strs, tstr, boost::algorithm::is_any_of("&gt;"), boost::algorithm::token_compress_on);
        for (int i = 0; i < strs.size(); ++i) {
            if (i > 0 && i < strs.size()-1) {
                rstr += strs.at(i);
                if (i < strs.size()-1-1) {
                    rstr += "&gt;";
                }
            }
        }
    }

    if (0) {
        std::string tstr;
        std::vector<std::string> strs;
        TidyNode head_node, tmp_node;
        TidyBuffer tbuf;
        ctmbstr node_name;

        head_node = tidyGetHead(this->mdoc);
        if (head_node) {
            for (tmp_node = tidyGetChild(head_node); tmp_node; tmp_node = tidyGetNext(tmp_node)) {
                node_name = tidyNodeGetName(tmp_node);
                if (node_name == NULL) {
                    continue;
                }
                if (strcasecmp(node_name, this->category_selector) == 0) {
                    tidyBufInit(&tbuf);
                    tstr = this->get_node_plain_text(this->mdoc, tidyGetChild(tmp_node), &tbuf);
                    tstr = std::string((char*)tbuf.bp, tbuf.size);
                    tidyBufFree(&tbuf);

                    if (!tstr.empty()) {
                        boost::algorithm::split(strs, tstr, boost::algorithm::is_any_of("_-"), boost::algorithm::token_compress_on);
                        if (strs.size() > 1) {
                            rstr = strs.at(1);
                        }
                    }

                }
            }
        }
    }

    return rstr;
}
