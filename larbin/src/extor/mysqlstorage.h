
#ifndef _MYSQL_STORAGE_H_
#define _MYSQL_STORAGE_H_

#include <string>
#include <map>
#include <queue>
#include <vector>

#include <mysql/mysql.h>


class StorageItem {
public:
    StorageItem() {
        memset(url, 0, sizeof(url));
        memset(rid, 0, sizeof(rid));
        memset(title, 0, sizeof(title));
        memset(body, 0, sizeof(body));
        memset(author, 0, sizeof(author));
        memset(category, 0, sizeof(category));
        memset(ctime, 0, sizeof(ctime));
        memset(keywords, 0, sizeof(keywords));
        memset(view_count, 0, sizeof(view_count));
        memset(comment_count, 0, sizeof(comment_count));
    }

    char url[512];
    char rid[255];
    char title[255];
    char body[1000*1000];
    char author[255];
    char category[255];
    char ctime[255];
    char keywords[255];
    char view_count[255];
    char comment_count[255];
};

class UrlItem {
public:
    UrlItem() {
        memset(url, 0, sizeof(url));
    }

    char url[512];
};

class MysqlStorage
{
public:
    MysqlStorage();
    virtual ~MysqlStorage();

    bool init();
    void run();

    bool addItem(StorageItem *item);
    bool addItem(UrlItem *item);

    bool execute(MYSQL *mh, StorageItem *item);
    bool execute(MYSQL *mh, UrlItem *item);

    bool setStop();

private:
    // QTextCodec *u8c;
    // QQueue<StorageItem*> items;
    // QWaitCondition m_cond;
    // QMutex m_mutex;
    std::queue<StorageItem*> items;
    std::queue<UrlItem*> uitems;
    bool m_user_stop;
    MYSQL mh;
};


#endif
