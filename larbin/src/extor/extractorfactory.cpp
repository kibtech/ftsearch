#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "stdlib.h"

#include "huanqiuextractor.h"

#include "extractorfactory.h"
#include "cuextractor.h"


ExtractorFactory::ExtractorFactory()
{

}

ExtractorFactory::~ExtractorFactory()
{

}

Extractor *ExtractorFactory::create(const char *url)
{
    qLogx()<<url<<1<<2;
    Extractor *extor = NULL;
    char *p = NULL;
    char *turl = strdup(url);

    if ((p = strcasestr(turl, "huanqiu.com")) != NULL) {
        extor = new HuanqiuExtractor();

    } else if ((p = strcasestr(turl, "chinaunix.net")) != NULL) {
        extor = new CuExtractor();

    } else {
        qLogx()<<"";
    }

    free(turl);

    return extor;
}
