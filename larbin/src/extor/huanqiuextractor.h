#ifndef _HUANQIU_EXTRACTOR_H_
#define _HUANQIU_EXTRACTOR_H_

#include "stdlog.h"

#include "extractor.h"

class HuanqiuExtractor : public Extractor
{
public:
    explicit HuanqiuExtractor();
    virtual ~HuanqiuExtractor();

    virtual std::string get_title();
    virtual std::string get_category();
    virtual std::string get_ctime();
    virtual std::string get_comment_count();

protected:
    virtual bool init();
};

#endif
