#ifndef _EXTRACTOR_FACTORY_H_
#define _EXTRACTOR_FACTORY_H_

#include "extor/extractor.h"

class ExtractorFactory
{
public:
    explicit ExtractorFactory();
    ~ExtractorFactory();

    static Extractor *create(const char *url);
};

#endif
