
#include <boost/algorithm/string.hpp>

#include "extor/stdlog.h"

#include "huanqiuextractor.h"

HuanqiuExtractor::HuanqiuExtractor()
    : Extractor()
{
    this->init();
}

HuanqiuExtractor::~HuanqiuExtractor()
{

}

bool HuanqiuExtractor::init()
{
    //    this->title_selector = "td#class=Ftext1";
    //    this->content_selector = "div#id=artibody";

    this->title_selector = "div#class=main, div#id=content, a#id=thread_subject";// "div[class=blkleft]>h1:first-child";
    this->content_selector = "div#id=text, td#class=t_f";
    this->view_count_selector = "span#id=linkCnt, span#class=xi1";
    // this->comment_count_selector = "iframe>html>body>p>a";
    this->comment_count_selector = "span#class=xi1";
    this->category_selector = "div#class=location, div#id=serch, div#id=pt";
    this->ctime_selector = "li#id=get_date, img#class=authicn vm";
    this->author_selector = "li#id=fromsite,div#class=authi";

    return true;
}

std::string HuanqiuExtractor::get_title()
{
    std::string rstr;

    int rlevel = 0;
     TidyNode html_node = NULL, auth_node = NULL, tmp_node = NULL;
    TidyBuffer tbuf;
    CssSelector *selector = this->create_selector(this->title_selector);
    std::string tstr;
    std::vector<std::string> strs, strs2;

    html_node = tidyGetHtml(this->mdoc);
    if (html_node != NULL) {
        for (int i = 0; i < selector->selectors.size(); ++i) {
            selector->setn(i);
            tmp_node = this->find_node(this->mdoc, html_node, selector, &rlevel);
            if (tmp_node) {
                break;
            }
        }
    }

    if (tmp_node) {
        auth_node = tidyGetChild(tmp_node);
        if (auth_node) {
            if (tidyNodeGetId(auth_node) == TidyTag_H1
                    || tidyNodeGetId(auth_node) == TidyTag_H2) {
                tidyBufInit(&tbuf);
                tstr = this->get_node_plain_text_r(this->mdoc, auth_node, &tbuf);
                tstr = std::string((char*)tbuf.bp, tbuf.size);
                qLogx()<<"Got title text:"<<tstr<<tbuf.size;
                tidyBufFree(&tbuf);
            }
            if (!tstr.empty()) {
                rstr = tstr;
            }
        } else {
            qLogx()<<"Not found pnode.";
        }
        if (rstr.empty() && auth_node != NULL) {
            tidyBufInit(&tbuf);
            tstr = this->get_node_plain_text_r(this->mdoc, auth_node, &tbuf);
            tstr = std::string((char*)tbuf.bp, tbuf.size);
            qLogx()<<"Got title text bbs:"<<tstr<<tbuf.size;
            tidyBufFree(&tbuf);

            if (!tstr.empty()) {
                rstr = tstr;
            }
        }
    } else {
        qLogx()<<"Not found. ";
    }

    delete selector;

    return rstr;
}

std::string HuanqiuExtractor::get_category()
{
    std::string rstr;

    std::string tstr;
    std::vector<std::string> strs;

    tstr = Extractor::get_category();

    if (!tstr.empty()) {
        boost::algorithm::replace_all(tstr, "›", "&gt;"); // for bbs nav
        boost::algorithm::split(strs, tstr, boost::algorithm::is_any_of("&gt;"), boost::algorithm::token_compress_on);
        for (int i = 0; i < strs.size(); ++i) {
            if (i > 0 && i < strs.size()-1) {
                rstr += strs.at(i);
                if (i < strs.size()-1-1) {
                    rstr += "&gt;";
                }
            }
        }
    }

    return rstr;
}

std::string HuanqiuExtractor::get_ctime()
{
    std::string rstr;

    rstr = Extractor::get_ctime();

    if (rstr.empty()) {
        // maybe bbs
        // authicn vm
        int rlevel = 0;
        TidyNode html_node = NULL, auth_node = NULL, tmp_node = NULL;
        TidyBuffer tbuf;
        CssSelector *selector = this->create_selector(this->ctime_selector);
        std::string tstr;
        std::vector<std::string> strs, strs2;


        html_node = tidyGetHtml(this->mdoc);
        if (html_node != NULL) {
            for (int i = 0; i < selector->selectors.size(); ++i) {
                selector->setn(i);
                tmp_node = this->find_node(this->mdoc, html_node, selector, &rlevel);
                if (tmp_node) {
                    break;
                }
            }
        }

        if (tmp_node) {
            qLogx()<<tidyNodeGetName(tmp_node);
            auth_node = tidyGetNext(tmp_node);
            if (auth_node) auth_node = tidyGetNext(auth_node);
            if (auth_node) auth_node = tidyGetChild(auth_node);
            if (auth_node) auth_node = tidyGetNext(auth_node);
//            tidyBufInit(&tbuf);
//            this->get_node_plain_text_r(this->mdoc, auth_node, &tbuf);
//            tstr = std::string((char*)tbuf.bp, tbuf.size);
//            qLogx()<<auth_node<<tidyNodeGetType(auth_node)<<tidyNodeGetId(auth_node)<<tstr;
//            tidyBufFree(&tbuf);
//            tstr.clear();

            if (auth_node && tidyNodeGetId(auth_node) == TidyTag_SPAN) {
                TidyAttr tattr = tidyAttrGetTITLE(auth_node);
                ctmbstr aval = tidyAttrValue(tattr);
                tstr = std::string(aval);
                qLogx()<<"Got ctime count text:"<<tstr;
            } else {
                qLogx()<<"Not found ctime count node.";
            }
            if (!tstr.empty()) {
                rstr = tstr;
            }
        } else {
            qLogx()<<"Not found. ";
        }

        delete selector;
    }

    return rstr;
}

std::string HuanqiuExtractor::get_comment_count()
{
    std::string rstr;

    int rlevel = 0;
    TidyNode html_node = NULL, auth_node = NULL, tmp_node = NULL;
    TidyBuffer tbuf;
    CssSelector *selector = this->create_selector(this->comment_count_selector);
    std::string tstr;
    std::vector<std::string> strs, strs2;

    html_node = tidyGetHtml(this->mdoc);
    if (html_node != NULL) {
        for (int i = 0; i < selector->selectors.size(); ++i) {
            selector->setn(i);
            tmp_node = this->find_node(this->mdoc, html_node, selector, &rlevel);
            if (tmp_node) {
                break;
            }
        }
    }

    if (tmp_node) {
        auth_node = tidyGetNext(tmp_node);
        if (auth_node) auth_node = tidyGetNext(auth_node);
        if (auth_node) auth_node = tidyGetNext(auth_node);
        if (auth_node) auth_node = tidyGetNext(auth_node);

        if (auth_node) {
            tidyBufInit(&tbuf);
            tstr = this->get_node_plain_text_r(this->mdoc, auth_node, &tbuf);
            tstr = std::string((char*)tbuf.bp, tbuf.size);
            qLogx()<<"Got comment count text:"<<tstr<<tbuf.size;
            tidyBufFree(&tbuf);
        } else {
            qLogx()<<"Not found comment count node.";
        }
        if (!tstr.empty()) {
            rstr = tstr;
        }
    } else {
        qLogx()<<"Not found. ";
    }

    delete selector;

    return rstr;
}
