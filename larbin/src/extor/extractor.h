#ifndef _EXTRACTOR_H_
#define _EXTRACTOR_H_

#include <iconv.h>
#include <string>
#include <vector>

#include <boost/tuple/tuple.hpp>

#include <tidy.h>
#include <buffio.h>

class Extractor
{
public:
    explicit Extractor();
    virtual ~Extractor();

    virtual bool parser_page(const char *url, const char *page);

    //    virtual bool parser_rid();
//    virtual bool parser_title();
//    virtual bool parser_ctime();
//    virtual bool parser_content();
//    virtual bool parser_auther();
//    virtual bool parser_category();
//    virtual bool parser_view_count();
//    virtual bool parser_comment_count();

    virtual std::string get_rid();
    virtual std::string get_ctime();
    virtual std::string get_title();
    virtual std::string get_content();
    virtual std::string get_author();
    virtual std::string get_category();
    virtual std::string get_view_count();
    virtual std::string get_comment_count();

    //// tools function
    static std::string strrtrim(std::string &str);
    static std::string strltrim(std::string &str);
    static std::string && strtrim(std::string &str);

protected:
    // pattern: [tag#attr=value[,tag2#attr2=value2](*)]
    const char *title_selector;// = "td#class=Ftext1"; // td#id=titleid
    const char *content_selector; // div#id=artibody,td#class=F14
    const char *author_selector;
    const char *ctime_selector;
    const char *category_selector;
    const char *view_count_selector;
    const char *comment_count_selector;

protected:
    bool parse_charset();
    virtual bool init() = 0;
    virtual bool destory();
    virtual bool tidy_init();
    virtual bool get_element_plain_text(const char *str_selectors, std::string &rstr);

    class CssSelector {
    public:
        CssSelector() {
            node_name = attr_name = attr_value = NULL;
            npos = -1;
        }
        virtual ~CssSelector() {
//            if (node_name) free(node_name);
//            if (attr_name) free(attr_name);
//            if (attr_value) free(attr_value);
        }
        virtual bool setn(int n) {
            if (n >=0 && n < this->selectors.size()) {
                this->node_name = selectors.at(n).get<E_NODE_NAME>().c_str();
                this->attr_name = selectors.at(n).get<E_ATTR_NAME>().c_str();
                this->attr_value = selectors.at(n).get<E_ATTR_VALUE>().c_str();
                npos = n;
                return true;
            }
            node_name = attr_name = attr_value = NULL;
            npos = -1;
            return false;
        }

        const char *node_name;
        const char *attr_name;
        const char *attr_value;
        typedef boost::tuples::tuple<std::string, std::string, std::string> SelectorElement;
        std::vector<SelectorElement> selectors;
    private:
        enum {E_NODE_NAME = 0, E_ATTR_NAME , E_ATTR_VALUE};
        int npos;
    };

    virtual CssSelector *create_selector(const char *str);
    virtual TidyNode find_node(TidyDoc tdoc, TidyNode pnode, CssSelector *selector, int *rlevel);
    virtual std::string get_node_plain_text(TidyDoc tdoc, TidyNode node, TidyBuffer *buf);
    virtual std::string get_node_plain_text_r(TidyDoc tdoc, TidyNode node, TidyBuffer *buf);

protected:
    char *murl;
    char *mpage;
    char *mu8page;
    char mcharset[32];

    TidyDoc mdoc;
    static const char *trim_chars;//  = " \r\n　"; // last is zh space
    static const int less_sec_time_len = 16; // "2011-12-06 17:55"

};

time_t time_from_string(const std::string &ts);

#endif
