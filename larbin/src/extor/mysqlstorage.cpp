#include <cassert>
#include <cstdio>
#include <cstring>
#include <strings.h>

#include "stdlog.h"

#include "mysqlstorage.h"

MysqlStorage::MysqlStorage()
{
    this->m_user_stop = false;
}

MysqlStorage::~MysqlStorage()
{

}

bool MysqlStorage::init()
{
    MYSQL *pmh;

    if (::mysql_init(&mh) == NULL) {
        assert(1==2);
    }

    const char *host = "202.108.15.6";// "172.24.149.6";// "202.108.15.6";
    int port = 3306;
    const char *user = "postgres";
    const char *pass = "post.DB#2872";
    const char *dbname = "drupal";
    bool bret;
    int iret;
    StorageItem *item;

retry_conn:
    pmh = ::mysql_real_connect(&mh, host, user, pass, dbname, port, NULL, 0);
    assert(pmh != NULL);
    ::mysql_real_query(&mh, "set names utf8", strlen("set names utf8"));
    qLogx()<<"mysql connected ok";

    return true;
}

void MysqlStorage::run()
{
    bool quit = true;
    bool bret;
    int iret;
    StorageItem *item;
    UrlItem *uitem;

    while (!this->m_user_stop) {
        while (!this->items.empty()) {
            item = this->items.front();
            this->items.pop();

            bret = this->execute(&mh, item);

            if (!bret) {
                iret = ::mysql_ping(&mh);
                if (iret != 0) {
                    this->items.push(item);
                    qLogx()<<"Reconnect to db in 3 seconds...";
                    ::sleep(3);
                    this->init();
                }
            }
            delete item; item = NULL;
        }

        while (!this->uitems.empty()) {
            uitem = this->uitems.front();
            this->uitems.pop();

            bret = this->execute(&mh, uitem);
            if (!bret) {
                iret = ::mysql_ping(&mh);
                if (iret != 0) {
                    this->uitems.push(uitem);
                    qLogx()<<"Reconnect i2 to db in 3 seconds...";
                    ::sleep(3);
                    this->init();
                }
            }
            delete uitem; uitem = NULL;
        }

        if (quit) {
            break;
        }
    }
}

bool MysqlStorage::addItem(StorageItem *item)
{

    if (item == NULL) {
        qLogx()<<"Null item:";
        return false;
    }
    this->items.push(item);
    this->run();

    return true;
}

bool MysqlStorage::addItem(UrlItem *item)
{
    if (item == NULL) {
        qLogx()<<"Null url item:";
        return false;
    }
    this->uitems.push(item);
    this->run();

    return true;
}

bool MysqlStorage::execute(MYSQL *mh, StorageItem *item)
{
    unsigned long iret;
    unsigned long lid = 0;
    char wbuf[1200*1000] = {0};
    char esc_title[512] = {0};
    char esc_author[512] = {0};
    char esc_body[1000*1000] = {0};
    char esc_url[512] = {0};
    char esc_rid[512] = {0};
    char esc_category[512] = {0};
    char esc_ctime[512] = {0};
    char esc_keywords[512] = {0};
    char esc_view_count[512] = {0};
    char esc_comment_count[512] = {0};

    iret = ::mysql_real_escape_string(mh, esc_title, item->title, strlen(item->title));
    iret = ::mysql_real_escape_string(mh, esc_author, item->author, strlen(item->author));
    iret = ::mysql_real_escape_string(mh, esc_body, item->body, strlen(item->body));
    iret = ::mysql_real_escape_string(mh, esc_url, item->url, strlen(item->url));
    iret = ::mysql_real_escape_string(mh, esc_rid, item->rid, strlen(item->rid));
    iret = ::mysql_real_escape_string(mh, esc_category, item->category, strlen(item->category));
    iret = ::mysql_real_escape_string(mh, esc_ctime, item->ctime, strlen(item->ctime));
    iret = ::mysql_real_escape_string(mh, esc_keywords, item->keywords, strlen(item->keywords));
    iret = ::mysql_real_escape_string(mh, esc_view_count, item->view_count, strlen(item->view_count));
    iret = ::mysql_real_escape_string(mh, esc_comment_count, item->comment_count, strlen(item->comment_count));


    snprintf(wbuf, sizeof(wbuf)-1,
             "INSERT INTO spider_items_el (rid, referer, title, author, category, ctime, keywords, view_count, comment_count, body) "
             " VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")",
             esc_rid, esc_url, esc_title, esc_author, esc_category, esc_ctime, esc_keywords, esc_view_count, esc_comment_count, esc_body
             );

    qLogx()<<"Execute content SQL: "<<wbuf;

    // return true; // 模拟模式

    iret = ::mysql_real_query(mh, wbuf, strlen(wbuf));
    if (iret != 0) {
        qLogx()<<iret<<::mysql_error(mh);
        return false;
    } else {
        lid = ::mysql_insert_id(mh);
        qLogx()<<"record id:"<<lid;
    }

    return true;
}

bool MysqlStorage::execute(MYSQL *mh, UrlItem *item)
{
    unsigned long iret;
    unsigned long lid = 0;
    char wbuf[1200*1000] = {0};
//    char esc_title[512] = {0};
//    char esc_author[512] = {0};
//    char esc_body[1000*1000] = {0};
    char esc_url[512] = {0};
//    char esc_rid[512] = {0};
//    char esc_category[512] = {0};
//    char esc_ctime[512] = {0};
//    char esc_keywords[512] = {0};
//    char esc_view_count[512] = {0};
//    char esc_comment_count[512] = {0};

//    iret = ::mysql_real_escape_string(mh, esc_title, item->title, strlen(item->title));
//    iret = ::mysql_real_escape_string(mh, esc_author, item->author, strlen(item->author));
//    iret = ::mysql_real_escape_string(mh, esc_body, item->body, strlen(item->body));
    iret = ::mysql_real_escape_string(mh, esc_url, item->url, strlen(item->url));
//    iret = ::mysql_real_escape_string(mh, esc_rid, item->rid, strlen(item->rid));
//    iret = ::mysql_real_escape_string(mh, esc_category, item->category, strlen(item->category));
//    iret = ::mysql_real_escape_string(mh, esc_ctime, item->ctime, strlen(item->ctime));
//    iret = ::mysql_real_escape_string(mh, esc_keywords, item->keywords, strlen(item->keywords));
//    iret = ::mysql_real_escape_string(mh, esc_view_count, item->view_count, strlen(item->view_count));
//    iret = ::mysql_real_escape_string(mh, esc_comment_count, item->comment_count, strlen(item->comment_count));


    //// add url
    {
        memset(wbuf, 0, sizeof(wbuf));
        snprintf(wbuf, sizeof(wbuf)-1,
                 "INSERT INTO spider_urls (url, ctime)"
                 " VALUES ('%s', %d)",
                 esc_url, time(NULL)
                 );

        qLogx()<<"Execute url SQL: "<<wbuf;
        // return true; // 模拟模式

        iret = ::mysql_real_query(mh, wbuf, strlen(wbuf));
        if (iret != 0) {
            qLogx()<<iret<<::mysql_error(mh);
            return false;
        } else {
            lid = ::mysql_insert_id(mh);
            qLogx()<<"record url id:"<<lid;
        }
    }

    return true;
}

bool MysqlStorage::setStop()
{
    this->m_user_stop = true;
    return true;
}
