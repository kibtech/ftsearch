#ifndef _CU_EXTRACTOR_H_
#define _CU_EXTRACTOR_H_

#include "stdlog.h"

#include "extractor.h"

class CuExtractor : public Extractor
{
public:
    explicit CuExtractor();
    virtual ~CuExtractor();

    virtual std::string get_author();
    virtual std::string get_ctime();
    virtual std::string get_category();

protected:
    virtual bool init();

};

#endif
