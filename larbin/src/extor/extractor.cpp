
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <strings.h>
#include <cerrno>
#include <algorithm>


#include <boost/algorithm/string.hpp>

#include <tidy.h>
#include <buffio.h>

#include "stdlog.h"
#include "extractor.h"

#include "TextExtractor.h"

const char *Extractor::trim_chars = " \r\n";
Extractor::Extractor()
{
    this->murl = NULL;
    this->mpage = NULL;
    this->mu8page = NULL;
    memset(this->mcharset, 0, sizeof(this->mcharset));

    this->mdoc = NULL;
    this->title_selector = "nonode#class=nonode";
    this->content_selector = "nonode#id=nonode";

    author_selector =
    ctime_selector =
    category_selector =
    view_count_selector =
    comment_count_selector = this->title_selector;
}

Extractor::~Extractor()
{
    this->destory();
}

bool Extractor::destory()
{
    qLogx()<<""<<(void*)this->mpage<<(void*)this->mu8page;
    if (this->murl) {
        free(this->murl); this->murl = NULL;
    }
    if (this->mpage) {
        if (this->mu8page == this->mpage) {
            free(this->mpage); this->mpage = NULL;
            this->mu8page = NULL;
        } else {
            free(this->mu8page); this->mu8page = NULL;
            free(this->mpage); this->mpage = NULL;
        }
    }
    if (this->mdoc != NULL) {
        tidyRelease(this->mdoc);
        this->mdoc = NULL;
    }
}

bool Extractor::parser_page(const char *url, const char *page)
{
    bool bret;
    int iret;

    bool need_iconv = true;
    char fcodec[32] = {0};
    const char *ucodec = "UTF-8";
    char *ptr = NULL, *dest_ptr = NULL;

    this->murl = strdup(url);
    this->mu8page = this->mpage = strdup(page);

    // get charset
    bret = this->parse_charset();
    if (!bret || strlen(this->mcharset) == 0) {
        // return false;
    }

    // iconv page if need
    if (strcasecmp(this->mcharset, "utf-8") == 0) {
        // enn, all ok
        need_iconv = false;
    } else if (strcasecmp(this->mcharset, "gb2312") == 0) {
        // strcpy(fcodec, "GB2312");
        strcpy(fcodec, "GBK");
    } else if (strcasecmp(this->mcharset, "gbk") == 0) {
        strcpy(fcodec, "GBK");
    } else {
        need_iconv = false;
        qLogx()<<"Unknown page encoding: "<<this->mcharset;
    }

    if ((need_iconv == true) && strlen(fcodec) > 0) {
        this->mu8page = (char*)calloc(2, strlen(page));
        if (this->mu8page == NULL) {
            // not good
        }
        size_t inlen = 0;
        size_t outlen = 0;

        iconv_t ich = iconv_open(ucodec, fcodec);
        qLogx()<<"Open iconv for:"<<ich<<fcodec<<"to"<<ucodec;
        if (ich == (iconv_t)-1) {
            // not good
            qLogx()<<"iconv create faild."<<(ich)<<errno<<strerror(errno);
        }

        inlen = strlen(this->mpage);
        outlen = 2 * inlen;
        qLogx()<<"Iconv preeeee:"<<errno<<strlen(this->mpage)<<inlen<<outlen;
        ptr = this->mpage;
        dest_ptr = this->mu8page;
        iret = ::iconv(ich, &ptr, &inlen, &dest_ptr, &outlen);
        if (iret == -1) {
            // not very good
            qLogx()<<"Iconv error."<<errno<<strerror(errno)<<strlen(this->mpage)<<inlen<<outlen;
        } else {
            qLogx()<<"Iconv ok." << strlen(this->mu8page);// <<this->mu8page;
        }

        iconv_close(ich);
    } else {
        // not very good
    }

    // tidy parser it
    bret = this->tidy_init();

    return true;
}

/*
  html4: <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
  html5: <meta charset="gbk"/>
  */
bool Extractor::parse_charset()
{
    char *src = this->mpage;
    char *cs_pos = NULL;
    char *p1, *p2;
    cs_pos = strcasestr(src, "charset=");
    if (cs_pos) {
        p1 = cs_pos + strlen("charset=");
        p2 = strpbrk(p1, " \">/'");
        if (p1 == NULL || p2 == NULL) {
            qLogx()<<"can not find page encodeing charset.";
        } else if (p2 == p1){
            // like this type; <meta charset="gbk" />
            p1++;
            p2 = strpbrk(p1, " \">/'");
            if (p2) {
                strncpy(this->mcharset, p1, p2-p1 > 31 ? sizeof(this->mcharset)-1 : (p2-p1));
                qLogx()<<"page encoding charset:"<<this->mcharset;
            }
        } else {
            strncpy(this->mcharset, p1, p2-p1 > 31 ? sizeof(this->mcharset)-1 : (p2-p1));
            // strncpy(this->mcharset, p1, p2-p1);
            qLogx()<<"page encoding charset:"<<this->mcharset;
        }
    } else {
        memset(this->mcharset, 0, sizeof(this->mcharset));
        qLogx()<<"should not be arrived here.";
    }

    if (strlen(this->mcharset) > 10) {
        qLogx()<<"Maybe invalid charset:"<<this->mcharset;
        // strcpy(this->mcharset, "utf-8");
    }

    return true;
}

bool Extractor::tidy_init()
{
    Bool tbret;
    int iret;

    mdoc = tidyCreate();
    iret = tidyOptSetBool(mdoc, TidyShowWarnings, ::no);
    iret = tidySetCharEncoding(mdoc, "UTF8");
    iret = tidyParseString(mdoc, this->mu8page);
    if (iret != 0) {
        qLogx()<<iret<<"Not very good"<<tidyWarningCount(mdoc)<<tidyErrorCount(mdoc);
    }

    return true;
}

Extractor::CssSelector *Extractor::create_selector(const char *str)
{
    Extractor::CssSelector *selector = NULL;

    const char *ptr = NULL;
    const char *p1 = NULL, *p2 = NULL, *p3 = NULL;
    std::vector<std::string> str_selectors;
    std::string str_selector, tstr;
    std::string node_name, attr_name, attr_value;

    selector = new CssSelector();

    tstr = std::string(str);
    boost::algorithm::split(str_selectors, tstr, boost::algorithm::is_any_of(","), boost::algorithm::token_compress_on);

    for (int i = 0; i < str_selectors.size(); ++i) {
        str_selector = str_selectors.at(i);
        boost::algorithm::trim_if(str_selector, boost::algorithm::is_any_of(this->trim_chars));

        node_name.clear(); attr_name.clear(); attr_value.clear();
        ptr = str_selector.c_str();

        p1 = strchr(ptr, '#');
        if (p1 != NULL) {
            p2 = strchr(p1, '=');

            if (p2) {
                node_name = std::string(ptr, p1-ptr);
                attr_name = std::string(p1+1, p2-p1-1);
                attr_value = std::string(p2+1);
            } else {
                node_name = std::string(ptr, p1-ptr);
            }
        } else {
            node_name = std::string(ptr);
        }

        CssSelector::SelectorElement se = boost::tuples::make_tuple(node_name, attr_name, attr_value);
        selector->selectors.push_back(se);
    }

    qLogx()<<"simple css selector:"<<selector->selectors.size();// <<selector->selectors.at(0);
    for (int i = 0; i < selector->selectors.size(); ++i) {
        selector->setn(i);
        qLogx()<<"selector "<<i<<selector->node_name<<selector->attr_name<<selector->attr_value;
    }
    if (selector->selectors.size() > 0) {
        selector->setn(0);
    }

    if (0) {
        p1 = strchr(str, '#');
        if (p1 != NULL) {
            p2 = strchr(p1, '=');

            if (p2) {
                selector->node_name = strndup(str, p1-str);
                selector->attr_name = strndup(p1+1, p2-p1-1);
                selector->attr_value = strdup(p2+1);
            } else {
                selector->node_name = strndup(str, p1-str);
            }
        } else {
            selector->node_name = strdup(str);
        }

        qLogx()<<"simple css selector:"<<selector->node_name<<selector->attr_name<<selector->attr_value;

    }
    return selector;
}

TidyNode Extractor::find_node(TidyDoc tdoc, TidyNode pnode, CssSelector *selector, int *rlevel)
{
    TidyNode rnode = NULL;

    TidyNode root_node, head_node, body_node;
    TidyNode tmp_node, tmp2_node;
    TidyAttr tmp_attr, tmp2_attr;
    TidyNodeType tmp_type;
    ctmbstr node_name = NULL;
    ctmbstr attr_name = NULL;
    ctmbstr attr_value = NULL;
    bool bfound = false;

    root_node = tidyGetRoot(tdoc);
    (*rlevel) += 1;

    // 深度遍历
    for (tmp_node = tidyGetChild(pnode); tmp_node; tmp_node = tidyGetNext(tmp_node)) {
        tmp_type = tidyNodeGetType(tmp_node);

        if (tmp_type == TidyNode_Text || tmp_type == TidyNode_Comment) {
            // qLogx()<<"Non element node, Text/Comment node, ignore.";
            continue;
        }

        node_name = tidyNodeGetName(tmp_node);

        // qLogx()<<"review node:"<<(*rlevel)<<tmp_type<<(node_name?node_name:NULL);
        if (node_name != NULL && strcasecmp(selector->node_name, node_name) == 0) {
            for (tmp_attr = tidyAttrFirst(tmp_node); tmp_attr; tmp_attr = tidyAttrNext(tmp_attr)) {
                attr_name = tidyAttrName(tmp_attr);
                // qLogx()<<"review node:"<<(*rlevel)<<node_name<<attr_name;
                if (strcasecmp(selector->attr_name, attr_name) == 0) {
                    attr_value = tidyAttrValue(tmp_attr);
                    if (attr_value != NULL && strcasecmp(selector->attr_value, attr_value) == 0) {
                        qLogx()<<"Node found:"<<(*rlevel)<<node_name<<attr_name<<attr_value;
                        return tmp_node;
                    }
                }
            }
        }

        if (!tidyGetChild(tmp_node)) {
            continue;
        }
        tmp2_node = find_node(tdoc, tmp_node, selector, rlevel);
        if (tmp2_node) {
            // qLogx()<<"Node found:"<<(*rlevel)<<node_name;// <<attr_name<<attr_value;
            return tmp2_node;
        }
    }

    return rnode;
}

// TODO can not return proper value
std::string Extractor::get_node_plain_text(TidyDoc tdoc, TidyNode node, TidyBuffer *buf)
{
    std::string rstr;

    TidyBuffer text = {0};

    tidyBufInit(&text);

    switch(tidyNodeGetType(node)) {
    case TidyNode_Text:
        tidyNodeGetText(tdoc, node, &text);
        // qLogx()<<"text found:"<<text.size;
        if(text.bp[text.size-1] == '\n') {
            text.size -= 1;
        }
        tidyBufAppend(buf, text.bp, text.size);
        // qLogx()<<"curr buff size:"<<buf->size;
        break;
    default:
        switch(tidyNodeGetId(node)) {
        case TidyTag_A:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_P:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_Q:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_SPAN:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_H1:
        case TidyTag_H2:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_LI:
            get_node_plain_text(tdoc, tidyGetChild(node), buf);
            break;
        case TidyTag_PLAINTEXT:
            // qLogx()<<"plain text found";
            tidyNodeGetText(tdoc, node, &text);
            tidyBufAppend(buf, text.bp, text.size);
            break;
        case TidyTag_TD:
        case TidyTag_DIV:
            TidyNode child;
            for(child = tidyGetChild(node) ; child ; child = tidyGetNext(child)) {
                get_node_plain_text(tdoc, child, buf);
            }
            break;
        default:
            std::string str;
            if (tidyNodeGetId(node) == TidyTag_UNKNOWN
                    || tidyNodeGetId(node) == 17) {
                TidyBuffer tbuf;
                tidyBufInit(&tbuf);
                tidyNodeGetText(this->mdoc, node, &tbuf);
                str = std::string((char*)tbuf.bp, tbuf.size);
                tidyBufFree(&tbuf);

            }
            qLogx()<<"Unknown html tag:"<<tidyNodeGetId(node)<<"|"<<str<<"|";// <<tidyNodeGetName(node);
            break;
        }

        // qLogx()<<"Unknwon html node type:"<<tidyNodeGetType(node);
        break;
    };
    tidyBufFree(&text);


    return rstr;
}

std::string Extractor::get_node_plain_text_r(TidyDoc tdoc, TidyNode node, TidyBuffer *buf)
{
    std::string rstr;

    if (node == NULL) {
        return rstr;
    }

    TidyBuffer text = {0};

    tidyBufInit(&text);

    switch(tidyNodeGetType(node)) {
    case TidyNode_Text:
        //q_debug()<<"text found";
        tidyNodeGetText(tdoc, node, &text);
        // qLogx()<<"text found:"<<text.size;
        if(text.bp[text.size-1] == '\n') {
            text.size -= 1;
        }
        tidyBufAppend(buf, text.bp, text.size);
        // qLogx()<<"curr buff size:"<<buf->size;
        break;
    default:
        if (node == NULL) {
        } else if (tidyNodeGetId(node) == TidyTag_BR) {
            tidyBufAppend(buf, (void*)"\n", 1);
        } else  {
            TidyNode child;
            for(child = tidyGetChild(node) ; child ; child = tidyGetNext(child)) {
                get_node_plain_text_r(tdoc, child, buf);
            }
        }
        // qLogx()<<"Unknwon html node type:"<<tidyNodeGetType(node);
        break;
    };
    tidyBufFree(&text);


    return rstr;
}

///////////////////////
bool Extractor::get_element_plain_text(const char *str_selectors, std::string &rstr)
{
    std::string tstr;

    TidyNode root_node;
    TidyNode tmp_node;
    TidyBuffer tbuf;
    CssSelector *selector = NULL;
    int rlevel = 0;

    if (!this->mdoc) {
        qLogx()<<"Very bad document, do nothing.";
        return false;
    }

    root_node = tidyGetHtml(this->mdoc);
    if (!root_node) {
        qLogx()<<"Can not get root node, ignore.";
        return false;
    }

    selector = this->create_selector(str_selectors);

    for (int i = 0; i < selector->selectors.size(); ++i) {
        selector->setn(i); // important

        qLogx()<<"Find node using selector:"<<i<<selector->node_name<<selector->attr_name<<selector->attr_value;
        rlevel = 0;
        tmp_node = this->find_node(this->mdoc, root_node, selector, &rlevel);

        if (tmp_node) {
            qLogx()<<"Content node search ok."<<rlevel<<" selector-n:"<<i
                  <<selector->node_name<<selector->attr_name<<selector->attr_value;

            tidyBufInit(&tbuf);
            // tidyNodeGetText(this->mdoc, tmp_node, &tbuf);
            this->get_node_plain_text_r(this->mdoc, tmp_node, &tbuf);
            rstr = std::string((char *)tbuf.bp, tbuf.size);
            tidyBufFree(&tbuf);

            qLogx()<<"Conent node text length:"<<rstr.length()<<tbuf.size;

            break;
        } else {
            qLogx()<<"Content node not found."<<i<<rlevel;
        }
    }

    delete selector;

    // this->strtrim(rstr);
    boost::algorithm::trim_if(rstr, boost::algorithm::is_any_of(this->trim_chars));

    return true;
}

std::string Extractor::get_rid()
{
    std::string rstr;

    char *idbuf = NULL;
    char *last_slash = NULL;
    char *last_dot = NULL;

    last_slash = strrchr(this->murl, '/');
    if (last_slash) {
        last_dot = strchr(last_slash, '.');
        if (last_dot) {
            idbuf = (char*)calloc(1, last_dot - last_slash + 1);
            strncpy(idbuf, last_slash+1, last_dot - last_slash - 1);
            rstr = std::string(idbuf);
            free(idbuf);
        }
    }

    return rstr;
}

std::string Extractor::get_ctime()
{
    std::string rstr;

    bool bret = false;

    bret = this->get_element_plain_text(this->ctime_selector, rstr);

    if (!bret) {

    }

    return rstr;

}

std::string Extractor::get_title()
{
    std::string rstr;

    bool bret = false;

    bret = this->get_element_plain_text(this->title_selector, rstr);

    if (!bret) {

    }

    return rstr;

//    std::string rstr;

//    Bool tbret;
//    TidyDoc tdoc;
//    TidyNode tnode;
//    TidyNode headn;
//    TidyBuffer tbuf;
//    int iret;

//    tdoc = tidyCreate();
//    iret = tidySetCharEncoding(tdoc, "UTF8");
//    if (iret != 0) {
//        qLogx()<<iret<<"Not very good";
//    }
//    iret = tidyOptSetBool(tdoc, TidyShowWarnings, ::no);
//    iret = tidyParseString(tdoc, this->mu8page);
//    if (iret != 0) {
//        qLogx()<<iret<<"Not very good"<<tidyWarningCount(tdoc)<<tidyErrorCount(tdoc);
//    }

//    headn = tidyGetHead(tdoc);
//    for (tnode = tidyGetChild(headn); tnode ; tnode = tidyGetNext(tnode)) {
//        ctmbstr name;
//        switch(tidyNodeGetType(tnode)) {
//        case TidyNode_Root:       name = "Root";                    break;
//        case TidyNode_DocType:    name = "DOCTYPE";                 break;
//        case TidyNode_Comment:    name = "Comment";                 break;
//        case TidyNode_ProcIns:    name = "Processing Instruction";  break;
//        case TidyNode_Text:       name = "Text";                    break;
//        case TidyNode_CDATA:      name = "CDATA";                   break;
//        case TidyNode_Section:    name = "XML Section";             break;
//        case TidyNode_Asp:        name = "ASP";                     break;
//        case TidyNode_Jste:       name = "JSTE";                    break;
//        case TidyNode_Php:        name = "PHP";                     break;
//        case TidyNode_XmlDecl:    name = "XML Declaration";         break;

//        case TidyNode_Start:
//        case TidyNode_End:
//        case TidyNode_StartEnd:
//        default:
//            name = tidyNodeGetName( tnode );
//            break;
//        }
//        qLogx()<<name;
//        if (strcasecmp(name, "title") == 0) {
//            tidyBufInit(&tbuf);
//            tbret = tidyNodeGetText(tdoc, tidyGetChild(tnode), &tbuf);
////            tbuf.bp[tbuf.size] = '\0';
//            rstr = std::string((char*)tbuf.bp, tbuf.size);
//            tidyBufClear(&tbuf);
//            tidyBufFree(&tbuf);
//            qLogx()<<"title:"<<rstr;
//            break;
//        }
//    }

//    tidyRelease(tdoc);

//    return rstr;

}

std::string Extractor::get_content()
{
    std::string rstr;

    int iret = -1;
    bool bret = false;

    // bret = this->get_element_plain_text(this->content_selector, rstr);

    std::string str;
    TidyBuffer tbuf;
    tidyBufInit(&tbuf);
    iret = tidySaveBuffer(this->mdoc, &tbuf);
    str = std::string((char*)tbuf.bp, tbuf.size);
    tidyBufFree(&tbuf);
    qLogx()<<"src length:"<<str.length()<<this->mdoc;// <<str;

    iret = parse_plain_text(str, rstr, 172);

    qLogx()<<"extor text:"<<rstr.length()<<rstr;

    if (!bret) {

    }

    if (0) {
        TidyNode root_node;
        TidyNode tmp_node;
        TidyBuffer tbuf;
        CssSelector *selector = NULL;
        int rlevel = 0;

        selector = this->create_selector(this->content_selector);
        if (!this->mdoc) {
            qLogx()<<"Very bad document, do nothing.";
            return rstr;
        }

        root_node = tidyGetHtml(this->mdoc);
        if (!root_node) {
            qLogx()<<"Can not get root node, ignore.";
            delete selector;
            return rstr;
        }
        tmp_node = this->find_node(this->mdoc, root_node, selector, &rlevel);

        if (tmp_node) {
            qLogx()<<"Content node search ok."<<rlevel;

            tidyBufInit(&tbuf);
            // tidyNodeGetText(this->mdoc, tmp_node, &tbuf);
            this->get_node_plain_text_r(this->mdoc, tmp_node, &tbuf);
            rstr = std::string((char *)tbuf.bp, tbuf.size);
            tidyBufFree(&tbuf);

            qLogx()<<"Conent node text length:"<<rstr.length();
        } else {
            qLogx()<<"Content node not found."<<rlevel;
        }

        delete selector;
    }


    return rstr;

}

std::string Extractor::get_author()
{
    std::string rstr;

    bool bret = false;

    bret = this->get_element_plain_text(this->author_selector, rstr);

    if (!bret) {

    }

    return rstr;

}

std::string Extractor::get_category()
{
    std::string rstr;

    bool bret = false;

    bret = this->get_element_plain_text(this->category_selector, rstr);

    if (!bret) {

    }

    return rstr;

}

std::string Extractor::get_view_count()
{
    std::string rstr;


    bool bret = false;

    bret = this->get_element_plain_text(this->view_count_selector, rstr);

    if (!bret) {

    }

    return rstr;

}

std::string Extractor::get_comment_count()
{
    std::string rstr;

    bool bret = false;

    bret = this->get_element_plain_text(this->comment_count_selector, rstr);

    if (!bret) {

    }

    return rstr;

}

std::string Extractor::strrtrim(std::string &str)
{
    int trim_len = 0;
    char ch;

    for (int i = 0; i < str.length(); ++i) {
        ch = str.at(str.length()-1-i);
        if (ch == ' ' || ch == '\r' || ch == '\n') {
            continue;
        } else {
            trim_len = i;
            break;
        }
    }

    str.resize(str.length() - trim_len);

    return str;
}

std::string Extractor::strltrim(std::string &str)
{
    int trim_len = 0;
    char ch;

    for (int i = 0; i < str.length(); ++i) {
        ch = str.at(i);
        if (ch == ' ' || ch == '\r' || ch == '\n') {
            continue;
        } else {
            trim_len = i;
            break;
        }
    }

    str = str.substr(trim_len);

    return str;
}

std::string && Extractor::strtrim(std::string &str)
{
    std::string tstr;

    tstr = strltrim(str);
    tstr = strrtrim(str);


    return std::move(tstr);
}

time_t time_from_string(const std::string &ts)
{
    time_t t = 0;

    const char *tfmt = "%Y-%m-%d %H:%M:%S";
    char *p;
    struct tm tm = {0};

    p = strptime(ts.c_str(), tfmt, &tm);

    t = ::mktime(&tm);

    if (t > time(NULL)) {
        t = time(NULL);
    }

    // qLogx()<<"tfs:"<<ts<<t;

    return t;
}

