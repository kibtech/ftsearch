// Larbin
// Sebastien Ailleret
// 23-11-99 -> 15-02-01

#include <iostream>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "options.h"

#include "global.h"
#include "types.h"
#include "utils/url.h"
#include "utils/connexion.h"
#include "fetch/hashTable.h"

#include "extor/stdlog.h"


/* constructor */
hashTable::hashTable (bool create) {

    this->uhs.set_empty_key(std::string());
    this->uihs.set_empty_key(0);

    this->m_use_hash_hash = true; //
    if (create) {
    } else {
        FILE *hfp = fopen("sparsehashtable.bak", "r+");
        if (hfp == NULL) {
            qLogx()<<"Cannot find hashtable.bak, restart from scratch";
        } else {
            if (this->m_use_hash_hash) {
                this->uihs.read_metadata<FILE>(hfp);
            } else {
                this->uhs.read_metadata<FILE>(hfp);
            }
            fclose(hfp);
        }
    }

    this->size = 0;
    table = NULL;
    if (0) {
        ssize_t total = hashSize/8;
        table = new char[total];
        if (create) {
            for (ssize_t i=0; i<hashSize/8; i++) {
                table[i] = 0;
            }
        } else {
            int fds = open("hashtable.bak", O_RDONLY);
            if (fds < 0) {
                std::cerr << "Cannot find hashtable.bak, restart from scratch\n";
                for (ssize_t i=0; i<hashSize/8; i++) {
                    table[i] = 0;
                }
            } else {
                ssize_t sr = 0;
                while (sr < total) {
                    ssize_t tmp = read(fds, table+sr, total-sr);
                    if (tmp <= 0) {
                        std::cerr << "Cannot read hashtable.bak : "
                                  << strerror(errno) << std::endl;
                        exit(1);
                    } else {
                        sr += tmp;
                    }
                }
                close(fds);
            }
        }
    }
}

/* destructor */
hashTable::~hashTable () {
    if (table) {
        delete [] table;
    }
    this->uhs.clear();
    this->uihs.clear();
}

/* save the hashTable in a file */
//void hashTable::save() {
//  rename("hashtable.bak", "hashtable.old");
//  int fds = creat("hashtable.bak", 00600);
//  if (fds >= 0) {
//    ecrireBuff(fds, table, hashSize/8);
//	close(fds);
//  }
//  unlink("hashtable.old");
//}

void hashTable::save()
{
    rename("sparsehashtable.bak", "sparsehashtable.old");
    FILE *hfp = fopen("sparsehashtable.bak", "w+");
    if (hfp == NULL) {
        qLogx()<<"Can not create save hash file.";
    } else {
        if (this->m_use_hash_hash) {
            this->uihs.write_metadata<FILE>(hfp);
        } else {
            this->uhs.write_metadata<FILE>(hfp);
        }
        fclose(hfp);
    }
    unlink("sparsehashtable.old");
}

/* test if this url is allready in the hashtable
 * return true if it has been added
 * return false if it has allready been seen
 */
//bool hashTable::test (url *U) {
//  unsigned int code = U->hashCode();
//  unsigned int pos = code / 8;
//  unsigned int bits = 1 << (code % 8);
//  return table[pos] & bits;
//}

/* set a url as present in the hashtable
 */
//void hashTable::set (url *U) {
//  unsigned int code = U->hashCode();
//  unsigned int pos = code / 8;
//  unsigned int bits = 1 << (code % 8);
//  table[pos] |= bits;
//}

void hashTable::set(url *U)
{
    std::string us = U->getUrl();
    this->uhs[us] = 1;
    this->uhs.insert(std::make_pair(us, 1));

    uint64_t hkey = CityHash64(U->getUrl(), strlen(U->getUrl()));
    this->uihs[hkey] = 1;
    this->uihs.insert(std::make_pair(hkey, 1));
}

/* add a new url in the hashtable
 * return true if it has been added
 * return false if it has allready been seen
 */
//bool hashTable::testSet (url *U) {
//  unsigned int code = U->hashCode();
//  unsigned int pos = code / 8;
//  unsigned int bits = 1 << (code % 8);
//  int res = table[pos] & bits;
//  table[pos] |= bits;
//  return !res;
//}

bool hashTable::testSet(url *U)
{
    if (this->m_use_hash_hash) {
        uint64_t hkey = CityHash64(U->getUrl(), strlen(U->getUrl()));

        if (this->uihs.find(hkey) == this->uihs.end()) {
            this->uihs[hkey] = 1;
            this->uihs.insert(std::pair<uint64_t, char>(hkey, 1));
            return true;
        } else {
            return false;
        }
    } else {
        std::string us = U->getUrl();

        if (this->uhs.find(us) == this->uhs.end()) {
            this->uhs[us] = 1;
            this->uhs.insert(std::pair<std::string, char>(us, 1));
            return true;
        } else {
            return false;
        }
    }
    assert(1==2);
    return true;
}
