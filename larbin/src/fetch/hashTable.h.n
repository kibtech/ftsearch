// Larbin
// Sebastien Ailleret
// 23-11-99 -> 14-01-00

/* class hashTable
 * This class is in charge of making sure we don't crawl twice the same url
 */

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include "types.h"
#include "utils/url.h"

#include <unordered_map>
#include "cityhash/city.h"
#include "sparsehash/google/dense_hash_map"
#include "sparsehash/google/dense_hash_set"
#include "sparsehash/google/sparse_hash_map"
#include "sparsehash/google/sparse_hash_set"

struct eqstr
{
    bool operator()(const char* s1, const char* s2) const
    {
        return (s1 == s2) || (s1 && s2 && strcmp(s1, s2) == 0);
    }
};

class hashTable {
 private:
  ssize_t size;
  char *table;

  // google::dense_hash_map<std::string, char, std::hash<std::string>> uhs;
  ::google::dense_hash_map<std::string, char> uhs;
  ::google::dense_hash_map<uint64_t, char> uihs;
  bool m_use_hash_hash; // 直接使用字符串还是使用整数类型

 public:
  /* constructor */
  hashTable (bool create);

  /* destructor */
  ~hashTable ();

  /* save the hashTable in a file */
  void save();

  /* test if this url is allready in the hashtable
   * return true if it has been added
   * return false if it has allready been seen
   */
  // bool test (url *U);

  /* set a url as present in the hashtable
   */
  void set (url *U);

  /* add a new url in the hashtable
   * return true if it has been added
   * return false if it has allready been seen
   */
  bool testSet (url *U);
};

#endif // HASHTABLE_H
