// Larbin
// Sebastien Ailleret
// 23-11-99 -> 15-02-01

#include <iostream>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "options.h"

#include "global.h"
#include "types.h"
#include "utils/url.h"
#include "utils/connexion.h"
#include "fetch/hashTable.h"

#include "extor/stdlog.h"

/* constructor */
hashTable::hashTable (bool create) {
    ssize_t total = hashNSize/8;
    table = new char[total];
    if (create) {
        for (ssize_t i=0; i<hashNSize/8; i++) {
            table[i] = 0;
        }
    } else {
        int fds = open("hashtable.bak", O_RDONLY);
        if (fds < 0) {
            std::cerr << "Cannot find hashtable.bak, restart from scratch\n";
            for (ssize_t i=0; i<hashNSize/8; i++) {
                table[i] = 0;
            }
        } else {
            ssize_t sr = 0;
            while (sr < total) {
                ssize_t tmp = read(fds, table+sr, total-sr);
                if (tmp <= 0) {
                    std::cerr << "Cannot read hashtable.bak : "
                              << strerror(errno) << std::endl;
                    exit(1);
                } else {
                    sr += tmp;
                }
            }
            close(fds);
        }
    }

    this->uihs.set_empty_key(0);
    // this->ulhs.set_empty_key(0);
    this->ulhs.set_deleted_key(0);
    if (create) {

    } else {
        FILE *hfp = fopen("sparseht.bak", "r+");
        if (hfp == NULL) {
            qLogx()<<"Can not open sparseht.bak:"<<errno<<strerror(errno);
        } else {
            this->uihs.read_metadata<FILE>(hfp);
            fclose(hfp);
        }
    }
}

/* destructor */
hashTable::~hashTable () {
    delete [] table;
}

/* save the hashTable in a file */
void hashTable::save() {
    rename("hashtable.bak", "hashtable.old");
    int fds = creat("hashtable.bak", 00600);
    if (fds >= 0) {
        ecrireBuff(fds, table, hashNSize/8);
        close(fds);
    }
    unlink("hashtable.old");

    ////////////
    FILE *hfp = fopen("sparseht.bak", "w+");

    if (hfp == NULL) {
        qLogx()<<"Can not open sparseht.bak:"<<errno<<strerror(errno);
    } else {
        rename("sparseht.bak", "sparseht.old");
        this->uihs.write_metadata<FILE>(hfp);
        unlink("sparseht.old");
        fclose(hfp);
    }

}

/* test if this url is allready in the hashtable
 * return true if it has been added
 * return false if it has allready been seen
 */
bool hashTable::test (url *U) {
    unsigned int code = U->hashCode();
    unsigned int pos = code / 8;
    unsigned int bits = 1 << (code % 8);

    ////
    if (this->uihs.find(code) == this->uihs.end()) {
        // return true;
    } else {
        // return false;
    }

    return table[pos] & bits;
}

/* set a url as present in the hashtable
 */
void hashTable::set (url *U) {
    unsigned int code = U->hashCode();
    unsigned int pos = code / 8;
    unsigned int bits = 1 << (code % 8);
    table[pos] |= bits;

    ////////
    this->uihs.insert(std::make_pair(code, 1));
    if (this->uihs.size() > hashNSize) {
        qLogx()<<"Hash table overflow: "<<this->uihs.size();
    }
}

/* add a new url in the hashtable
 * return true if it has been added
 * return false if it has allready been seen
 */
bool hashTable::testSet (url *U) {
    unsigned int code = U->hashCode();
    unsigned int pos = code / 8;
    unsigned int bits = 1 << (code % 8);
    int res = table[pos] & bits;
    table[pos] |= bits;


    ////////
    if (this->uihs.find(code) == this->uihs.end()) {
        this->uihs.insert(std::pair<uint, char>(code,1));
        if (this->uihs.size() > hashNSize) {
            qLogx()<<"Hash table overflow: "<<this->uihs.size();
        }
        // return true;
    } else {
        // return false;
    }

    return !res;
}

