// Larbin
// Sebastien Ailleret
// 14-12-99 -> 19-03-02

#include <unistd.h>
#include <iostream>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "options.h"

#include "types.h"
#include "global.h"
#include "utils/text.h"
#include "utils/url.h"
#include "utils/string.h"
#include "utils/Vector.h"
#include "fetch/site.h"
#include "fetch/file.h"
#include "fetch/fetchOpen.h"
#include "fetch/checker.h"

#include "utils/debug.h"

#include <zlib.h>
#include "extor/stdlog.h"
#include "fetch/httpheader.h"

#define ANSWER 0
#define HEADERS 1
#define HEADERS30X 2
#define HTML 3
#define SPECIFIC 4
#define DEFLATE 5

enum SimpleState {
    SS_ANSWER = 0,
    SS_HEADERS,
    SS_HEADERS30X,
    SS_HTML,
    SS_SPECIFIC,
    SS_DEFLATE
};

#define LINK 0
#define BASE 1


/***********************************
 * implementation of file
 ***********************************/

file::file (Connexion *conn) {
  buffer = conn->buffer;
  pos = 0;
  posParse = buffer;
}

file::~file () {
}

/***********************************
 * implementation of robots
 ***********************************/

/** Constructor
 */
robots::robots (NamedSite *server, Connexion *conn) : file(conn) {
  newPars();
  this->server = server;
  answerCode = false;
  isRobots = true;
}

/** Destructor
 */
robots::~robots () {
  delPars();
  // server is not deleted on purpose
  // it belongs to someone else
}

/** we get some more chars of this file
 */
int robots::endInput () {
  return 0;
}

/** input and parse headers
 */
int robots::inputHeaders (int size) {
  pos += size;
  if (!answerCode && pos > 12) {
    if (buffer[9] == '2') {
      answerCode = true;
    } else {
      errno = err40X;
      return 1;
    }
  }
  if (pos > maxRobotsSize) {
	// no more input, forget the end of this file
	errno = tooBig;
	return 1;
  } else {
	return 0;
  }
}

/** parse the robots.txt
 */
void robots::parse (bool isError) {
  if (answerCode && parseHeaders()) {
    siteRobots();
    buffer[pos] = 0;
	if (isError) {
	  // The file could be incomplete, delete last token
	  // We could have Disallow / instead of Disallow /blabla
	  for (uint i=pos-1; i>0 && !isspace(buffer[i]); i--) {
		buffer[i] = ' ';
	  }
	}
    parseRobots();
  }
}

/** test http headers
 * return true if OK, false otherwise
 */
bool robots::parseHeaders () {
  for(posParse = buffer+9; posParse[3] != 0; posParse++) {
    if ((posParse[0] == '\n' && 
         (posParse[1] == '\n'
          || posParse[2] == '\n'))
        || (posParse[0] == '\r' &&
            (posParse[1] == '\r'
             || posParse[2] == '\r'))) {
      return true;
    }
  }
  return false;
}

/** try to understand the file
 */
void robots::parseRobots () {
  robotsOK();
#ifndef NOSTATS
  bool goodfile = true;
#endif // NOSTATS
  server->forbidden.recycle();
  uint items = 0; // size of server->forbidden
  // state
  // 0 : not concerned
  // 1 : weakly concerned
  // 2 : strongly concerned
  int state = 1;
  char *tok = nextToken(&posParse, ':');
  while (tok != NULL) {
	if (!strcasecmp(tok, "useragent") || !strcasecmp(tok, "user-agent")) {
	  if (state == 2) {
		// end of strong concern record => the end for us
		return;
	  } else {
		state = 0;
		// what is the new state ?
		tok = nextToken(&posParse, ':');
		while (tok != NULL
			   && strcasecmp(tok, "useragent")
			   && strcasecmp(tok, "user-agent")
			   && strcasecmp(tok, "disallow")) {
          if (caseContain(tok, global::userAgent)) {
            state = 2;
          } else if (state == 0 && !strcmp(tok, "*")) {
            state = 1;
          }
		  tok = nextToken(&posParse, ':');
		}
	  }
	  if (state) {
		// delete old forbidden : we've got a better record than older ones
		server->forbidden.recycle();
		items = 0;
	  } else {
        // forget this record
        while (tok != NULL
			   && strcasecmp(tok, "useragent")
			   && strcasecmp(tok, "user-agent")) {
          tok = nextToken(&posParse, ':');
        }
      }
	} else if (!strcasecmp(tok, "disallow")) {
      tok = nextToken(&posParse, ':');
      while (tok != NULL
             && strcasecmp(tok, "useragent")
             && strcasecmp(tok, "user-agent")
             && strcasecmp(tok, "disallow")) {
        // add nextToken to forbidden
        if (items++ < maxRobotsItem) {
          // make this token a good token
          if (tok[0] == '*') { // * is not correct, / disallows everything
            tok[0] = '/';
          } else if (tok[0] != '/') {
            tok--;
            tok[0] = '/';
          }
          if (fileNormalize(tok)) {
            server->forbidden.addElement(newString(tok));
          }
        }
        tok = nextToken(&posParse, ':');
      }
	} else {
#ifndef NOSTATS
	  if (goodfile) {
        robotsOKdec();
		goodfile = false;
	  }
#endif // NOSTATS
	  tok = nextToken(&posParse, ':');
	}
  }
}


/*************************************
 * implementation of html
 *************************************/


/////////////////////////////////////////
#ifdef SPECIFICSEARCH

#include "fetch/specbuf.cc"

#define _newSpec() if (state==SPECIFIC) newSpec()
#define _destructSpec() if (state==SPECIFIC) destructSpec()
#define _endOfInput() if (state==SPECIFIC) return endOfInput()
#define _getContent() \
  if (state==SPECIFIC) return getContent(); \
  else return contentStart
#define _getSize() \
  if (state==SPECIFIC) return getSize(); \
  else return (buffer + pos - contentStart)

///////////////////////////////////////
#else // not a SPECIFICSEARCH

void initSpecific () { }

#define constrSpec() ((void) 0)
#define _newSpec() ((void) 0)
#define pipeSpec() 0
#define _endOfInput() ((void) 0)
#define _destructSpec() ((void) 0)
#define _getContent() return contentStart
#define _getSize() return (buffer + pos - contentStart)

#endif // SPECIFICSEARCH
/////////////////////////////////////////

#if CGILEVEL >= 1
#define notCgiChar(c) (c!='?' && c!='=' && c!='*')
#else
#define notCgiChar(c) true
#endif // CGILEVEL

/** Constructor
 */
html::html (url *here, Connexion *conn) : file(conn) {
  newPars();
  this->here = here;
  base = here->giveBase();
  state = ANSWER;
  isInteresting = false;
  constrSpec();
  pages();
  isRobots = false;

  m_hdr = NULL;
  m_zstm = NULL;
  m_uncpos = 0;
  m_uncbuf = buffer + maxPageSize/4 ;

//  deflate_content = false;
//  chunk_mode = false;
}

/** Destructor
 */
html::~html () {
  _destructSpec();
  delPars();
  delete here;
  delete base;

  if(m_hdr) {
      delete m_hdr;
  }
  if (m_zstm) {
      ::inflateEnd(m_zstm);
      free(m_zstm);
  }
}

/* get the content of the page */
char *html::getPage () {
  _getContent();
}

int html::getLength () {
  _getSize();
}

/* manage a new url : verify and send it */
void html::manageUrl (url *nouv, bool isRedir) {
  if (nouv->isValid()
      && filter1(nouv->getHost(), nouv->getFile())
      && (global::externalLinks || isRedir
          || !strcmp(nouv->getHost(), this->here->getHost()))) {
    // The extension is not stupid (gz, pdf...)
#ifdef LINKS_INFO
    links.addElement(nouv->giveUrl());
#endif // LINKS_INFO
    if (nouv->initOK(here)) {
      check(nouv);
    } else {
      // this url is forbidden for errno reason (set by initOK)
      answers(errno);
      delete nouv;
    }
  } else {
    // The extension is stupid
    delete nouv;
  }
}

/**********************************************/
/* This part manages command line and headers */
/**********************************************/

/** a string is arriving, treat it only up to the end of headers
 * return 0 usually, 1 if no more input and set errno accordingly
 */
int html::inputHeaders (int size) {
    bool bret;
    char tbuf[532] = {0};
    strncpy(tbuf, posParse, sizeof(tbuf)-1);
    // qLogx()<<pos<<size<<tbuf<<strlen(posParse);
    if (pos == 0) {
        qLogx()<<pos<<size<<tbuf<<strlen(posParse);
    } else {
        // qLogx()<<pos<<size<<strlen(posParse)<<buffer;
    }

  pos += size;
  buffer[pos] = 0;

  char *tptr = NULL;
  if (m_hdr == NULL && (tptr = strstr(buffer, "\r\n\r\n")) != NULL) {
      m_hdr = new HttpHeader();
      bret = m_hdr->parse(buffer);
      qLogx()<<"Parse header ret:"<<bret;
      if (!bret) {
          return 1;
      }

      posParse = tptr + 4; //
      contentStart = posParse;

      //// verify
      if (m_hdr->httpCode() >= 200 && m_hdr->httpCode() < 300) {

      } else if (m_hdr->httpCode() >= 300 && m_hdr->httpCode() < 400) {
          if (m_hdr->location()) {
              // read the location (do not decrease depth)
              url *nouv = new url(m_hdr->location(), here->getDepth(), base);
#ifdef URL_TAGS
              nouv->tag = here->tag;
#endif // URL_TAGS
              manageUrl(nouv, true);
              // we do not need more headers
              errno = err30X;
          } else {
              errno = err40X;
          }
          return 1;
      } else {
          errno = err40X;
          return 1;
      }

      ////
      if (m_hdr->contentLength() > maxPageSize) {
          errno = tooBig;
          return 1;
      }
  }

  if (m_hdr) {
      state = HTML;

      if (m_hdr->hasTransferEncoding() && m_hdr->transferEncoding() == HttpHeader::TE_CHUNKED) {
          // how to do
          this->unchunk();
      }
  }


//  char *posn;
//  while (posParse < buffer + pos) {
//    switch (state) {
//    case ANSWER:
//      posn = strchr(posParse, '\n');
//      if (posn != NULL) {
//        posParse = posn;
//        if (parseCmdline ()) {
//          return 1;
//        }
//        area = ++posParse;
//      } else {
//        return 0;
//      }
//      break;
//    case HEADERS:
//    case HEADERS30X:
//      posn = strchr(posParse, '\n');
//      if (posn != NULL) {
//        posParse = posn;
//        int tmp;
//        if (state == HEADERS)
//          tmp = parseHeader();
//        else tmp = parseHeader30X();
//        if (tmp) {
//            qLogx()<<""<<posParse<<deflate_content<<strlen(posParse)<<pos;
//            if (deflate_content) {
//                state = DEFLATE;
//                return 0;
//            } else {
//                return 1;
//            }
//        }
//        area = ++posParse;
//      } else {
//        return 0;
//      }
//      break;
//    case SPECIFIC:
//      return pipeSpec();
//    default:
//        // qLogx()<<pos<<size<<strlen(posParse);
//      return 0;
//    }
//  }
  return 0;
}

/*
  第一步，解析非gzip压缩的chunk块
  第二步，解析gzip压缩的chunk块。

  对非gzip的chunk,直接把表示chunk长度那一行删除掉。
  */

bool html::unchunk()
{
    qLogx()<<"haha";
    int ret = -1;
    int chunk_len = 0;
    char tbuf[16] = {0};
    char *chbuf = NULL;
    char uncbuf[maxPageSize] = {0};
    int unclen = 0;
    char *p1, *p2;
    char *chend = NULL;
    bool gzed = false;

    gzed = m_hdr->hasContentEncoding() && m_hdr->contentEncoding();
    if (m_zstm == NULL &&
            (m_hdr->contentEncoding() == HttpHeader::CE_GZIP
             || m_hdr->contentEncoding() == HttpHeader::CE_DEFLATE)) {
        m_zstm = (z_stream*)calloc(1, sizeof(z_stream));
        memset(m_zstm, 0, sizeof(z_stream));
        /* allocate inflate state */
        m_zstm->zalloc = Z_NULL;
        m_zstm->zfree = Z_NULL;
        m_zstm->opaque = Z_NULL;
        m_zstm->avail_in = 0;
        m_zstm->next_in = Z_NULL;
        if (m_hdr->contentEncoding() == HttpHeader::CE_GZIP) {
            ret = inflateInit2(m_zstm, 16+MAX_WBITS); // must use inflateInit2
        } else {
            ret = inflateInit(m_zstm);
        }
        if (ret != Z_OK) {
            qLogx()<<"gzdec init error:"<<ret<<(zError(ret));
            return false; // return ret;
        }
    }


    while (posParse < buffer + pos) {
        p1 = strchr(posParse, '\n');
        if (p1 == NULL) {
            break;
        }
        memcpy(tbuf, posParse, p1-1-posParse);
        tbuf[p1-1-posParse] = 0;
        chunk_len = strtol(tbuf, NULL, 16);
        qLogx()<<"chunk len:" << "\""<< tbuf << "\"" << chunk_len<<(p1-1-posParse);
        if (chunk_len == 0) {
            qLogx()<<"Chunked content ended."<<this->pos;
            break;
        }

        chend = (p1+1 + chunk_len);
        if (chend - buffer >= pos) {
            qLogx()<<"chunk not really readed.";
            break;
        }

        chbuf = (char*)calloc(1, chunk_len + 1);
        memcpy(chbuf, p1+1, chunk_len);
        chbuf[chunk_len] = 0;
        qLogx()<<"chunk len:" << "\""<< tbuf << "\"" << chunk_len;
              // <<chbuf
                ;

        if (gzed == true) { // 如果需要，解压chunk
            memset(uncbuf, 0, sizeof(uncbuf));
            unclen = maxPageSize;
            // ret = ::gunzip_stream((unsigned char *)chbuf, chunk_len, (unsigned char *)uncbuf, &unclen);
            ret = this->gunzip_chunk((unsigned char *)chbuf, chunk_len, (unsigned char *)uncbuf, &unclen);
            qLogx()<<ret<<unclen<<strlen(uncbuf)<<chunk_len<<ret<<unclen;
            if (ret != Z_OK) {
                qLogx()<<"gzdec uncompress error:"<<ret<<(zError(ret))
                         <<this->getUrl()->getUrl()
                         ;
                assert(ret == Z_OK);
                return false; // return ret;
            } else {
//                if (unclen == maxPageSize) {
//                    unclen = strlen(uncbuf); // 难道说就这个引起的大问题。
//                }
                if (unclen != strlen(uncbuf)) {
                    // assert(unclen == strlen(uncbuf));
                    unclen = std::min<int>(strlen(uncbuf), unclen);
                }
                // 越界检测
                if (m_uncpos + unclen> maxPageSize*3/4) {
                    qLogx()<<"Page is too big.>"<<(maxPageSize*3/4)<<m_uncpos<<unclen;
                } else {
                    memcpy(m_uncbuf + m_uncpos, uncbuf, unclen);
                    m_uncpos += unclen;
                    m_uncbuf[m_uncpos] = 0;
                }
            }
        } else {
            qLogx()<<"Override not gziped chunk length line.";
            memset(posParse, ' ', p1-1-posParse+2);
        }
        if (chbuf) free(chbuf); chbuf = NULL;
        posParse = (chend + 2);
    }

    return true;
}

/*
  dlen, in/out, 返回解压后的结果长度
  */
int html::gunzip_chunk(unsigned char *in, size_t ilen, unsigned char *out, int *dlen)
{
    unsigned char *pin = in + 0;
    ilen = ilen - 0;
    int have_len = *dlen;
    int ret = 0;
    qLogx()<<in<<out<<ilen<<strlen((char*)in);

    m_zstm->avail_in = ilen;
    m_zstm->next_in = pin;

    m_zstm->avail_out = have_len;
    m_zstm->next_out = out;

    ret = inflate(m_zstm, Z_NO_FLUSH);
    assert(ret != Z_STREAM_ERROR);

    switch (ret) {
    case Z_NEED_DICT:
        qLogx()<<""<<ret;
        ret = Z_DATA_ERROR;     /* and fall through */
    case Z_DATA_ERROR:
        qLogx()<<""<<ret;
    case Z_MEM_ERROR:
        qLogx()<<""<<ret;
        return ret;
    }


    // qLogx()<<ret<<m_zstm->avail_out<<(*dlen-m_zstm->avail_out)<<*dlen<<strlen((char*)out)<<(char*)pin<<"///";
    // qLogx()<<(char*)out;
    // qLogx()<<ret<<m_zstm->avail_out<<have_len<<(*dlen - m_zstm->avail_out);
    if (ret == Z_STREAM_END || ret == Z_OK) {
        *dlen = *dlen - m_zstm->avail_out;
        return Z_OK;
    } else {
        return Z_DATA_ERROR;
    }
    return (ret == Z_STREAM_END || ret == Z_OK) ? Z_OK : Z_DATA_ERROR;
    return 0;
}


/** parse the answer code line */
int html::parseCmdline () {
  if (posParse - buffer >= 12) {
    switch (buffer[9]) {
    case '2':
      state = HEADERS;
      break;
    case '3':
      state = HEADERS30X;
      break;
    default:
      errno = err40X;
      return 1;
    }
  } else {
    errno = earlyStop;
    return 1;
  }
  return 0;
}

/** parse a line of header
 * @return 0 if OK, 1 if we don't want to read the file
 */
int html::parseHeader () {
  if (posParse - area < 2) {
	// end of http headers
#ifndef FOLLOW_LINKS
    state = SPECIFIC;
#elif defined(SPECIFICSEARCH)
    if (isInteresting) {
      state = SPECIFIC;
    } else {
      state = HTML;
    }
#else // not a SPECIFICSEARCH
    state = HTML;
#endif // SPECIFICSEARCH
    qLogx()<<"header ended.";
    contentStart = posParse + 1;
    *(posParse-1) = 0;
    _newSpec();
  } else {
    *posParse = 0;
    here->addCookie(area);
    *posParse = '\n';

    if (verifType ()) return 1;
    if (verifLength()) return 1;
    // if (verifyContentEncoding()) return 1;
  }
  return 0;
}

/** function called by parseHeader
 * parse content-type
 * return 1 (and set errno) if bad type, 0 otherwise
 * can toggle isInteresting
 */
#define errorType() errno=badType; return 1

#ifdef ANYTYPE
#define checkType() return 0
#elif defined(IMAGES)
#define checkType() if (startWithIgnoreCase("image", area+14)) { \
    return 0; \
  } else { errorType (); }
#else
#define checkType() errorType()
#endif

int html::verifType () {
  if (startWithIgnoreCase("content-type: ", area)) {
    // Let's read the type of this doc
    if (!startWithIgnoreCase("text/html", area+14)) {
#ifdef SPECIFICSEARCH
      if (matchContentType(area+14)) {
        interestingSeen();
        isInteresting = true;
      } else {
        checkType();
      }
#else // SPECIFICSEARCH
      checkType();
#endif // SPECIFICSEARCH
    }
  }
  return 0;
}

/** function called by parseHeader
 * parse content-length
 * return 1 (and set errno) if too long file, 0 otherwise
 */
int html::verifLength () {
#ifndef SPECIFICSEARCH
  if (startWithIgnoreCase("content-length: ", area)) {
    int len = 0;
    char *p = area+16;
    while (*p >= '0' && *p <= '9') {
      len = len*10 + *p -'0';
      p++;
    }
    if (len > maxPageSize) {
      errno = tooBig;
      return 1;
    }
  }
#endif // SPECIFICSEARCH
  return 0;
}

/** parse a line of header (ans 30X) => just look for location
 * @return 0 if OK, 1 if we don't want to read the file
 */
int html::parseHeader30X () {
  if (posParse - area < 2) {
	// end of http headers without location => err40X
    errno = err40X;
    return 1;
  } else {
	if (startWithIgnoreCase("location: ", area)) {
      int i=10;
      while (area[i]!=' ' && area[i]!='\n' && area[i]!='\r'
             && notCgiChar(area[i])) {
        i++;
      }
      if (notCgiChar(area[i])) {
        area[i] = 0; // end of url
        // read the location (do not decrease depth)
        url *nouv = new url(area+10, here->getDepth(), base);
#ifdef URL_TAGS
        nouv->tag = here->tag;
#endif // URL_TAGS
        manageUrl(nouv, true);
        // we do not need more headers
      }
      errno = err30X;
      return 1;
	}
  }
  return 0;
}

/*********************************************/
/* This part manages the content of the file */
/*********************************************/
//#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
//#  include <fcntl.h>
//#  include <io.h>
//#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
//#else
//#  define SET_BINARY_MODE(file)
//#endif
#define CHUNK 5120
int gunzip_stream(unsigned char *in, size_t ilen, unsigned char *out, int *dlen)
{
    unsigned char *pin = in + 0;
    ilen = ilen - 0;
    qLogx()<<in<<out<<ilen<<strlen((char*)in);
//    int iret;
//    int olen = maxPageSize*10;

//    iret = uncompress(out, (uLongf*)&olen, pin, ilen);

//    qLogx()<<"Uncompress iret:"<<iret<<olen;
//    return iret;

    int ret, flush;
    unsigned have;
    z_stream strm;
    // unsigned char in[CHUNK];
    // unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit2(&strm, 16+MAX_WBITS); // must use inflateInit2
    if (ret != Z_OK)
        return ret;

    size_t tpos = 0;
    size_t dpos = 0;
    /* decompress until deflate stream ends or end of file */
    do {
        qLogx()<<""<<tpos<<dpos;
        // strm.avail_in = fread(in, 1, CHUNK, source);
//        if (ferror(source)) {
//            (void)inflateEnd(&strm);
//            return Z_ERRNO;
//        }
        strm.avail_in = CHUNK <= (ilen - tpos) ? CHUNK : (ilen - tpos);
        if (strm.avail_in == 0)
            break;
        // strm.next_in = in;
        strm.next_in = pin + tpos;
        tpos += strm.avail_in;
        /* run inflate() on input until output buffer not full */
        do {
            qLogx()<<""<<tpos<<dpos;
            strm.avail_out = CHUNK;
            // strm.next_out = out;
            strm.next_out = out + dpos;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                qLogx()<<""<<tpos<<dpos<<ret;
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
                qLogx()<<""<<tpos<<dpos<<ret;
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
                qLogx()<<""<<tpos<<dpos<<ret;
                return ret;
            }

            have = CHUNK - strm.avail_out;
            dpos += have;
            *dlen = dpos;
            qLogx()<<""<<tpos<<dpos;
//            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
//                (void)inflateEnd(&strm);
//                return Z_ERRNO;
//            }

        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;

    return 0;
}

/** file download is complete, parse the file (headers already done)
 * return 0 usually, 1 if there was an error
 */
int html::endInput () {
    int iret;
    // 如果是chunk/gzip格式的，则要把unchunk/ungzip的内容放到所需要位置。
    // 然后，设置正确的state状态为HTML
    // qLogx()<<deflate_content<<pos<<contentStart<<(posParse-buffer)<<state;
    if (m_hdr == NULL) {
        qLogx()<<"what problem: NULL response header???"<<this->getUrl()->getUrl();
    } else {
        qLogx()<<m_hdr->hasContentEncoding()<<pos<<(posParse-buffer)<<state<<m_uncpos<<(m_uncbuf-buffer);// <<m_uncbuf;
        if (m_hdr->hasContentEncoding() && m_hdr->contentEncoding() == HttpHeader::CE_GZIP) {
            // memmove(contentStart, m_uncbuf, m_uncpos);
            // contentStart[m_uncpos] = 0;
            contentStart = m_uncbuf;
            contentStart[m_uncpos] = 0;
            pos = (m_uncbuf - buffer) + m_uncpos;
        }
        posParse = contentStart;
    }

  if (state <= HEADERS) {
    errno = earlyStop;
    return 1;
  }
  if (state == HEADERS30X) {
    errno = err40X;
    return 1;
  }
#ifdef NO_DUP
  if (!global::hDuplicate->testSet(posParse)) {
    errno = duplicate;
    return 1;
  }
#endif // NO_DUP
  buffer[pos] = 0;
  _endOfInput();

  // now parse the html
  parseHtml();
  return 0;
}

/* parse an html page */
void html::parseHtml () {
    qLogx()<<""<<pos<<this->getUrl()->getUrl();// <<posParse<<contentStart;
  while ((posParse=strchr(posParse, '<')) != NULL) {
    if (posParse[1] == '!') {
      if (posParse[2] == '-' && posParse[3] == '-') {
        posParse += 4;
        parseComment();
      } else {
        // nothing...
        posParse += 2;
      }
    } else {
      posParse++;
      parseTag();
    }
  }
}

/* skip a comment */
void html::parseComment() {
  while ((posParse=strchr(posParse, '-')) != NULL) {
    if (posParse[1] == '-' && posParse[2] == '>') {
      posParse += 3;
      return;
    } else {
      posParse++;
    }
  }
  posParse = buffer+pos;
}

/* macros used by the following functions */
#define skipSpace() \
  while (*posParse == ' ' || *posParse == '\n' \
         || *posParse == '\r' || *posParse == '\t') { \
    posParse++; \
  }
#define skipText() \
  while (*posParse != ' ' && *posParse != '\n' && *posParse != '>' \
         && *posParse != '\r' && *posParse != '\t' && *posParse != 0) { \
    posParse++; \
  }
#define nextWord() skipText(); skipSpace()
#define thisCharIs(i, c) (c == (posParse[i]|32))
#define isTag(t, p, a, i) if (t) { \
      param = p; \
      action = a; \
      posParse += i; \
    } else { \
      posParse++; \
      return; \
    }

/** Try to understand this tag */
void html::parseTag () {
  skipSpace();
  char *param=NULL; // what parameter are we looking for
  int action=-1;
  // read the name of the tag
  if (thisCharIs(0, 'a')) { // a href
    param = "href";
    action = LINK;
    posParse++;
  } else if (thisCharIs(0, 'l')) {
    isTag(thisCharIs(1, 'i') && thisCharIs(2, 'n') && thisCharIs(3, 'k'),
          "href", LINK, 4);
  } else if (thisCharIs(0, 'b')) { // base href
    isTag(thisCharIs(1, 'a') && thisCharIs(2, 's') && thisCharIs(3, 'e'),
          "href", BASE, 4);
  } else if (thisCharIs(0, 'f')) { // frame src
    isTag(thisCharIs(1, 'r') && thisCharIs(2, 'a')
          && thisCharIs(3, 'm') && thisCharIs(4, 'e'),
          "src", LINK, 5);
#ifdef IMAGES
  } else if (thisCharIs(0, 'i')) { // img src
    isTag(thisCharIs(1, 'm') && thisCharIs(2, 'g'), "src", LINK, 3);
#endif // IMAGES
  } else {
    return;
  }
  // now find the parameter
  assert(param != NULL);
  skipSpace();
  for (;;) {
    int i=0;
    while (param[i]!=0 && thisCharIs(i, param[i])) i++;
    posParse += i;
    if (posParse[i]=='>' || posParse[i]==0) return;
    if (param[i]==0) {
      parseContent(action);
      return;
    } else {
      // not the good parameter
      nextWord();
    }
  }
}

/** read the content of an interesting tag */
void html::parseContent (int action) {
    char dbuf[200] = {0};

  posParse++;
  while (*posParse==' ' || *posParse=='=') posParse++;
  if (*posParse=='\"' || *posParse=='\'') posParse++;
  area = posParse;
  char *endItem = area + maxUrlSize;
  if (endItem > buffer + pos) endItem = buffer + pos;
  while (posParse < endItem && *posParse!='\"' && *posParse!='\''
         && *posParse!='\n' && *posParse!=' ' && *posParse!='>'
         && *posParse!='\r' && *posParse!='\t' && notCgiChar(*posParse)) {
    if (*posParse == '\\') *posParse = '/';    // Bye Bye DOS !
    posParse++;
  }
  if (posParse == buffer + pos) {
    // end of file => content may be truncated => forget it
    return;
  } else if (posParse < endItem && notCgiChar(*posParse)) {
    // compute this url (not too long and not cgi)
    char oldchar = *posParse;
    *posParse = 0;
    switch (action) {
    case LINK:
      // try to understand this new link
      manageUrl(new url(area, here->getDepth()-1, base), false);
      break;
    case BASE:
      // This page has a BASE HREF tag
      {
        uint end = posParse - area - 1;
        if (posParse == area) {
            qLogx()<<this->getUrl()->getUrl()<<end;
            qLogx()<<(void*)posParse<<(void*)area;
            strncpy(dbuf, area-12, 60);
            qLogx()<<dbuf;
            qLogx()<<"Invalid/Empty Base's HREF NODE.ignored. <base href=''>";
            // qLogx()<<area[end]; // for test stop, should comment when test done.
            break;
        }
        // qLogx()<<area[end];
        while (end > 7 && area[end] != '/') end--; // 7 because http://
        // qLogx()<<end<<area[end];
        if (end > 7) { // this base looks good
          end++;
          char tmp = area[end];
          area[end] = 0;
          url *tmpbase = new url(area, 0, (url *) NULL);
          area[end] = tmp;
          delete base;
          if (tmpbase->isValid()) {
            base = tmpbase;
          } else {
            delete tmpbase;
            base = NULL;
          }
        }
      }
      break;
    default: assert(false);
    }
    *posParse = oldchar;
  }
  posParse++;
}
