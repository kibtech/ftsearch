#include <string.h>

#include "extor/stdlog.h"

#include "httpheader.h"

HttpHeader::HttpHeader()
{
    this->m_header = this->m_end = NULL;

    this->m_has_content_length = false;
    this->m_content_length = 0;

    this->m_has_content_encoding = false;
    this->m_content_encoding = CE_NONE;

    this->m_has_transfer_encoding = false;
    this->m_transfer_encoding = TE_NONE;

    this->m_location  = NULL;
}

HttpHeader::~HttpHeader()
{
    if (this->m_location) {
        free(this->m_location);
    }
}

bool HttpHeader::parse(char *header)
{
    int hlen = 0;
    char *p1,*p2, *p3, *pk;
    char kbuf[60];
    char vbuf[5120];// cookie!!!
    int llen = 0;

    p1 = strstr(header, "\r\n\r\n");
    if (p1 == NULL) {
        return false;
    }
    this->m_header = header;
    this->m_end = p1 + 4;
    hlen = this->m_end - this->m_header;

    qLogx()<<"Full header:"<<(void*)m_header<<(void*)m_end<<hlen;

    p2 = m_header;
    // first status line of header
    if (strncmp(p2, "HTTP/", 5) == 0) {
        p3 = strchr(p2, '\n');
        if (p3 == NULL) {
            return false;
        }

        m_http_version = *(p2+7) == '1' ? HV_1_1 : HV_1_0;
        ////
        memcpy(kbuf, p2 + 9, 3);
        kbuf[3] = 0;

        m_http_code = atoi(kbuf);

        qLogx()<<m_http_version<<kbuf<<m_http_code;

        p2 = ++p3;
    } else {
        return false;
    }

    qLogx()<<""<<p2[0];

    while (p2 < p1) {
        p3 = strchr(p2, '\n');
        if (p3 != NULL) {
            pk = strchr(p2, ':');
            if (pk != NULL) {
                // key =
                llen = p3 - p2; // 行长度
                if (llen > 15 && strncmp(p2, "Content-Length:", 15) == 0) {
                    this->m_has_content_length = true;
                    memcpy(kbuf, pk+2, (p3-1) - (pk+2));
                    kbuf[(p3-1) - (pk+2)] = 0;
                    this->m_content_length = atoi(kbuf);
                    qLogx()<<"Content lenght:"<<this->m_has_content_length<<this->m_content_length<<kbuf;
                } else if (llen > 17 && strncmp(p2, "Content-Encoding:", 17) == 0) {
                    this->m_has_content_encoding = true;
                    this->m_content_encoding = CE_GZIP;
                    qLogx()<<"Content encoding:"<<this->m_has_content_encoding<<this->m_content_encoding;
                } else if (llen > 18 && strncmp(p2, "Transfer-Encoding:", 18) == 0) {
                    this->m_has_transfer_encoding = true;
                    this->m_transfer_encoding = TE_CHUNKED;
                    qLogx()<<"Transfer encoding:"<<this->m_has_transfer_encoding<<this->m_transfer_encoding;
                } else if (llen > 9 && strncmp(p2, "Location:", 9) == 0) {
                    memcpy(vbuf, pk+2, (p3-1) - (pk+2));
                    vbuf[(p3-1) - (pk+2)] = 0;
                    this->m_location = strdup(vbuf);
                    qLogx()<<"30X Location->:"<<vbuf;
                }
            }

            //////
            p2 = ++p3;
        }
    }

    return true;
}

int HttpHeader::headerLength()
{

}

unsigned int HttpHeader::contentLength()
{
    return this->m_content_length;
}

const char *HttpHeader::contentType()
{

}

bool HttpHeader::hasContentLength()
{
    return this->m_has_content_length;
}

bool HttpHeader::hasContentType()
{

}

bool HttpHeader::hasContentEncoding()
{
    return this->m_has_content_encoding;
}

int HttpHeader::contentEncoding()
{
    return this->m_content_encoding;
}

bool HttpHeader::hasTransferEncoding()
{
    return this->m_has_transfer_encoding;
}

int HttpHeader::transferEncoding()
{
    return this->m_transfer_encoding;
}
