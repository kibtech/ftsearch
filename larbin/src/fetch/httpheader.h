#ifndef _HTTP_HEADER_H_
#define _HTTP_HEADER_H_

class HttpHeader
{
public:
    HttpHeader();
    virtual ~HttpHeader();

    bool parse(char *header);

    int httpCode() {
        return this->m_http_code;
    }

    int headerLength();
    unsigned int contentLength();
    const char *contentType();
    bool hasContentLength();
    bool hasContentType();

    enum HttpVersion {
        HV_1_0 = 0 , HV_1_1
    };

    enum ContentEncoding {
        CE_NONE = 0, CE_GZIP, CE_DEFLATE
    };
    // chunked, compress, deflate, gzip, identity
    enum TransferEncoding {
        TE_NONE = 0, TE_CHUNKED, TE_COMPRESS, TE_DEFLATE, TE_GZIP, TE_IDENTITY
    };

    bool hasContentEncoding();
    int contentEncoding();
    bool hasTransferEncoding();
    int transferEncoding();

    char *location() {
        return this->m_location;
    }

public:

protected:
    char *m_header;
    char *m_end; // header end
    int m_http_version;
    int m_http_code;
    bool m_has_content_length;
    int m_content_length;
    bool m_has_content_encoding;
    int m_content_encoding;
    bool m_has_transfer_encoding;
    int m_transfer_encoding;
    char *m_location;
};

#endif
