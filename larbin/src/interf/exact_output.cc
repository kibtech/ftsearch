// Larbin
// Sebastien Ailleret
// 07-12-01 -> 07-12-01

#include <iostream>

#include <string.h>
#include <unistd.h>

#include "options.h"

#include "types.h"
#include "global.h"
#include "fetch/file.h"
#include "utils/text.h"
#include "utils/debug.h"
#include "interf/output.h"

#include "extor/stdlog.h"
#include "extor/extractor.h"
#include "extor/extractorfactory.h"
#include "extor/mysqlstorage.h"
#include "extor/base62.h"

Extractor *extor = NULL;
MysqlStorage *storage = NULL;

/** A page has been loaded successfully
 * @param page the page that has been fetched
 */
void loaded ( html *page )
{
    // Here should be the code for managing everything
    // page->getHeaders() gives a char* containing the http headers
    // page->getPage() gives a char* containing the page itself
    // those char* are statically allocated, so you should copy
    // them if you want to keep them
    // in order to accept \000 in the page, you can use page->getLength()
#ifdef BIGSTATS
    cout << "fetched : ";
    page->getUrl()->print();
    // cout << page->getHeaders() << "\n" << page->getPage() << "\n";
#endif // BIGSTATS
    // printf ( "hahaha: %s, len: %d\n", page->getUrl()->getUrl(), page->getLength());//, page->getPage());
    qLogx()<<"\n\n\nBefore extracting, "<< page->getUrl()->getUrl()<<page->getLength()<<strlen(page->getPage());

    bool bret;
    std::string rid, title, ctime, category, author, content, view_count, comment_count;


    Extractor *ext = ExtractorFactory::create(page->getUrl()->getUrl());
    if (ext == NULL) {
        return;
    }

    bret = ext->parser_page(page->getUrl()->getUrl(), page->getPage());
    if (!bret) {
        qLogx()<<bret;
    }

    rid = ext->get_rid();
    title = ext->get_title();
    content = ext->get_content();
    author = ext->get_author();
    ctime = ext->get_ctime();
    category = ext->get_category();
    view_count = ext->get_view_count();
    comment_count = ext->get_comment_count();

    qLogx()<<"rid:"<<rid;
    qLogx()<<"title:"<<title;
    qLogx()<<"author:"<<author;
    qLogx()<<"category:"<<category;
    qLogx()<<"ctime:"<<ctime;
    qLogx()<<"content len:"<<content.length()<<content.substr(0, 300);
    qLogx()<<"view_count:"<<view_count;
    qLogx()<<"comment_count:"<<comment_count;

    delete ext; ext = NULL;

    if (title.empty() || content.empty()) {
        qLogx()<<"An invalid page, can not got expected content.";
    } else {
        char cmbuf[20] = {0};
        StorageItem *item = new StorageItem();
        strncpy(item->url, page->getUrl()->getUrlWithoutDefaultPort(), sizeof(item->url)-1);
        strncpy(item->rid, rid.c_str(), sizeof(item->rid)-1);
        strncpy(item->title, title.c_str(), sizeof(item->title)-1);
        strncpy(item->author, author.c_str(), sizeof(item->author)-1);
        strncpy(item->body, content.c_str(), sizeof(item->body)-1);
        strncpy(item->category, category.c_str(), sizeof(item->category)-1);
        snprintf(cmbuf, sizeof(cmbuf)-1, "%d", ::time_from_string(ctime));
        strncpy(item->ctime, cmbuf, sizeof(item->ctime)-1);
        strncpy(item->view_count, view_count.c_str(), sizeof(item->view_count)-1);
        strncpy(item->comment_count, comment_count.c_str(), sizeof(item->comment_count)-1);

        //
//        item->url = std::string(page->getUrl()->getUrlWithoutDefaultPort());
//        item->rid = rid;
//        item->title = title;
//        item->author = author;
//        item->body = content;
//        item->category = category;
//        snprintf(cmbuf, sizeof(cmbuf)-1, "%d", ::time_from_string(ctime));
//        item->ctime = std::string(cmbuf); // ctime;
//        item->view_count = view_count;
//        item->comment_count = comment_count;

        bret = storage->addItem(item);
        //    delete item;
    }

    // add url to storage
    {
        UrlItem *item = new UrlItem();
        strncpy(item->url, page->getUrl()->getUrlWithoutDefaultPort(), sizeof(item->url)-1);

        char *usrc = item->url;
        char *usdest = (char*)calloc(1, strlen(usrc)*2+1);
        usdest[0] = '\0';
        strbase62_encode(usrc, usdest, strlen(usrc)*2+1);
        qLogx()<<"short url:" << usdest << ",full url:"<<usrc;

        bret = storage->addItem(item);
        // delete item;

    }

    //// get lang
    // <meta charset="utf-8">
//    char csbuf[16] = {0};
//    char *src = page->getPage();
//    char *cs_pos = NULL;
//    char *p1, *p2;
//    cs_pos = strcasestr(src, "charset=");
//    if (cs_pos) {
//        p1 = cs_pos + strlen("charset=");
//        p2 = strpbrk(p1, " \">/");
//        if (p1 == NULL || p2 == NULL) {
//            printf("can not find page encodeing charset\n");
//        } else {
//            strncpy(csbuf, p1, p2-p1);
//            printf("page encoding charset: '%s'\n", csbuf);
//        }
//    } else {
//        printf("should not be arrived here.\n");
//    }
}

/** The fetch failed
 * @param u the URL of the doc
 * @param reason reason of the fail
 */
void failure ( url *u, FetchError reason )
{
    // Here should be the code for managing everything
#ifdef BIGSTATS
    cout << "fetched failed (" << ( int ) reason << ") : ";
    u->print();
#endif // BIGSTATS
}

/** initialisation function
 */
void initUserOutput ()
{
    qLogx()<<"module inited.";

    storage = new MysqlStorage();
    storage->init();
}

// TODO 应该有个deinitUserOutput, 与initUserOutput对应

/** stats, called in particular by the webserver
 * the webserver is in another thread, so be careful
 * However, if it only reads things, it is probably not useful
 * to use mutex, because incoherence in the webserver is not as critical
 * as efficiency
 */
void outputStats ( int fds )
{
    char tbuf[100] = {0};

    strcpy(tbuf, "Nothing to declare");
    ecrire ( fds,  tbuf);
}







