#!/bin/sh

set -x

cvs -d:pserver:anonymous@tidy.cvs.sourceforge.net:/cvsroot/tidy login
cvs -z3 -d:pserver:anonymous@tidy.cvs.sourceforge.net:/cvsroot/tidy co -P tidy
