
include_directories(./include)

add_library(tidy
src/access.c   src/attrdict.c  src/buffio.c    src/config.c    src/iconvtc.c  src/localize.c  src/pprint.c    src/tags.c     src/utf8.c
src/alloc.c    src/attrget.c   src/charsets.c  src/entities.c  src/istack.c   src/mappedio.c  src/streamio.c  src/tidylib.c  src/win32tc.c
src/attrask.c  src/attrs.c     src/clean.c     src/fileio.c    src/lexer.c    src/parser.c    src/tagask.c    src/tmbstr.c

)

add_executable(tidyc
console/tidy.c
)
target_link_libraries(tidyc tidy)

add_executable(tab2space
console/tab2space.c
)
target_link_libraries(tab2space tidy)
