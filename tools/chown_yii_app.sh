#!/bin/sh

set -x

CPWD=`pwd`

if [ x"$1" == x"" ] ; then
    true
else
    cd $1 || (echo "cd non dir, exit"; exit)
fi

mkdir -pv assets
chmod 777 assets

mkdir -pv protected/runtime
chmod 777 protected/runtime

cd $CPWD
