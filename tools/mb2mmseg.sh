#!/bin/sh
set -x

# php ./sg2mmseg.php

####不需要了，已经在前面处理了
#### cat ../public/wa1/protected/controllers/etc/unigram.txt ./sgunigram.txt > all_unigram.txt

# thesaurus.txt all_thesaurus.txt

###### 生成这个同义词源文件，需要大概1G内存。
# python ../sources/coreseek-4.1-beta/mmseg-3.2.14/script/build_thesaurus.py all_unigram.txt > all_thesaurus.txt


##### 生成同义词二进制格式文件需要1.3G内存。
# 本机
# ../public/wa1/protected/controllers/mmseg -t all_thesaurus.txt 
 
# 服务器
# ../sources/coreseek-4.1-beta/mmseg-3.2.14/src/mmseg -t all_thesaurus.txt
                                                                        

#### 生成词典二进制格式文件
../sources/coreseek-4.1-beta/mmseg-3.2.14/src/mmseg -u all_unigram.txt

