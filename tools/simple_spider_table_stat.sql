use drupal;

select '';
select '-- duplicate items...';
select distinct(rid),count(nid) as cnid,nid from spider_items group by rid having cnid > 1;

select '';
select '-- duplicate items count...';
select count(*) from (select distinct(rid),count(nid) as cnid,nid from spider_items group by rid having cnid > 1) as dt1;

select '';
select '-- invalid items count...';
select count(*) from spider_items where title = '' or title = null or body = '' or body = null;

select '';
select '-- latest fetched items...';
select nid, rid, ctime, view_count,comment_count, author, title, referer from spider_items order by nid desc limit 10;

select '';
select '-- total items count...';
SELECT *, (total_count - dup_count) FROM (
       SELECT (select count(*) from spider_items) AS total_count,
               (select count(*) from (select distinct(rid),count(nid) as cnid,nid from spider_items group by rid having cnid > 1) as dt1) AS dup_count,
               (select count(*) from spider_items where title = '' or title = null or body = '' or body = null) AS invlid_count
       ) AS st1
        ;

-- drop duplicate record
-- DELETE FROM spider_items WHERE nid IN (
--        SELECT nid FROM (
--              SELECT DISTINCT(rid), COUNT(*) AS dc, nid FROM spider_items GROUP BY rid HAVING dc > 1
--              ) AS dt1
--        ); 