#ifndef SPIDERCONST_H
#define SPIDERCONST_H

#include <QObject>

class SpiderConst : public QObject
{
    Q_OBJECT
public:
    explicit SpiderConst(QObject *parent = 0);
    
signals:
    
public slots:

public:
    static const int min_fetch_inteval = 1000; // in msec
    static const int max_fetch_inteval = 5000;
    static const int max_parral_fetch_thread = 3;
};

#endif // SPIDERCONST_H
