
#include "simplelog.h"

#include "extractor.h"
#include "cdextractor.h"
#include "elextractor.h"
#include "huanqiuextractor.h"
#include "tianyabookextractor.h"

#include "extractorfactory.h"

Extractor *ExtractorFactory::create(QWebFrame *wf, const QString &url)
{
    Extractor *extor = NULL;
    QString host;

    host = QUrl(url).host();

    if (host.endsWith("csdn.net", Qt::CaseInsensitive)) {
        extor = new CdExtractor(wf);
    } else if (host.endsWith("chinaunix.net", Qt::CaseInsensitive)) {
        extor = new CUExtractor(wf);
    } else if (host.endsWith("elong.com", Qt::CaseInsensitive)) {
        extor = new ElExtractor(wf);
    } else if (host.endsWith("huanqiu.com", Qt::CaseInsensitive)) {
        extor = new HuanqiuExtractor(wf);
    } else if (host.endsWith("tianyabook.com", Qt::CaseInsensitive)) {
        extor = new TianyabookExtractor(wf);
    } else {
        qLogx()<<"Not impled extractor for: "<<url;
    }

    return extor;
}
