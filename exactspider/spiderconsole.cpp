
#include <QtCore>
#include <QtGui>

#include "simplelog.h"

#include "mainwindow.h"
#include "taskmanager.h"

#include "spiderconsole.h"


int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    MainWindow *w;
    TaskManager *m;

    if (argc == 2) {
        // TaskManager m;
        QString url = QString(argv[1]);
        m = new TaskManager;
        m->start(url);
        // m->start("http://news.chinaunix.net/sci/350.shtml");
    } else {
        // MainWindow w;
        w = new MainWindow;
        w->show();
    }

    qLogx()<<"enter event loop.";
    return a.exec();
    return 0;
}
