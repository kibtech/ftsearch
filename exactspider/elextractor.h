#ifndef _ELEXTRACTOR_H_
#define _ELEXTRACTOR_H_


#include "extractor.h"

class ElExtractor : public Extractor
{
    Q_OBJECT;
public:
    ElExtractor(QWebFrame *wf) : Extractor(wf) {
        this->init();
    }

    virtual ~ElExtractor() {

    }

    virtual bool init() {
        this->m_body_url_selector = "a";
        /*
    QString slt_title = "td[class=Ftext1]";
    QString slt_content = "div#artibody";
    QString slt_view_count = "span#hits";
    QString slt_ctime = "td[align=center]";
    QString slt_comment_count = "span#comments_top";
         */
        this->m_title_selector = "h4";
        this->m_content_select = "div[class=jj-sum]|div[class='jj-intro bd-1']|div[class=article-content]";
        this->m_view_count_selector = "li[class=_went] > b";
        this->m_comment_count_selector = "li[class='_want want'] > b";
        this->m_ctime_selector = "div[class=notexistshahaha]";
        this->m_author_selector = "div#menu-current";

        return true;
    }

    virtual QStringList getCareBodyPages();
    virtual QStringList filterredLinks(QStringList &links);

//    virtual QString getTitle();
//    virtual QString getCTime();
    virtual QString getAuthor();
    virtual QString getRid();
    virtual QString getContent();
    // virtual QString getViewCount();
    // virtual QString getCommentCount();

};

#endif /* _ELEXTRACTOR_H_ */
