#ifndef HUANQIUEXTRACTOR_H
#define HUANQIUEXTRACTOR_H

#include <QtCore>
#include <QtWebKit>

#include "extractor.h"

class HuanqiuExtractor : public Extractor
{
public:
    HuanqiuExtractor(QWebFrame *wf) : Extractor(wf) {
        this->init();
    }

    virtual ~HuanqiuExtractor() {

    }

    virtual bool init() {
        this->m_body_url_selector = "a";
        /*
    QString slt_title = "td[class=Ftext1]";
    QString slt_content = "div#artibody";
    QString slt_view_count = "span#hits";
    QString slt_ctime = "td[align=center]";
    QString slt_comment_count = "span#comments_top";
         */
        this->m_title_selector = "div[class=main]>h1";// "div[class=blkleft]>h1:first-child";
        this->m_content_select = "div#text";
        this->m_view_count_selector = "span#linkCnt";
        this->m_comment_count_selector = "iframe>html>body>p>a";
        this->m_ctime_selector = "li#get_date";
        this->m_author_selector = "li#fromsite";

        return true;
    }

    virtual QStringList filterredLinks(QStringList &links);

    virtual QString getRid();
    virtual QString getCTime();
//    virtual QString getAuthor();
//    virtual QString getViewCount();
};

#endif // HUANQIUEXTRACTOR_H
