
#include "simplelog.h"

#include "elextractor.h"


QStringList ElExtractor::getCareBodyPages()
{
    QStringList urls;
    QString url;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_body_url_selector);
    QUrl burl = this->mwf->baseUrl();
    QUrl curl, aurl;

    for (int i = 0; i < wes.count(); ++i) {
        we = wes.at(i);
        url = we.attribute("href");
        aurl = curl = QUrl(url);
        if (curl.isRelative()) {
            aurl = burl.resolved(curl);
            url = aurl.toString();
        }

        if (aurl.host() == "trip.elong.com") {
            urls.append(url);
        } else {
            qLogx()<<"Omited outer link:"<<url;
        }
    }

    return urls;
}

QStringList ElExtractor::filterredLinks(QStringList &links)
{
    QStringList tlinks;
    QStringList filters, havent_filters;
    QString alink, afilter;
    int filterred_count = 0;

    filters << ".com/u/" << ".com/so/" << ".com/ask/" << ".com/bbs/";

    havent_filters << "trip.elong.com/";

    for (int i = links.count()-1; i >=0 ; --i) {
        alink = links.at(i);
        //        qLogx()<<"begin filter:"<<alink;

        for (int j = 0; j < filters.count(); ++j) {
            afilter = filters.at(j);

            if (alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

        for (int j = 0; j < havent_filters.count(); ++j) {
            afilter = havent_filters.at(j);

            if (!alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

refilter_next:
        continue;
    }


    qLogx()<<"Filterred links:"<<filterred_count;

    tlinks = links;
    return tlinks;
}

//QString ElExtractor::getTitle()
//{
//    QString rstr;

//    QString str, str2;
//    QStringList strs;

//    str2 = u8c->toUnicode(QByteArray("："));
//    str = Extractor::getTitle();

//    strs = str.split(str2);

//    if (strs.count() > 1) {
//        rstr = strs.at(1);
//    } else {
//        rstr = str;
//    }

//    return rstr;
//}

//QString ElExtractor::getCTime()
//{
//    QString rstr;

//    rstr = "2011-11-22 01:02:03";

//    return rstr;
//}

QString ElExtractor::getAuthor()
{
    QString rstr;

    QStringList strs;
    QString str = u8c->toUnicode(QByteArray("："));
    QString str2 = Extractor::getAuthor();

    strs = str2.split(str);

    if (strs.count() > 1) {
        rstr = strs.at(1);
    } else {
        rstr = str2;
    }

    return rstr;
}

QString ElExtractor::getRid()
{
    QString rstr;

    rstr = "0";

    return rstr;

}

QString ElExtractor::getContent()
{
    QString rstr;

    QStringList selectors;
    QWebElement we;
    QWebElementCollection wes; // = this->mwf->findAllElements(this->m_content_select);

    selectors = this->m_content_select.split(this->selector_sep);

    for (int i = 0; i < selectors.count(); ++i) {
        wes = this->mwf->findAllElements(selectors.at(i));
        if (wes.count() >= 1) {
            for (int j = 0; j < wes.count(); ++j) {
                we = wes.at(j);
                rstr += we.toPlainText().trimmed();
            }
            if (wes.count() > 1) {
                qLogx()<<"Warning: more than one content???";
            }
            break;
        } else {

        }
    }
    return rstr;

}

//QString ElExtractor::getViewCount()
//{
//    QString rstr;

//    QWebElement we;
//    QWebElementCollection wes = this->mwf->findAllElements(this->m_view_count_selector);

//    if (wes.count() >= 1) {
//        we = wes.at(0);
//        rstr = we.toPlainText().trimmed();
//        if (wes.count() > 1) {
//            qLogx()<<"Warning: more than one title???";
//        }
//    } else {

//    }
//    return rstr;

//}

//QString ElExtractor::getCommentCount()
//{
//    QString rstr;

//    QWebElement we;
//    QWebElementCollection wes = this->mwf->findAllElements(this->m_comment_count_selector);

//    if (wes.count() >= 1) {
//        we = wes.at(0);
//        rstr = we.toPlainText().trimmed();
//        if (wes.count() > 1) {
//            qLogx()<<"Warning: more than one title???";
//        }
//    } else {

//    }
//    return rstr;

//}

