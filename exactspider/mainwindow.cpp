
#include <QtWebKit>

#include "simplelog.h"

#include "filternetworkaccessmanager.h"
#include "mysqlstorage.h"
#include "autonext.h"
#include "extractor.h"
#include "cdextractor.h"
#include "extractorfactory.h"
#include "loadstate.h"

#include "taskmanager.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#define BLANK_URL "about:blank"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    this->ws = QWebSettings::globalSettings();
//    this->ws->setAttribute(QWebSettings::AutoLoadImages, false);
//    this->ws->setAttribute(QWebSettings::PluginsEnabled, false);
//    this->ws->setAttribute(QWebSettings::JavascriptEnabled, false);
//    this->ws->setAttribute(QWebSettings::DnsPrefetchEnabled, true);

    this->tm = new TaskManager;

//    QNetworkAccessManager *manager = this->ui->qwebview->page()->networkAccessManager();
//    this->m_index_fnac = new FilterNetworkAccessManager(manager);
//    this->ui->qwebview->page()->setNetworkAccessManager(this->m_index_fnac);
//    manager = this->ui->qwebview_2->page()->networkAccessManager();
//    this->m_body_fnac = new FilterNetworkAccessManager(manager);
//    this->ui->qwebview_2->page()->setNetworkAccessManager(this->m_body_fnac);

    QObject::connect(this->ui->pushButton, SIGNAL(clicked()), this, SLOT(start()));
    QObject::connect(this->ui->pushButton_2, SIGNAL(clicked()), this, SLOT(stop()));

//    QObject::connect(this->ui->qwebview, SIGNAL(loadFinished(bool)), this, SLOT(indexPageLoadFinished(bool)));
//    QObject::connect(this->ui->qwebview_2, SIGNAL(loadFinished(bool)), this, SLOT(bodyPageLoadFinished(bool)));
//    // QObject::connect(this->ui->qwebview_2, SIGNAL(loadProgress(int)), this, SLOT(bodyPageLoadProgress(int)));
//    QObject::connect(this->ui->qwebview_2->page(), SIGNAL(statusBarMessage(QString)),
//                     this, SLOT(onStatusBarMessage(QString)));

//    QObject::connect(this->ui->qwebview, SIGNAL(urlChanged(QUrl)), this, SLOT(onIndexPageUrlChanged(QUrl)));
    QObject::connect(this->ui->qwebview_2, SIGNAL(urlChanged(QUrl)), this, SLOT(onBodyPageUrlChanged(QUrl)));

//    QObject::connect(this->ui->qwebview, SIGNAL(statusBarMessage(QString)),
//                     this, SLOT(onIndexStatusBarMessage(QString)));
    QObject::connect(this->ui->qwebview_2, SIGNAL(statusBarMessage(QString)),
                     this, SLOT(onBodyStatusBarMessage(QString)));

    // QObject::connect(this, SIGNAL(loadNextIndex(QString)), this, SLOT(onLoadNextIndex(QString)));
    // QObject::connect(this, SIGNAL(loadNextBody()), this, SLOT(onLoadNextBody()));

//    body_view_free = true;
//    mystore = new MysqlStorage();
//    mystore->start();

    // this->ui->qwebview_2->setUrl(QUrl("http://news.chinaunix.net/industry/2011/1119/1796927.shtml"));
    // this->ui->qwebview_2->setUrl(QUrl("http://news.csdn.net/a/20090330/210006.html"));

//    index_page_count = 0;
//    body_page_finish_count = 0;
//    total_body_len = 0;
//    extract_item_len = 0;
//    fetch_speed = 0; // Bytes/sec
//    stat_timer = new QTimer();
//    stat_timer->setInterval(1000);
//    QObject::connect(stat_timer, SIGNAL(timeout()), this, SLOT(onUpdateStat()));

//    m_nextor = new AutoNext();
}

MainWindow::~MainWindow()
{
//    stat_timer->stop();
//    delete stat_timer;
    delete ui;

//    this->mystore->setStop();
//    this->mystore->wait(200);

//    delete this->mystore;
}

void MainWindow::start()
{
    QString iu = this->ui->lineEdit->text();
    QString ureg = this->ui->lineEdit_2->text();

//    this->ui->qwebview->setZoomFactor(0.7);
//    this->ui->qwebview_2->setZoomFactor(0.7);
//    this->ui->qwebview->setPage(tm->m_index_page);
    this->ui->qwebview_2->setPage(tm->m_body_page);
    this->ui->qwebview_2->setZoomFactor(0.7); // 这个设置要放在setPage之后才能生效。

    this->tm->start(iu);

//    this->index_url_queue.enqueue(iu);
//    this->index_page_count ++;
//    start_time = QDateTime::currentDateTime();

//    QTimer::singleShot(1, this, SLOT(onLoadNextIndex()));

//    stat_timer->start();
}

void MainWindow::stop()
{
    // stat_timer->stop();

//    this->ui->qwebview->setPage(NULL);
    this->ui->qwebview_2->setPage(NULL);

    this->tm->stop();
}

//void MainWindow::onIndexPageUrlChanged(const QUrl &url)
//{
//    QString str;

//    str = url.toString();

//    this->ui->comboBox->setEditText(str);
//}

void MainWindow::onBodyPageUrlChanged(const QUrl &url)
{
    QString str;

    str = url.toString();

    this->ui->comboBox_3->setEditText(str);
}

//void MainWindow::onIndexStatusBarMessage(const QString &text)
//{
//    this->ui->comboBox_2->setItemText(0, text);
//}

void MainWindow::onBodyStatusBarMessage(const QString &text)
{
    this->ui->comboBox_4->setItemText(0, text);
}

//void MainWindow::indexPageLoadFinished(bool finished)
//{
//    QWebPage *wp = this->ui->qwebview->page();
//    QWebFrame *mwf = wp->mainFrame();// wp->currentFrame();
//    QString url = this->ui->qwebview->url().toString();
//    int child_frame_count = mwf->childFrames().count();

//    if (!finished) {
//        qLogx()<<"Not really finished index page: "<<url<<child_frame_count;
//    }

//    this->total_body_len += wp->totalBytes();

//    if (this->index_finish_list.contains(url)) {
//        qLogx()<<"duplicated index url:"<<url;
//    } else {
//        this->index_finish_list[url] = 1;
//    }

//    QStringList urls;
//    Extractor *extor = ExtractorFactory::create(mwf, url);
//    Q_ASSERT(extor != NULL);
//    QString newu;

//    urls = extor->getCareBodyPages();
//    qLogx()<<"Got "<<urls.count()<<" body urls";
//    delete extor; extor = NULL;
//    for (int i = 0; i < urls.count(); ++i) {
//        newu = urls.at(i);
//        // qLogx()<<i<<newu;
//        if (newu.split("/").count() < 6) { // TODO, why this.
//            qLogx()<<"Maybe invalid url: omit,"<<newu<<", refrom:"<<url;
//            continue;
//        }

//        this->body_url_queue.enqueue(newu);
//    }


//    // qLogx()<<this->ui->qwebview_2->url().toString();
//    if (urls.count() > 0
//            && (this->ui->qwebview_2->url().toString().isEmpty()
//                || body_view_free == true)) {
//                // || this->ui->qwebview_2->url().toString() == BLANK_URL)) {
//        //        emit this->loadNextBody();
//        int next_delay = qrand() % 6000 + 1000;
//        QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
//    }

//    // parse next index page url
//    //    QString up, uf, ul;
//    //    up = this->m_nextor->getPrevPage(wf);
//    //    uf = this->m_nextor->getFirstPage(wf);
//    //    ul = this->m_nextor->getLastPage(wf);
//    //    qLogx()<<up<<uf<<ul;
//    QString first_page_url = this->m_nextor->getFirstPage(mwf);
//    QString prev_page_url = this->m_nextor->getPrevPage(mwf);
//    QString next_page_url = this->m_nextor->getNextPage(mwf);

//    newu = prev_page_url;
//    if (newu.isEmpty() || newu == first_page_url) {
//        qLogx()<<"No more index page left.";
//    } else {
//        qLogx()<<newu;

//        this->index_url_queue.enqueue(newu);

//        int next_delay = qrand() % 12000 + 1000;
//        QTimer::singleShot(next_delay, this, SLOT(onLoadNextIndex()));
//    }

//}

//void MainWindow::onLoadNextIndex()
//{
//    QString url;

//    if (!this->index_url_queue.isEmpty()) {
//        url = this->index_url_queue.dequeue();
//    }

//    if (!url.isEmpty()) {
//        this->ui->comboBox->setEditText(url);
//        this->ui->qwebview->setUrl(QUrl(url));
//    }
//}

//void MainWindow::onLoadNextBody()
//{
//    QString url;

////    if (this->faild_list.count() > 0 && qrand() % 3 == 0) {
////        url = this->faild_list.begin().key();
////        body_view_free = false;
////        qLogx()<<"Retry loading faild url:"<<url;
////        this->ui->comboBox_2->setEditText(url);
////        this->ui->qwebview_2->setUrl(url);
////        return;
////    }

//    while (!this->body_url_queue.isEmpty()) {
//        url = this->body_url_queue.dequeue();

//        if (!url.isEmpty()) {
//            if (this->body_finish_list.contains(url)) {
//                qLogx()<<"already fetched url, omited:"<<url;
//                continue;
//            }
//            body_view_free = false;
//            this->ui->comboBox_2->setEditText(url);
//            this->ui->qwebview_2->setUrl(url);
//            return;
//        } else {
//            // body_view_free = true;
//            // continue;
//        }
//    }

//    if (this->body_url_queue.isEmpty()) {
//        body_view_free = true;
//        return;
//    }
//}

//void MainWindow::bodyPageLoadFinished(bool finished)
//{
//    QWebPage *wp = this->ui->qwebview_2->page();
//    QWebFrame *mwf = wp->mainFrame();
//    QWebFrame *cwf = wp->currentFrame();
//    QString murl = mwf->url().toString();
//    QString curl = cwf->url().toString();
//    int child_frame_cound = mwf->childFrames().count();
//    // LoadState *ols = NULL;

//    if (murl == BLANK_URL) {
//        return;
//    }

//    if (!finished) {
////        if (this->faild_list.contains(murl)) {
////            ols = this->faild_list.value(murl);
////        } else {
////            ols = new LoadState();
////            this->faild_list[murl] = ols;
////        }
////        ols->url = murl;
////        ols->retry_times += 1;

//        qLogx()<<"Not really finished, retry later."<<finished<<this->m_status_text
//              <<wp->totalBytes()<<wp->bytesReceived()
//             <<murl<<curl; // <<ols->retry_times;
//            // <<this->faild_list.count();

////        {
////            wp->settings()->clearMemoryCaches();
////            int next_delay = qrand() % 3000 + 1000;
////            QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
////        }
////        return;
//    }

//    if (cwf != mwf) {
//        qLogx()<<"Multi frame page found."<<murl<<curl;
//    }

//    this->total_body_len += wp->bytesReceived(); // wp->totalBytes();

//    if (this->body_finish_list.contains(murl)) {
//        qLogx()<<"duplicated body url:"<<murl;
//    } else {
//        this->body_finish_list[murl] = 1;
//        //////
//        qLogx()<<"body load finished:"<<murl<<child_frame_cound;

//        bool bret = this->parseBody(mwf);
//        if (!bret) {
//            // this->ui->qwebview_2->reload();
//            // qLogx()<<"Reload url:"<<url;
//            // QTimer::singleShot(50, this->ui->qwebview_2, SLOT(reload()));
//        }
////        if (this->faild_list.contains(murl)) {
////            ols = this->faild_list.value(murl);
////            this->faild_list.remove(murl);
////            delete ols;
////        }
//    }

//    // this->ui->qwebview_2->setUrl(QUrl());

//    {
//        //        emit this->loadNextBody();
//        int next_delay = qrand() % 3000 + 1000;
//        QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
//    }
//}

//void MainWindow::bodyPageLoadProgress(int progress)
//{
//    QWebPage *wp = this->ui->qwebview_2->page();
//    QWebFrame *wf = wp->mainFrame();

//    QString url = this->ui->qwebview_2->url().toString();

//    // if (qrand() % 3 == 1 || progress == 100 || progress == 0)
//    //    if (progress % 10 == 0)
//        qLogx()<<progress<<url;

//    if (progress >= 100) {
//        this->bodyPageLoadFinished(true);
//    }
//}

//bool MainWindow::parseBody(QWebFrame * frame)
//{
//    Q_ASSERT(frame != NULL);

//    int child_frame_count = frame->childFrames().count();
//    QString url = frame->url().toString();
//    QString rid;
//    QString title;
//    QString content;
//    QString author;
//    QString ctime;
//    QString view_count;
//    QString comment_count;

//    Extractor * extor = ExtractorFactory::create(frame, url);
//    Q_ASSERT(extor != NULL);

//    rid = extor->getRid();
//    title = extor->getTitle();
//    content = extor->getContent();
//    view_count = extor->getViewCount();
//    author = extor->getAuthor();
//    ctime = extor->getCTime();
//    comment_count = extor->getCommentCount();

//    delete extor; extor = NULL;

//    qLogx()<<rid<<url;
//    qLogx()<<title;
//    // qLogx()<<content;
//    qLogx()<<"body length:"<<content.length()<<" of (Total/Got) "
//          <<frame->page()->totalBytes()<<frame->page()->bytesReceived();
//    qLogx()<<view_count;
//    // qLogx()<<strs;
//    qLogx()<<author;
//    qLogx()<<ctime;
//    qLogx()<<comment_count;

//    if (title.isEmpty() || content.isEmpty()) {
//        // Invalid item?????
//        qLogx()<<"An invalid page, can not got expected content."
//              <<child_frame_count<<frame->toHtml();
//        return false;
//    }

//    StorageItem *item = new StorageItem();
//    item->author = author;
//    item->body = content;
//    item->comment_count = comment_count;
//    item->ctime = QString("%1").arg(QDateTime::fromString(ctime, "yyyy-MM-dd hh:mm:ss").toTime_t());
//    item->rid = rid;
//    item->title = title;
//    item->url = url;
//    item->view_count = view_count;

//    this->mystore->addItem(item);
//    // delete item;

//    /////
//    this->extract_item_len += content.length();

//    return true;
//}

void MainWindow::onUpdateStat()
{
    QDateTime ntime = QDateTime::currentDateTime();

//    this->ui->lineEdit_3->setText(QString("%1").arg(this->index_page_count));
//    this->ui->lineEdit_7->setText(QString("%1").arg(this->body_url_queue.count()));
//    this->ui->lineEdit_8->setText(QString("%1").arg(this->body_page_finish_count));

//    this->ui->lineEdit_5->setText(QString("%1").arg(this->total_body_len));
//    this->ui->lineEdit_9->setText(QString("%1").arg(this->extract_item_len));

//    int ecs = this->start_time.secsTo(ntime);
//    this->fetch_speed = ecs == 0 ? 1024*1024*1024 : (this->total_body_len/ecs);
//    this->extract_speed = ecs == 0 ? 1024*1024*1024 : (this->extract_item_len/ecs);

//    this->ui->lineEdit_6->setText(QString("%1").arg(this->fetch_speed));
//    this->ui->lineEdit_10->setText(QString("%1").arg(this->extract_speed));
}

//void MainWindow::onStatusBarMessage(const QString &text)
//{
//    this->m_status_text = text;
//}
