#ifndef _CDEXTRACTOR_H_
#define _CDEXTRACTOR_H_

#include <QtCore>
#include <QtWebKit>

#include "extractor.h"

class CdExtractor : public Extractor
{
    Q_OBJECT;
public:
    CdExtractor(QWebFrame *wf) : Extractor(wf) {
        this->init();
    }

    virtual ~CdExtractor() {

    }

    virtual bool init()
    {
        this->m_body_url_selector = "a[class=title]";
        /*
    QString slt_title = "td[class=Ftext1]";
    QString slt_content = "div#artibody";
    QString slt_view_count = "span#hits";
    QString slt_ctime = "td[align=center]";
    QString slt_comment_count = "span#comments_top";
         */
        this->m_title_selector = "div[class=blkleft]>h1";// "div[class=blkleft]>h1:first-child";
        this->m_content_select = "div[class=blkCont]";
        this->m_view_count_selector = "p[class=blueline]>span:first-child + span";
        this->m_comment_count_selector = "font#remark_count1";
        this->m_ctime_selector = "p[class=blueline]>span:first-child";
        this->m_author_selector = "p[class=blueline]>span:first-child + span + span";

        return true;
    }

    virtual QString getRid();
    virtual QString getCTime();
    virtual QString getAuthor();
    virtual QString getViewCount();
};

#endif /* _CDEXTRACTOR_H_ */
