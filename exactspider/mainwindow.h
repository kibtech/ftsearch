#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtWebKit>
#include <QMainWindow>

class FilterNetworkAccessManager;
class MysqlStorage;
class AutoNext;
class Extractor;
class LoadState;
class TaskManager;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
public slots:
    void start();
    void stop();
//    void indexPageLoadFinished(bool finished);
//    void bodyPageLoadFinished(bool finished);
//    void bodyPageLoadProgress(int progress);

//    void onStatusBarMessage(const QString &text);

//    /////
//    void onLoadNextIndex();
//    void onLoadNextBody();

//    bool parseBody(QWebFrame * cwf);

    ////
    void onUpdateStat();

    // void onIndexPageUrlChanged(const QUrl &url);
    void onBodyPageUrlChanged(const QUrl &url);

    // void onIndexStatusBarMessage(const QString &text);
    void onBodyStatusBarMessage(const QString &text);

//signals:
//    void loadNextIndex();
//    void loadNextBody();

private:
    Ui::MainWindow *ui;
    //    QWebSettings *ws;

    TaskManager *tm;

//    QQueue<QString> body_url_queue;
//    QHash<QString, int> body_finish_list;

//    QQueue<QString> index_url_queue;
//    QHash<QString, int> index_finish_list;

//    QHash<QString, LoadState*> loading_list; //
//    // QHash<QString, LoadState*> faild_list;

//    bool body_view_free; // 用来处理内容页面的webview是否空闲

//    //// stats
//    QDateTime start_time;
//    int index_page_count;
//    int body_page_finish_count;
//    qint64 total_body_len;
//    qint64 extract_item_len;
//    int fetch_speed; // Bytes/sec
//    int extract_speed; //
//    QTimer *stat_timer;

//    MysqlStorage *mystore;
//    AutoNext *m_nextor;

//    FilterNetworkAccessManager *m_index_fnac;
//    FilterNetworkAccessManager *m_body_fnac;

//    QString m_status_text;
};

#endif // MAINWINDOW_H
