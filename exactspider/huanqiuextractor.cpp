
#include "simplelog.h"

#include "huanqiuextractor.h"


QStringList HuanqiuExtractor::filterredLinks(QStringList &links)
{
    QStringList tlinks;
    QStringList filters, havent_filters;
    QString alink, afilter;
    int filterred_count = 0;

    //filters << ".com/u/" << ".com/so/" << ".com/ask/";
    filters << "bbs.huanqiu.com" << "blog.huanqiu.com"<< "photo.huanqiu.com";

    //havent_filters << "trip.elong.com/";

    for (int i = links.count()-1; i >=0 ; --i) {
        alink = links.at(i);
        //        qLogx()<<"begin filter:"<<alink;

        for (int j = 0; j < filters.count(); ++j) {
            afilter = filters.at(j);

            if (alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

        for (int j = 0; j < havent_filters.count(); ++j) {
            afilter = havent_filters.at(j);

            if (!alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

refilter_next:
        continue;
    }


    qLogx()<<"Filterred links:"<<filterred_count;

    tlinks = links;
    return tlinks;
}

QString HuanqiuExtractor::getRid()
{
    QString rstr;

    QString rid;
    QStringList strs, strs2;
    QString url = this->mwf->url().toString();

    strs = url.split("/");
    strs2 = strs.at(strs.count()-1).split(".");
    rid = strs2.at(0);

    rstr = rid;
    return rstr;
}

QString HuanqiuExtractor::getCTime()
{
    QString rstr;

    QString tstr = Extractor::getCTime();

    if (!tstr.isEmpty()) {
        rstr = tstr + ":06";
    }

    return rstr;
}

//QString HuanqiuExtractor::getAuthor()
//{
//    QString rstr;

//    QString author;
//    QString tstr, str2;
//    QStringList strs2;

//    tstr = Extractor::getAuthor();

//    // strs = tstr.split(" ");
//    str2 = u8c->toUnicode(QByteArray("："));
//    strs2 = tstr.split(str2);
//    qLogx()<<strs2;

//    if (strs2.count() > 1)
//        author = strs2.at(1).trimmed();
//    else
//        qLogx()<<"Error: fetch author error."<<tstr;

//    rstr = author;
//    return rstr;
//}

//QString HuanqiuExtractor::getViewCount()
//{
//    QString rstr;
//    char tbuf[16] = {0};

//    QString tstr = Extractor::getViewCount();
//    char *ptr = tstr.toAscii().data();
//    char *ss = ptr;
//    char *sd = &tbuf[0];

//    while (*ss >= '0' && *ss <= '9') {
//        *sd++ = *ss++;
//    }
//    *sd = '\0';

//    if (tbuf[0] == '\0') {
//        tbuf[0] = '0';
//    }
//    rstr = QString(tbuf);

//    // qLogx()<<tstr<<rstr;

//    return rstr;
//}
