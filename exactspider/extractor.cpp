
#include "simplelog.h"

#include "extractor.h"

Extractor::Extractor(QWebFrame * wf)
    : QObject(0)
    , mwf(wf)
{
    Q_ASSERT(wf != NULL);
    u8c = QTextCodec::codecForName("UTF-8");
    this->selector_sep = "|";
    this->enable_javascript = true;
}

Extractor::~Extractor()
{

}

QStringList Extractor::getCareBodyPages()
{
    QStringList urls;
    QString url;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_body_url_selector);
    QUrl burl = this->mwf->baseUrl();
    QUrl curl, aurl;

    for (int i = 0; i < wes.count(); ++i) {
        we = wes.at(i);
        url = we.attribute("href");
        curl = QUrl(url);
        if (curl.isRelative()) {
            aurl = burl.resolved(curl);
            url = aurl.toString();
        }

        urls.append(url);
    }

    return urls;
}

QStringList Extractor::filterredLinks(QStringList &links)
{
    return links;
}

QString Extractor::getTitle()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_title_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one title???";
        }
    } else {

    }

    return rstr;
}

QString Extractor::getCTime()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_ctime_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one ctime???";
        }
    } else {

    }

    if (rstr.isEmpty()) {
        rstr = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    }

    return rstr;

}

QString Extractor::getAuthor()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_author_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one author???";
        }
    } else {

    }
    return rstr;

}

QString Extractor::getRid()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_rid_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one rid???";
        }
    } else {

    }
    return rstr;

}

QString Extractor::getContent()
{
    QString rstr;

    QStringList selectors;
    QWebElement we;
    QWebElementCollection wes; // = this->mwf->findAllElements(this->m_content_select);

    selectors = this->m_content_select.split(this->selector_sep);

    for (int i = 0; i < selectors.count(); ++i) {
        wes = this->mwf->findAllElements(selectors.at(i));
        if (wes.count() >= 1) {
            we = wes.at(0);
            rstr = we.toPlainText().trimmed();
            if (wes.count() > 1) {
                qLogx()<<"Warning: more than one content???";
            }
            break;
        } else {

        }
    }
    return rstr;

}

QString Extractor::getViewCount()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_view_count_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one view count???";
        }
    } else {

    }
    return rstr;

}

QString Extractor::getCommentCount()
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(this->m_comment_count_selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one comment???";
        }
    } else {

    }
    return rstr;

}

// notice: only first
QString Extractor::getElementText(const QString &selector)
{
    QString rstr;

    QWebElement we;
    QWebElementCollection wes = this->mwf->findAllElements(selector);

    if (wes.count() >= 1) {
        we = wes.at(0);
        rstr = we.toPlainText().trimmed();
        if (wes.count() > 1) {
            qLogx()<<"Warning: more than one title???";
        }
    } else {

    }
    return rstr;
}
