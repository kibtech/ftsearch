
#include "simplelog.h"

#include "tianyabookextractor.h"


QStringList TianyabookExtractor::filterredLinks(QStringList &links)
{
    QStringList tlinks;
    QStringList filters, havent_filters;
    QString alink, afilter;
    int filterred_count = 0;

    //filters << ".com/u/" << ".com/so/" << ".com/ask/";
    // filters << "bbs.huanqiu.com" << "blog.huanqiu.com"<< "photo.huanqiu.com";

    //havent_filters << "trip.elong.com/";

    for (int i = links.count()-1; i >=0 ; --i) {
        alink = links.at(i);
        //        qLogx()<<"begin filter:"<<alink;

        for (int j = 0; j < filters.count(); ++j) {
            afilter = filters.at(j);

            if (alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

        for (int j = 0; j < havent_filters.count(); ++j) {
            afilter = havent_filters.at(j);

            if (!alink.contains(afilter, Qt::CaseInsensitive)) {
                links.removeAt(i);
                ++filterred_count;
                goto refilter_next;
            }
        }

refilter_next:
        continue;
    }


    qLogx()<<"Filterred links:"<<filterred_count;

    tlinks = links;
    return tlinks;
}

QString TianyabookExtractor::getRid()
{
    QString rstr;

    QString rid;
    QStringList strs, strs2;
    QString url = this->mwf->url().toString();

    strs = url.split("/");
    strs2 = strs.at(strs.count()-1).split(".");
    rid = strs2.at(0);

    rstr = rid;
    return rstr;
}


QString TianyabookExtractor::getTitle()
{
    QString rstr;


    QStringList tps;
    rstr = this->mwf->title().trimmed();
    tps = rstr.split("-", QString::SkipEmptyParts);
    // qLogx()<<tps;
    if (tps.count()>0) {
        rstr = tps.at(0).trimmed();
    }

    return rstr;
}

QString TianyabookExtractor::getContent()
{
    QString rstr;

    rstr = this->mwf->toPlainText().trimmed();

    int head_end_pos = rstr.indexOf("\n");
    int tail_begin_pos = rstr.indexOf("------------------");

    if (tail_begin_pos != -1) {
        rstr = rstr.left(tail_begin_pos).trimmed();
    }

    if (head_end_pos != -1) {
        rstr = rstr.right(rstr.length() - head_end_pos).trimmed();
    }

    return rstr;
}


//QString TianyabookExtractor::getCTime()
//{
//    QString rstr;

//    QString tstr = Extractor::getCTime();

//    if (!tstr.isEmpty()) {
//        rstr = tstr + ":06";
//    }

//    return rstr;
//}

QString TianyabookExtractor::getAuthor()
{
    QString rstr;

    QString author;
    QString tstr, str2;
    QStringList strs2;

    tstr = this->mwf->title();
    strs2 = tstr.split("-", QString::SkipEmptyParts);
    // qLogx()<<strs2;
    if (strs2.count() > 2) {
        author = strs2.at(1).trimmed();
    }

    rstr = author;
    return rstr;
}

//QString HuanqiuExtractor::getViewCount()
//{
//    QString rstr;
//    char tbuf[16] = {0};

//    QString tstr = Extractor::getViewCount();
//    char *ptr = tstr.toAscii().data();
//    char *ss = ptr;
//    char *sd = &tbuf[0];

//    while (*ss >= '0' && *ss <= '9') {
//        *sd++ = *ss++;
//    }
//    *sd = '\0';

//    if (tbuf[0] == '\0') {
//        tbuf[0] = '0';
//    }
//    rstr = QString(tbuf);

//    // qLogx()<<tstr<<rstr;

//    return rstr;
//}
