#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include <QtCore>
#include <QtWebKit>

class FilterNetworkAccessManager;
class MysqlStorage;
class AutoNext;
class Extractor;
class LoadState;


class TaskManager : public QObject
{
    Q_OBJECT
public:
    explicit TaskManager(QObject *parent = 0);
    virtual ~TaskManager();

signals:
//    void loadNextIndex();
    void loadNextBody();

public slots:
    bool start(const QString &start_url);
    bool stop();

//    QWebPage *get_index_page();
    QWebPage *get_body_page();

//    bool go_index_page(const QString &url);
//    bool stop_index_page(const QString &url);
//    bool refresh_index_page(const QString &url);

    bool go_body_page(const QString &url);
    bool stop_body_page(const QString &url);
    bool refresh_body_page(const QString &url);

//    void indexPageLoadFinished(bool finished);

    void bodyPageLoadFinished(bool finished);
    void bodyPageLoadProgress(int progress);
    void bodyPageLoadStarted();

    void onStatusBarMessage(const QString &text);

    /////
//    void onLoadNextIndex();
    void onLoadNextBody();

    bool parseBody(QWebFrame * cwf);

    void on_body_load_timeout();

    void on_page_load_timeout(const QString &url);

public:
    QWebSettings *ws;
//    QWebPage *m_index_page;
    QWebPage *m_body_page;

    QString m_start_url;
    // QQueue<QPair<QString, LoadState*> > m_ready_queue;
    QHash<QString, LoadState*> m_ready_queue;
    QHash<QString, LoadState*> m_body_running_hash;
    QHash<QString, LoadState*> m_body_faild_hash;
    QHash<QString, LoadState*> m_body_finish_hash;

    QTimer m_lazy_finder_timer;

public:

//    QQueue<QString> index_url_queue;
//    QHash<QString, int> index_finish_list;

//    QHash<QString, LoadState*> loading_list; //
    // QHash<QString, LoadState*> faild_list;

//    bool body_view_free; // 用来处理内容页面的webview是否空闲

    //// stats
    QDateTime start_time;
//    int index_page_count;
    int body_page_finish_count;
    qint64 total_body_len;
    qint64 extract_item_len;
    int fetch_speed; // Bytes/sec
    int extract_speed; //
    QTimer *stat_timer;

    MysqlStorage *mystore;
    AutoNext *m_nextor;

//    FilterNetworkAccessManager *m_index_fnac;
    FilterNetworkAccessManager *m_body_fnac;

    QString m_status_text;
};

#endif // TASKMANAGER_H
