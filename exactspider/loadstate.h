#ifndef _LOADSTATE_H_
#define _LOADSTATE_H_

#include <QtCore>

class LoadState : public QObject
{
    Q_OBJECT;
public:
    LoadState();
    virtual ~LoadState();

    QString url;
    QString last_url; // 上一次加载url
    int retry_times;
    bool load_started; //
    bool load_finished;
    QDateTime ctime;  // 实例创建时间
    QDateTime mtime;  // load 开始时间
    int progress;

    QTimer load_timeout_timer; // 加载超时

    const int max_retry_times;

protected slots:
    void on_load_timeout();

signals:
    void load_timeout(const QString &url);
};


#endif /* _LOADSTATE_H_ */
