
#include "loadstate.h"

LoadState::LoadState()
    : QObject(0)
    , max_retry_times(6)
{
    this->retry_times = 0;
    this->load_started = false;
    this->load_finished = false;

    this->ctime = QDateTime::currentDateTime();
    this->progress = 0;

    this->load_timeout_timer.setInterval(1000 * 20);
    this->load_timeout_timer.setSingleShot(true);

    QObject::connect(&this->load_timeout_timer, SIGNAL(timeout()),
                     this, SLOT(on_load_timeout()));
}

LoadState::~LoadState()
{
    if (this->load_timeout_timer.isActive()) {
        this->load_timeout_timer.stop();
    }
}

void LoadState::on_load_timeout()
{
    emit this->load_timeout(this->url);
}
