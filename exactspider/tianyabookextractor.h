#ifndef TIANYABOOKEXTRACTOR_H
#define TIANYABOOKEXTRACTOR_H

#include <QtCore>
#include <QtWebKit>

#include "extractor.h"

class TianyabookExtractor : public Extractor
{
public:
    TianyabookExtractor(QWebFrame *wf) : Extractor(wf) {
        this->init();
    }

    virtual ~TianyabookExtractor() {

    }

    virtual bool init() {
        this->m_body_url_selector = "a";
        /*
    QString slt_title = "td[class=Ftext1]";
    QString slt_content = "div#artibody";
    QString slt_view_count = "span#hits";
    QString slt_ctime = "td[align=center]";
    QString slt_comment_count = "span#comments_top";
         */
        this->m_title_selector = "head>title";// "div[class=blkleft]>h1:first-child";
        this->m_content_select = "body";
        this->m_view_count_selector = "noelem#noelem";
        this->m_comment_count_selector = "noelem#noelem";
        this->m_ctime_selector = "noelem#noelem";
        this->m_author_selector = "noelem#noelem";

        return true;
    }

    virtual QStringList filterredLinks(QStringList &links);

    virtual QString getRid();
//    virtual QString getCTime();
    virtual QString getTitle();
    virtual QString getContent();
    virtual QString getAuthor();
//    virtual QString getViewCount();
};

#endif // TIANYABOOKEXTRACTOR_H
