
#include <mysql/mysql.h>

#include "simplelog.h"

#include "mysqlstorage.h"

MysqlStorage::MysqlStorage(QObject *parent)
    : QThread(parent)
{
    this->u8c = QTextCodec::codecForName("UTF-8");
    this->m_user_stop = false;
}

MysqlStorage::~MysqlStorage()
{

}

void MysqlStorage::run()
{
    MYSQL mh, *pmh;

    if (::mysql_init(&mh) == NULL) {
        Q_ASSERT(1==2);
    }

    const char *host = "202.108.15.6";// "172.24.149.6";// "202.108.15.6";
    int port = 3306;
    const char *user = "postgres";
    const char *pass = "post.DB#2872";
    const char *dbname = "drupal";
    bool bret;
    int iret;
    StorageItem *item;

retry_conn:
    pmh = ::mysql_real_connect(&mh, host, user, pass, dbname, port, NULL, 0);
    Q_ASSERT(pmh != NULL);
    ::mysql_real_query(&mh, "set names utf8", strlen("set names utf8"));
    qLogx()<<"mysql connected ok";

    while (!this->m_user_stop) {
        while (!this->items.isEmpty()) {
            item = this->items.dequeue();

            bret = this->execute(&mh, item);

            if (!bret) {
                iret = ::mysql_ping(&mh);
                if (iret != 0) {
                    this->items.enqueue(item);
                    qLogx()<<"Reconnect to db.";
                    goto retry_conn;
                }
            }
            delete item;
        }
        iret = qrand() % 60;
        if (iret == 1)
            qLogx()<<"Waiting for next storage task...";

        // qLogx()<<"Waiting for next task 222..."<<iret;
        // this->usleep(50);
        this->m_cond.wait(&this->m_mutex, 50);
        if (this->m_user_stop) {
            qLogx()<<"Friendly stop thread.";
            break;
        }
    }

    qLogx()<<"Disconnect from mysql.";
    ::mysql_close(&mh);

    // this->usleep(50);
}

bool MysqlStorage::addItem(StorageItem *item)
{
    if (item == NULL) {
        qLogx()<<"Null item:";
        return false;
    }
    this->items.enqueue(item);
    this->m_cond.wakeOne();

    return true;
}

bool MysqlStorage::execute(MYSQL *mh, StorageItem *item)
{
    unsigned long iret;
    unsigned long lid = 0;
    char wbuf[1000*1024] = {0};
    char esc_title[255] = {0};
    char esc_author[255] = {0};
    char esc_body[1000*1024] = {0};
    char esc_url[255] = {0};
    char esc_rid[255] = {0};
    char esc_ctime[255] = {0};
    char esc_view_count[255] = {0};
    char esc_comment_count[255] = {0};

    QByteArray u8str;

    u8str = this->u8c->fromUnicode(item->title);
    iret = ::mysql_real_escape_string(mh, esc_title, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->author);
    iret = ::mysql_real_escape_string(mh, esc_author, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->body);
    iret = ::mysql_real_escape_string(mh, esc_body, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->url);
    iret = ::mysql_real_escape_string(mh, esc_url, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->rid);
    iret = ::mysql_real_escape_string(mh, esc_rid, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->ctime);
    iret = ::mysql_real_escape_string(mh, esc_ctime, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->view_count);
    iret = ::mysql_real_escape_string(mh, esc_view_count, u8str.data(), u8str.length());
    u8str = this->u8c->fromUnicode(item->comment_count);
    iret = ::mysql_real_escape_string(mh, esc_comment_count, u8str.data(), u8str.length());


    snprintf(wbuf, sizeof(wbuf)-1,
             "INSERT INTO spider_items_el (rid, referer, title, author, ctime, view_count, comment_count, body) "
             " VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")",
             esc_rid, esc_url, esc_title, esc_author, esc_ctime, esc_view_count, esc_comment_count, esc_body
             );

    qLogx()<<"Execute: "<<wbuf;
    iret = ::mysql_real_query(mh, wbuf, strlen(wbuf));
    if (iret != 0) {
        qLogx()<<iret<<::mysql_error(mh);
        return false;
    } else {
        lid = ::mysql_insert_id(mh);
        qLogx()<<"record id:"<<lid;
    }

    return true;
}

bool MysqlStorage::setStop()
{
    this->m_user_stop = true;
    this->m_cond.wakeOne();

    return true;
}
