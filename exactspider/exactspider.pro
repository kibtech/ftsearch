#-------------------------------------------------
#
# Project created by QtCreator 2011-11-21T18:56:27
#
#-------------------------------------------------

QT       += core gui network webkit

TARGET = exactspider
TEMPLATE = app

# main.cpp mainwindow.cpp
SOURCES += mainwindow.cpp \
        spiderconsole.cpp \
        autonext.cpp \
        extractor.cpp cuextractor.cpp cdextractor.cpp \
        elextractor.cpp \
        extractorfactory.cpp \
        filternetworkaccessmanager.cpp \
        loadstate.cpp \
        simplelog.cpp\
        mysqlstorage.cpp \
    taskmanager.cpp \
    spiderconst.cpp \
    huanqiuextractor.cpp \
    tianyabookextractor.cpp

# mainwindow.h
HEADERS  += mainwindow.h \
        spiderconsole.h \
        autonext.h \
        extractor.h cdextractor.h \
        elextractor.h \
        extractorfactory.h \
        filternetworkaccessmanager.h \
        loadstate.h \
        simplelog.h \
        mysqlstorage.h \
    taskmanager.h \
    spiderconst.h \
    huanqiuextractor.h \
    tianyabookextractor.h

FORMS    += mainwindow.ui \
    simplebrowser.ui

INCLUDEPATH += /usr/local/mysql-5.0.48/include /usr/local/mysql-5.0.50/include
INCLUDEPATH += ../../boost_1_48_0/ ../../boost_1_47_0/ /usr/include/boost /usr/include/boost-1_48/
LIBS += -lmysqlclient -L/usr/local/mysql-5.0.48/lib/mysql -L/usr/local/mysql-5.0.50/lib/mysql

