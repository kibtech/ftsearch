
#include "simplelog.h"

#include "extractor.h"


/*
已完成：
it业界
互联网
社会
財經：
科学
体育
数码

进行中，
电信*3G
  */


QString CUExtractor::getCTime()
{
    QString rstr;

    QString ctime;
    QString tstr, str2;
    QStringList strs, strs2;

    tstr = Extractor::getCTime();

    strs = tstr.split(" ");
    str2 = u8c->toUnicode(QByteArray("："));
    strs2 = strs.at(0).split(str2);
    qLogx()<<strs2;
//    if (strs2.count() >= 2)
//        author = strs.at(0).split(str2).at(1).trimmed();
//    else
//        qLogx()<<"Error: fetch auther error."<<tstr;
    if (strs.count() >= 4)
        ctime = strs.at(2).trimmed() + ' ' + strs.at(3).left(8).trimmed();
    else
        qLogx()<<"Error: fetch ctime error."<<tstr;

    rstr = ctime;
    return rstr;
}

QString CUExtractor::getAuthor()
{
    QString rstr;

    QString author;
    QString tstr, str2;
    QStringList strs, strs2;

    tstr = Extractor::getCTime();

    strs = tstr.split(" ");
    str2 = u8c->toUnicode(QByteArray("："));
    strs2 = strs.at(0).split(str2);
    qLogx()<<strs2;
    if (strs2.count() >= 2)
        author = strs.at(0).split(str2).at(1).trimmed();
    else
        qLogx()<<"Error: fetch auther error."<<tstr;
//    if (strs.count() >= 4)
//        ctime = strs.at(2).trimmed() + ' ' + strs.at(3).left(8).trimmed();
//    else
//        qLogx()<<"Error: fetch ctime error."<<tstr;

    rstr = author;
    return rstr;
}

QString CUExtractor::getRid()
{
    QString rstr;

    QString rid;
    QStringList strs, strs2;
    QString url = this->mwf->url().toString();

    strs = url.split("/");
    strs2 = strs.at(strs.count()-1).split(".");
    rid = strs2.at(0);

    rstr = rid;
    return rstr;
}
