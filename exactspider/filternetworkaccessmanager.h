#ifndef _FILTERNETWORKACCESSMANAGER_H_
#define _FILTERNETWORKACCESSMANAGER_H_

#include <QtCore>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <Qt/QtNetwork>

#include "simplelog.h"

class NullNetworkReply : public QNetworkReply
{
    Q_OBJECT;
public:
    NullNetworkReply() : QNetworkReply() {

    }

    virtual void abort() {

    }

protected:
    virtual qint64 readData(char *data, qint64 maxlen)
    {
        return -1;
    }
};

class FilterNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT;
public:
    FilterNetworkAccessManager(QNetworkAccessManager *manager, QObject *parent = 0)
        : QNetworkAccessManager(parent) {

        this->m_old_manager = manager;
        this->setCache(manager->cache());
        this->setCookieJar(manager->cookieJar());
        this->setProxy(manager->proxy());
        this->setProxyFactory(manager->proxyFactory());

        this->m_img_suffixes[".gif"] = 1;
        this->m_img_suffixes[".jpg"] = 1;
        this->m_img_suffixes[".png"] = 1;
        this->m_img_suffixes[".bmp"] = 1;
        this->m_img_suffixes[".jpeg"] = 1;
        this->m_img_suffixes[".swf"] = 1;
    }

    virtual ~FilterNetworkAccessManager() {

    }

    virtual QNetworkReply *createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
    {
        QNetworkReply *reply = NULL;

        QString req_url = request.url().toString();
        QString req_host = request.url().host();

        if (this->m_img_suffixes.contains(req_url.right(4))
                || this->m_img_suffixes.contains(req_url.right(5))
                // || req_host.endsWith("linezing.com", Qt::CaseInsensitive)
                // || req_host.endsWith("baidu.com", Qt::CaseInsensitive)
                // || req_host.endsWith("doubleclick.net", Qt::CaseInsensitive)
                ) {

            qLogx()<<"Filtered url:"<<req_url;
            return new NullNetworkReply();
            return NULL;
        }

        // qLogx()<<"UNFiltered url:"<<req_url;
        return QNetworkAccessManager::createRequest(op, request, outgoingData);
        return reply;
    }

protected:
    QHash<QString, int> m_img_suffixes;
    QNetworkAccessManager *m_old_manager;
};

#endif /* _FILTERNETWORKACCESSMANAGER_H_ */
