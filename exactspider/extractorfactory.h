#ifndef _EXTRACTORFACTORY_H_
#define _EXTRACTORFACTORY_H_

#include <QtCore>
#include <QtWebKit>

class Extractor;
class CuExtractor;
class CdExtractor;

class ExtractorFactory : public QObject
{
    Q_OBJECT;
public:
    ExtractorFactory() {}
    virtual ~ExtractorFactory() {}

    static Extractor *create(QWebFrame *wf, const QString &url);
};

#endif /* _EXTRACTORFACTORY_H_ */
