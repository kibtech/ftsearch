
#include "simplelog.h"

#include "filternetworkaccessmanager.h"
#include "mysqlstorage.h"
#include "autonext.h"
#include "extractor.h"
#include "cdextractor.h"
#include "extractorfactory.h"
#include "loadstate.h"

#include "taskmanager.h"


#define BLANK_URL "about:blank"

/*
  TODO:
  无界面方式运行。 ------- ok
  加cache
  看是否能指定中文字体
  禁用插件          -------ok
  不加载图片        ------ ok
  继续任务功能
  页面缩放        ------- ok
  重试机制
  更多并发，多个站点一起，那么在一个站点上的抓取就不会太快，但并行效率却高
  去掉VIEW层，全部使用QWebPage和QWebFrame在内存中处理。  --------ok
  内存处理时，支持某种方式的抓图，便于调试。
  乱序下载内容页面功能，不用队列，用数组或者hash，是否能防止服务器察觉？？？

  通用spider模式的设计 + 个别站点url的地址过滤规则。这种设计的结果准备性问题如何保证。

  进化为模拟类人行为自动浏览器
  包括，滚动屏幕，随机选择下一链接,遍历的广度与深度结合，随机开n多个浏览器窗口
  */

TaskManager::TaskManager(QObject *parent) :
    QObject(parent)
{
//    this->m_index_page = new QWebPage();
    this->m_body_page = new QWebPage();

//    QObject::connect(this->m_index_page->mainFrame(), SIGNAL(loadFinished(bool)), this, SLOT(indexPageLoadFinished(bool)));
    QObject::connect(this->m_body_page->mainFrame(), SIGNAL(loadFinished(bool)), this, SLOT(bodyPageLoadFinished(bool)));
    QObject::connect(this->m_body_page, SIGNAL(loadProgress(int)), this, SLOT(bodyPageLoadProgress(int)));
    QObject::connect(this->m_body_page->mainFrame(), SIGNAL(loadStarted()), this, SLOT(bodyPageLoadStarted()));
    QObject::connect(this->m_body_page, SIGNAL(statusBarMessage(QString)),
                     this, SLOT(onStatusBarMessage(QString)));

    // this->ws = QWebSettings::globalSettings();
    this->ws = this->m_body_page->settings();
    this->ws->setAttribute(QWebSettings::AutoLoadImages, false);
    this->ws->setAttribute(QWebSettings::PluginsEnabled, false);
    this->ws->setAttribute(QWebSettings::JavascriptEnabled, false);
    this->ws->setAttribute(QWebSettings::DnsPrefetchEnabled, true);


//    body_view_free = true;
    mystore = new MysqlStorage();
    mystore->start();

//    index_page_count = 0;
    body_page_finish_count = 0;
    total_body_len = 0;
    extract_item_len = 0;
    fetch_speed = 0; // Bytes/sec

    // stat_timer = new QTimer();
    // stat_timer->setInterval(1000);
    // QObject::connect(stat_timer, SIGNAL(timeout()), this, SLOT(onUpdateStat()));

    m_nextor = new AutoNext();

    QObject::connect(&this->m_lazy_finder_timer, SIGNAL(timeout()),
                     this, SLOT(on_body_load_timeout()));
    //    this->m_lazy_finder_timer.start(1000 * 8);

}
TaskManager::~TaskManager()
{
//    delete this->m_index_page;
    delete this->m_body_page;
}

bool TaskManager::start(const QString &start_url)
{
    this->m_start_url = start_url;
//    this->m_index_page->mainFrame()->setUrl(this->m_start_url);
    this->m_body_page->mainFrame()->setUrl(this->m_start_url);
    return true;
}

bool TaskManager::stop()
{

    return true;
}

//QWebPage *TaskManager::get_index_page()
//{
//    return this->m_index_page;
//}
QWebPage *TaskManager::get_body_page()
{
    return this->m_body_page;
}


//bool TaskManager::go_index_page(const QString &url)
//{
//    return true;
//}

//bool TaskManager::stop_index_page(const QString &url)
//{
//    return true;
//}

//bool TaskManager::refresh_index_page(const QString &url)
//{
//    return true;
//}


bool TaskManager::go_body_page(const QString &url)
{
    return true;
}

bool TaskManager::stop_body_page(const QString &url)
{
    return true;
}

bool TaskManager::refresh_body_page(const QString &url)
{
    return true;
}


//void TaskManager::indexPageLoadFinished(bool finished)
//{
//    QWebPage *wp = this->m_index_page;
//    QWebFrame *mwf = wp->mainFrame();// wp->currentFrame();
//    QString url = mwf->url().toString();// this->ui->qwebview->url().toString();
//    int child_frame_count = mwf->childFrames().count();

//    if (!finished) {
//        qLogx()<<"Not really finished index page: "<<url<<child_frame_count;
//    }

//    this->total_body_len += wp->totalBytes();

//    if (this->index_finish_list.contains(url)) {
//        qLogx()<<"duplicated index url:"<<url;
//    } else {
//        this->index_finish_list[url] = 1;
//    }

//    QStringList urls;
//    Extractor *extor = ExtractorFactory::create(mwf, url);
//    Q_ASSERT(extor != NULL);
//    QString newu;

//    urls = extor->getCareBodyPages();
//    qLogx()<<"Got "<<urls.count()<<" body urls";
//    delete extor; extor = NULL;
//    for (int i = 0; i < urls.count(); ++i) {
//        newu = urls.at(i);
//        // qLogx()<<i<<newu;
//        if (newu.split("/").count() < 6) { // TODO, why this.
//            qLogx()<<"Maybe invalid url: omit,"<<newu<<", refrom:"<<url;
//            continue;
//        }

//        this->m_ready_queue.enqueue(QPair<QString, LoadState*>(newu, 0));
//        // this->body_url_queue.enqueue(newu);
//    }


//    // qLogx()<<this->ui->qwebview_2->url().toString();
//    if (urls.count() > 0
//            && (this->m_body_page->mainFrame()->url().toString().isEmpty()
//                || body_view_free == true)) {
//                // || this->ui->qwebview_2->url().toString() == BLANK_URL)) {
//        qLogx()<<"Start body load loop...";
//        int next_delay = qrand() % 6000 + 1000;
//        QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
//    }

//    // parse next index page url
//    //    QString up, uf, ul;
//    //    up = this->m_nextor->getPrevPage(wf);
//    //    uf = this->m_nextor->getFirstPage(wf);
//    //    ul = this->m_nextor->getLastPage(wf);
//    //    qLogx()<<up<<uf<<ul;
//    QString first_page_url = this->m_nextor->getFirstPage(mwf);
//    QString prev_page_url = this->m_nextor->getPrevPage(mwf);
//    QString next_page_url = this->m_nextor->getNextPage(mwf);

//    newu = prev_page_url;
//    if (newu.isEmpty() || newu == first_page_url) {
//        qLogx()<<"No more index page left.";
//    } else {
//        qLogx()<<newu;

//        this->index_url_queue.enqueue(newu);

//        int next_delay = qrand() % 12000 + 1000;
//        QTimer::singleShot(next_delay, this, SLOT(onLoadNextIndex()));
//    }

//}

//void TaskManager::onLoadNextIndex()
//{
//    QString url;

//    if (!this->index_url_queue.isEmpty()) {
//        url = this->index_url_queue.dequeue();
//    }

//    if (!url.isEmpty()) {
//        this->m_index_page->mainFrame()->setUrl(url);
//        // this->ui->comboBox->setEditText(url);
//        // this->ui->qwebview->setUrl(QUrl(url));
//    }
//}

void TaskManager::onLoadNextBody()
{
    QString url;
    QUrl ourl;

//    if (this->faild_list.count() > 0 && qrand() % 3 == 0) {
//        url = this->faild_list.begin().key();
//        body_view_free = false;
//        qLogx()<<"Retry loading faild url:"<<url;
//        this->ui->comboBox_2->setEditText(url);
//        this->ui->qwebview_2->setUrl(url);
//        return;
//    }

    LoadState *ls = NULL;
    // QPair<QString, LoadState*> telem;
    QHash<QString, LoadState*>::iterator fit;

    while (!this->m_ready_queue.isEmpty()) {
        // telem = this->m_ready_queue.dequeue();
        // url = telem.first;
        fit = this->m_ready_queue.begin();
        url = fit.key();
        ls = fit.value();
        this->m_ready_queue.erase(fit);
        ourl = QUrl(url);

        if (!ourl.isValid()) {
            qLogx()<<"Invalid next url, omited:"<<ourl;
        } else if (!url.isEmpty()) {
            if (this->m_body_finish_hash.contains(url)) {
                qLogx()<<"already fetched url, omited:"<<url;
                continue;
            }
//            body_view_free = false;
            if (ls == NULL) {
                ls = new LoadState();
                ls->url = url;
                ls->load_started = true;
                QObject::connect(ls, SIGNAL(load_timeout(QString)), this, SLOT(on_page_load_timeout(QString)));
            }
            if (this->m_body_running_hash.count() > 0) {
                qLogx()<<"Maybe has problem, running queue count > 0:"<<this->m_body_running_hash.count();
                for (QHash<QString, LoadState*>::iterator it = this->m_body_running_hash.begin();
                     it != this->m_body_running_hash.end(); ++it) {
                    if (it.value() != NULL) {
                        delete it.value();
                    }
                }
                this->m_body_running_hash.clear();
            }
            this->m_body_running_hash[url] = ls; // telem.second;
            ls->load_timeout_timer.start();
            qLogx()<<"browser url:"<<ourl<<url;
            this->m_body_page->mainFrame()->setUrl(ourl);
            // this->ui->comboBox_2->setEditText(url);
            // this->ui->qwebview_2->setUrl(url);
            return;
        } else {
            // body_view_free = true;
            // continue;
        }
    }

    if (this->m_ready_queue.isEmpty()) {
//        body_view_free = true;
        return;
    }
}

void TaskManager::bodyPageLoadFinished(bool finished)
{
    QWebPage *wp = this->m_body_page; //this->ui->qwebview_2->page();
    QWebFrame *mwf = wp->mainFrame();
    QWebFrame *cwf = wp->currentFrame();
    QString murl = mwf->url().toString();
    QString curl = cwf->url().toString();
    int child_frame_cound = mwf->childFrames().count();
    LoadState *ols = NULL;

    ols = this->m_body_running_hash.value(murl);

    if (murl == BLANK_URL) {
        return;
    }

    if (!finished) {
//        if (this->faild_list.contains(murl)) {
//            ols = this->faild_list.value(murl);
//        } else {
//            ols = new LoadState();
//            this->faild_list[murl] = ols;
//        }
//        ols->url = murl;
//        ols->retry_times += 1;

        qLogx()<<"Not really finished, retry later."<<finished<<this->m_status_text
              <<wp->totalBytes()<<wp->bytesReceived()
             <<murl<<curl; // <<ols->retry_times;
            // <<this->faild_list.count();

//        {
//            wp->settings()->clearMemoryCaches();
//            int next_delay = qrand() % 3000 + 1000;
//            QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
//        }
//        return;
    }

    if (cwf != mwf) {
        qLogx()<<"Multi frame page found."<<murl<<curl;
    }

    // 从运行队列删除
    if (this->m_body_running_hash.contains(murl)) {
        ols = this->m_body_running_hash.value(murl);
        this->m_body_running_hash.remove(murl);
        if (ols != NULL) {
            delete ols;
            ols = NULL;
        } else {
            qLogx()<<"Uninited LoadState obj,"<<murl;
        }
    }
    // 放到完成队列
    if (this->m_body_finish_hash.contains(murl)) {
        qLogx()<<"1. duplicated body url:"<<murl;
        // goto try_next;
    } else {
        // this->body_finish_list[murl] = 1;
        this->m_body_finish_hash[murl] = ols;
    }

    //// 找到所有的next链接，放入到ready队列中
    Extractor * extor = ExtractorFactory::create(mwf, murl);
    Q_ASSERT(extor != NULL);
    QString alink;
    QStringList alinks = this->m_nextor->getAllLinks(mwf);
    extor->filterredLinks(alinks);
    delete extor; extor = NULL;
    int really_next_links_count = 0;

    for (int i = 0; i < alinks.count(); ++i) {
        alink = alinks.at(i);
        if (this->m_body_running_hash.contains(alink)
                || this->m_body_faild_hash.contains(alink)
                || this->m_body_finish_hash.contains(alink)
                || this->m_ready_queue.contains(alink)) {
            continue;
        } else {
            // 这可能导致重复，有可能已经在这个队列里了
            // this->m_ready_queue.append(QPair<QString, LoadState*>(alink, 0));
            this->m_ready_queue.insert(alink, 0);
            ++really_next_links_count;
        }
    }
    qLogx()<<"Got Next links num:"<<alinks.count()<<really_next_links_count;
    alinks.clear();

    //// 统计数据更新
    this->total_body_len += wp->bytesReceived(); // wp->totalBytes();

    // 内容解析
//    if (this->m_body_finish_hash.contains(murl)) {
//        qLogx()<<"duplicated body url:"<<murl;
//    } else {
        // this->m_body_finish_hash[murl] = ols;

        //////
        qLogx()<<"body load finished:"<<murl<<child_frame_cound;

        bool bret = this->parseBody(mwf);
        if (!bret) {
        }
//    }

    {
try_next:
        int next_delay = qrand() % 3000 + 1000;
        QTimer::singleShot(next_delay, this, SLOT(onLoadNextBody()));
    }
}

void TaskManager::bodyPageLoadProgress(int progress)
{
    QWebPage *wp = this->m_body_page;// this->ui->qwebview_2->page();
    QWebFrame *wf = wp->mainFrame();

    QString url = wf->url().toString();
    // LoadState *state = NULL;

    // if (qrand() % 3 == 1 || progress == 100 || progress == 0)
    //    if (progress % 10 == 0)
    // qLogx()<<progress<<url;

    if (this->m_body_running_hash.contains(url)) {
        this->m_body_running_hash[url]->progress = progress;
    }

    if (progress >= 100) {
        // this->bodyPageLoadFinished(true);
    }
}

void TaskManager::bodyPageLoadStarted()
{
    QWebFrame *wf = static_cast<QWebFrame*>(sender());
    QUrl ourl = wf->url();
    qLogx()<<"Page loading started:"<<ourl;
}

bool TaskManager::parseBody(QWebFrame * frame)
{
    Q_ASSERT(frame != NULL);

    int child_frame_count = frame->childFrames().count();
    QString url = frame->url().toString();
    QString rid;
    QString title;
    QString content;
    QString author;
    QString ctime;
    QString view_count;
    QString comment_count;

    Extractor * extor = ExtractorFactory::create(frame, url);
    Q_ASSERT(extor != NULL);

    rid = extor->getRid();
    title = extor->getTitle();
    content = extor->getContent();
    view_count = extor->getViewCount();
    author = extor->getAuthor();
    ctime = extor->getCTime();
    comment_count = extor->getCommentCount();

    delete extor; extor = NULL;

    qLogx()<<rid<<url;
    qLogx()<<title;
    // qLogx()<<content;
    qLogx()<<"body length:"<<content.length()<<" of (Total/Got) "
          <<frame->page()->totalBytes()<<frame->page()->bytesReceived();
    // qLogx()<<strs;
    qLogx()<<"author:"<<author;
    qLogx()<<"ctime:"<<ctime;
    qLogx()<<"view_count:"<<view_count;
    qLogx()<<"comment_count:"<<comment_count;

    if (title.isEmpty() || content.isEmpty()) {
        // Invalid item?????
        qLogx()<<"An invalid page, can not got expected content."
              <<child_frame_count;// <<frame->toHtml();
        return false;
    }

    StorageItem *item = new StorageItem();
    item->author = author;
    item->body = content;
    item->comment_count = comment_count;
    item->ctime = QString("%1").arg(QDateTime::fromString(ctime, "yyyy-MM-dd hh:mm:ss").toTime_t());
    item->rid = rid;
    item->title = title;
    item->url = url;
    item->view_count = view_count;

//    qLogx()<<"before add item to stoarge.";
    this->mystore->addItem(item);
//    qLogx()<<"end add item to stoarge.";
//    delete item;

    /////
    this->extract_item_len += content.length();

    return true;
}

void TaskManager::onStatusBarMessage(const QString &text)
{
    this->m_status_text = text;
}

void TaskManager::on_body_load_timeout()
{
    qLogx()<<""<<this->m_body_running_hash.count();

    int timeout_interval = 20; // in second
    QDateTime curr_time = QDateTime::currentDateTime();
    LoadState *state;
    QHash<QString, LoadState*>::iterator it;

    for (it = this->m_body_running_hash.begin(); it != this->m_body_running_hash.end(); ++it) {
        state = it.value();
        if (state == NULL) {
            qLogx()<<"load state obj not exists."<<it.key();
            continue;
        }

        if (state->ctime.secsTo(curr_time) > timeout_interval) {
            qLogx()<<"reloading timeout url:"<<it.key();
            // this->m_body_page->mainFrame()->setUrl(QUrl(it.key()));
            this->m_body_page->triggerAction(QWebPage::Stop);
            break;
        }
    }

}

void TaskManager::on_page_load_timeout(const QString &url)
{
    qLogx()<<sender()<<this->m_body_finish_hash.count()<<url;
}

