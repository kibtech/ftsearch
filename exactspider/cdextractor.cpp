
#include "simplelog.h"

#include "cdextractor.h"


QString CdExtractor::getRid()
{
    QString rstr;

    QString rid;
    QStringList strs, strs2;
    QString url = this->mwf->url().toString();

    strs = url.split("/");
    strs2 = strs.at(strs.count()-1).split(".");
    rid = strs2.at(0);

    rstr = rid;
    return rstr;
}

QString CdExtractor::getCTime()
{
    QString rstr;

    QString tstr = Extractor::getCTime();

    if (!tstr.isEmpty()) {
        rstr = tstr + ":05";
    } else {
    }

    return rstr;
}

QString CdExtractor::getAuthor()
{
    QString rstr;

    QString author;
    QString tstr, str2;
    QStringList strs2;

    tstr = Extractor::getAuthor();

    // strs = tstr.split(" ");
    str2 = u8c->toUnicode(QByteArray("："));
    strs2 = tstr.split(str2);
    qLogx()<<strs2;

    if (strs2.count() > 1)
        author = strs2.at(1).trimmed();
    else
        qLogx()<<"Error: fetch author error."<<tstr;

    rstr = author;
    return rstr;
}

QString CdExtractor::getViewCount()
{
    QString rstr;
    char tbuf[16] = {0};

    QString tstr = Extractor::getViewCount();
    char *ptr = tstr.toAscii().data();
    char *ss = ptr;
    char *sd = &tbuf[0];

    while (*ss >= '0' && *ss <= '9') {
        *sd++ = *ss++;
    }
    *sd = '\0';

    if (tbuf[0] == '\0') {
        tbuf[0] = '0';
    }
    rstr = QString(tbuf);

    // qLogx()<<tstr<<rstr;

    return rstr;
}
