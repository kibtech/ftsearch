// autonext.cpp --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-23 20:16:50 -0800
// Version: $Id$
// 

#include "simplelog.h"

#include "autonext.h"

AutoNext::AutoNext()
    : QObject(0)
{
    this->init();
}

AutoNext::~AutoNext()
{
}

bool AutoNext::init()
{
    static const char *u8_next_keys =
            "下一页|下一頁|Next|next|Next >|next >|>|Next »|next »|»|Next >>|next >>|>>|MORE RESULTS|Newer »|Older topics »|next page|Go to the next photo in the stream";
    static const char *u8_prev_keys =
            "上一页|上一頁|Previous|previous|Prev|prev|< Previous|< previous|PREVIOUS RESULTS|< Prev|< prev|<|« Prev|« prev|«|<< Prev|<< prev|<<|« Older|« Newer topics|previous page|Go to the previous photo in the stream";
    static const char *u8_first_keys =
            "1|首页|首頁|First|first|< First|< first|first page|« first|« First";
    static const char *u8_last_keys =
            "尾页|未页|未頁|Last|last|Last >|last >|last page|last »|Last »";

    this->u8c = QTextCodec::codecForName("UTF-8");
    Q_ASSERT(this->u8c != NULL);

    QString sepc = "|";
    QString u16str;
    QStringList keys;

    u16str = u8c->toUnicode(u8_next_keys, strlen(u8_next_keys));
    this->m_next_keys = u16str.split(sepc);

    u16str = u8c->toUnicode(u8_prev_keys, strlen(u8_prev_keys));
    this->m_prev_keys = u16str.split(sepc);

    u16str = u8c->toUnicode(u8_first_keys, strlen(u8_first_keys));
    this->m_first_keys = u16str.split(sepc);

    u16str = u8c->toUnicode(u8_last_keys, strlen(u8_last_keys));
    this->m_last_keys = u16str.split(sepc);

    return true;
}

QString AutoNext::getNextPage(QWebFrame *wf)
{
    Q_ASSERT(wf != NULL);
    QString rurl;

    rurl = this->getPageByCode(wf, PC_NEXT);

    return rurl;
}

QString AutoNext::getPrevPage(QWebFrame *wf)
{
    Q_ASSERT(wf != NULL);
    QString rurl;

    rurl = this->getPageByCode(wf, PC_PREV);

    return rurl;
}

QString AutoNext::getFirstPage(QWebFrame *wf)
{
    Q_ASSERT(wf != NULL);
    QString rurl;

     rurl = this->getPageByCode(wf, PC_FIRST);

    return rurl;
}

QString AutoNext::getLastPage(QWebFrame *wf)
{
    Q_ASSERT(wf != NULL);
    QString rurl;

     rurl = this->getPageByCode(wf, PC_LAST);

    return rurl;
}

QString AutoNext::getPageByCode(QWebFrame *wf, int pcode)
{
    Q_ASSERT(wf != NULL);
    Q_ASSERT(pcode > PC_MIN && pcode < PC_MAX);
    QString rurl;

    QString atitle;
    QWebElement we;
    QWebElementCollection wes;
    bool found = false;
    QStringList keys;

    switch (pcode) {
    case PC_NEXT: keys = this->m_next_keys; break;
    case PC_PREV: keys = this->m_prev_keys; break;
    case PC_FIRST: keys = this->m_first_keys; break;
    case PC_LAST: keys = this->m_last_keys; break;
    default:
        Q_ASSERT(1==2);
        break;
    }

    wes = wf->findAllElements(QString("A"));
    for (int i = wes.count()-1; i >= 0; --i) {
        we = wes.at(i);
        atitle = we.toPlainText().trimmed();
        // qLogx()<<atitle;
        for (int j = 0; j < keys.count(); ++j) {
            if (keys.at(j).compare(atitle, Qt::CaseInsensitive) == 0) {
                rurl = we.attribute(QString("href"));
                found = true;
                break;
            }
        }
        if (found) {
            break;
        }
    }

    if (!rurl.isEmpty() && QUrl(rurl).isRelative()) {
        QUrl rel_url(rurl);
        QUrl burl = wf->baseUrl();

        rurl = burl.resolved(rel_url).toString();
    }

    return rurl;
}

QStringList AutoNext::getAllLinks(QWebFrame *wf)
{
    Q_ASSERT(wf != NULL);
    QStringList rurls;
    QString rurl;

    QString atitle;
    QWebElement we;
    QWebElementCollection wes;

    int outer_link_count = 0;
    int inner_link_count = 0;
    QString chost;
    QUrl burl = wf->url();
    QString bhost = this->root_domain_name(burl.host());
    wes = wf->findAllElements(QString("A"));
    for (int i = wes.count()-1; i >= 0; --i) {
        we = wes.at(i);
        atitle = we.toPlainText().trimmed();
        // qLogx()<<atitle;

        rurl = we.attribute(QString("href"));
        if (rurl.isEmpty()) {
            continue;
        } else if (rurl.startsWith("javascript:", Qt::CaseInsensitive)
                   || rurl.startsWith("mailto:", Qt::CaseInsensitive)) {
            // can not link to this type link
            continue;
        } else {
            QUrl rel_url(rurl);
            QUrl res_url = rel_url;
            if (rel_url.isRelative()) {
                res_url = burl.resolved(rel_url);
                rurl = res_url.toString(QUrl::RemoveFragment);
            }
            if (!res_url.isValid()) {
                qLogx()<<"Invalid link:"<<res_url<<res_url.toString();
                continue;
            }

            chost = res_url.host();
            if (chost.endsWith(bhost, Qt::CaseInsensitive)
                    || bhost.endsWith(chost, Qt::CaseInsensitive)
                    || bhost == chost) {
                rurls << rurl;
                ++inner_link_count;
            } else {
                // qLogx()<<"Outernal link,omited:"<<rurl;
                ++outer_link_count;
            }
        }
    }
    qLogx()<<"Outernal links:"<<outer_link_count<<", inner links:"<<inner_link_count;


    return rurls;
}

QString AutoNext::root_domain_name(const QString &host)
{
    QString root_domain;

    QStringList double_suffixes;
    int rdc = 2;

    double_suffixes << ".com.cn"<<".net.cn"<<".org.cn"<<".com.hk"<<".net.hk";

    for (int i = 0; i < double_suffixes.count(); ++i) {
        if (host.endsWith(double_suffixes.at(i), Qt::CaseInsensitive)) {
            rdc = 3;
            break;
        }
    }

    QStringList doms = host.split(".");

    if (doms.count() >= rdc) {
        for (int i = 0; i < rdc ; ++i) {
            root_domain = "." + doms.at(doms.count() - i - 1) + root_domain;
        }
    } else {
        root_domain = host;
    }

    return root_domain;
}
