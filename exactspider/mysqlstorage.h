#ifndef _MYSQLSTORAGE_H_
#define _MYSQLSTORAGE_H_

#include <mysql/mysql.h>

#include <QtCore>

class StorageItem {
public:

    QString url;
    QString rid;
    QString title;
    QString body;
    QString author;
    QString ctime;
    QString view_count;
    QString comment_count;
};

class MysqlStorage : public QThread
{
    Q_OBJECT;
public:
    MysqlStorage(QObject *parent = 0);
    ~MysqlStorage();

    void run();

    bool execute(MYSQL *mh, StorageItem *item);

    bool addItem(StorageItem *item);
    bool setStop();

private:
    QTextCodec *u8c;
    QQueue<StorageItem*> items;
    QWaitCondition m_cond;
    QMutex m_mutex;
    bool m_user_stop;
};

#endif /* _MYSQLSTORAGE_H_ */
