// autonext.h --- 
// 
// Author: liuguangzhao
// Copyright (C) 2007-2012 liuguangzhao@users.sf.net
// URL: 
// Created: 2011-11-23 20:16:25 -0800
// Version: $Id$
// 
#ifndef _AUTONEXT_H_
#define _AUTONEXT_H_

#include <QtCore>
#include <QtWebKit>

class AutoNext : public QObject
{
    Q_OBJECT;
public:
    AutoNext();
    virtual ~AutoNext();

    enum {PC_MIN = 0, PC_NEXT, PC_PREV, PC_FIRST, PC_LAST, PC_MAX};
    QString getNextPage(QWebFrame *wf);
    QString getPrevPage(QWebFrame *wf);
    QString getFirstPage(QWebFrame *wf);
    QString getLastPage(QWebFrame *wf);

    // really private
    QString getPageByCode(QWebFrame *wf, int pcode);

    //
    QStringList getAllLinks(QWebFrame *wf);

protected:
    virtual bool init();

    virtual QString root_domain_name(const QString &host);
private:
    QTextCodec *u8c;
    // 这些key已经是unicode编码的了。
    QStringList m_next_keys;
    QStringList m_prev_keys;
    QStringList m_first_keys;
    QStringList m_last_keys;

};

#endif /* _AUTONEXT_H_ */
