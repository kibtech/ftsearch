#ifndef _EXTRACTOR_H_
#define _EXTRACTOR_H_

#include <QtCore>
#include <QtWebKit>


class Extractor : public QObject
{
    Q_OBJECT;
public:
    Extractor(QWebFrame * wf);
    virtual ~Extractor();

    virtual bool init() = 0;

    virtual bool isJavascriptEnable() {
        return this->enable_javascript;
    }

    virtual QStringList getCareBodyPages();
    virtual QStringList filterredLinks(QStringList &links);

    virtual QString getTitle();
    virtual QString getCTime();
    virtual QString getAuthor();
    virtual QString getRid();
    virtual QString getContent();
    virtual QString getViewCount();
    virtual QString getCommentCount();

    QString getElementText(const QString &selector);

public:
    QString m_body_url_selector;

    QString m_title_selector;
    QString m_ctime_selector;
    QString m_author_selector;
    QString m_rid_selector;
    QString m_content_select;
    QString m_view_count_selector;
    QString m_comment_count_selector;

protected:
    QWebFrame *mwf;
    QTextCodec *u8c;
    QString selector_sep;
    bool enable_javascript;
};

class CUExtractor : public Extractor
{
    Q_OBJECT;
public:
    CUExtractor(QWebFrame * wf) : Extractor(wf) {
        this->init();
    }
    virtual ~CUExtractor() {

    }

    virtual bool init() {
        this->m_body_url_selector = "a[class=url]";
        /*
    QString slt_title = "td[class=Ftext1]";
    QString slt_content = "div#artibody";
    QString slt_view_count = "span#hits";
    QString slt_ctime = "td[align=center]";
    QString slt_comment_count = "span#comments_top";
         */
        this->m_title_selector = "td[class=Ftext1]";
        this->m_content_select = "div#artibody|td[class=F14]";
        this->m_view_count_selector = "span#hits";
        this->m_comment_count_selector = "span#comments_top";
        this->m_ctime_selector =
                this->m_author_selector = "td[align=center]";

        return true;
    }

    virtual QString getRid();
    virtual QString getCTime();
    virtual QString getAuthor();
};

#endif /* _EXTRACTOR_H_ */
