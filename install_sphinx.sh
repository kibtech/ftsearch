#!/bin/sh

set -x 

####
INSTALL_PREFIX=$HOME/local/sphinx
# this db must be mysql
DB_HOST=127.0.0.1     ###localhost
DB_PORT=3306
DB_USER=root
DB_PASS=
DB_NAME=drupal

######
CPWD=`pwd`
SRC_DIR=$CPWD/sources
DATA_DIR=$CPWD/data
TMP_DIR=$CPWD/tmp
SCRIPT_FILE=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_FILE`

mkdir -pv $TMP_DIR

function error_exit()
{
    lno=$1
    msg=$2

    echo "Error [$lno]: $msg";
    exit $lno;
}
# error_exit $LINENO "test funcccccccc";

function unpackage()
{
    cd $TMP_DIR
    tar xvf $SRC_DIR/coreseek-*.tar.gz

    ## cd coreseek-* || error_exit $LINENO "no source dir found."
    cd $CPWD
}

### install mmseg-*
function install_mmseg()
{
    true;
    cd $TMP_DIR/coreseek-* || error_exit $LINENO "no source dir found."
    cd mmseg-*
    if [ -f /usr/bin/libtoolize.22 ] ; then
	cat bootstrap|sed 's/libtoolize/libtoolize.22/' > bootstrap_fixed.sh
	chmod +x bootstrap_fixed.sh
    else
	cp -va bootstrap bootstrap_fixed.sh
    fi
    ./bootstrap_fixed.sh || error_exit $LINENO "generate configure script error."
    ./configure --prefix=$INSTALL_PREFIX || error_exit $LINENO "mmseg- configure error."
    ( make && make install ) || error_exit $LINENO ""

    cd $CPWD;
}

#### install sphinx-cn
function install_spinx()
{
    true;
    cd $TMP_DIR/coreseek-* || error_exit $LINENO "no source dir found."
    cd csft-*
    ./buildconf.sh
    ./configure --prefix=$INSTALL_PREFIX --with-mysql=/usr/local/mysql* --with-mmseg --with-mmseg-includes=$INSTALL_PREFIX/include/mmseg/ --with-mmseg-libs=$INSTALL_PREFIX/lib/|| error_exit $LINENO "mmseg- configure error."
    ( make && make install ) || error_exit $LINENO ""


    cd $CPWD;
}

echo $LINENO

cd $CPWD;
######## setting sphinx-cn
{
#	| sed s/abcd/${INSPRIX}/     \           # 因为这个变量带路径分隔符号/，导致sed解释出错。在sed中可使用分隔符号:代替/


    if [ -f $INSTALL_PREFIX/etc/csft.conf ] ; then
	cp -v $INSTALL_PREFIX/etc/csft.conf $INSTALL_PREFIX/etc/csft.conf.bak
    fi

    cat etc/csft_mysql.conf \
	| sed s:INSTALL_PREFIX:${INSTALL_PREFIX}:     \
	| sed s/sql_host_place/${DB_HOST}/  \
	| sed s/sql_port_place/${DB_PORT}/  \
	| sed s/sql_user_place/${DB_USER}/  \
	| sed s/sql_pass_place/${DB_PASS}/  \
	| sed s/sql_db_place/${DB_NAME}/    \
	| tee $INSTALL_PREFIX/etc/csft.conf

    # 下载同义词基础库文件。
    wget -O etc/ http://www.coreseek.cn/uploads/csft/3.2/dict/default/thesaurus.lib
}

###### init mysql
function setting_mysql()
{
    /usr/local/mysql-5.0.90/bin/mysql_install_db --defaults-file=$INSTALL_PREFIX/etc/my.conf
    /usr/local/mysql-5.0.90/bin/mysqld_safe --defaults-file=$INSTALL_PREFIX/etc/my.conf
}



echo "All build OK."

#### last
cd $CPWD;


