#!/bin/sh

set -x
CPWD=`pwd`
SCRIPT_FILE=`readlink -f $0`
SCRIPT_DIR=`dirname $SCRIPT_FILE`

cd $SCRIPT_DIR

wget -c http://www.coreseek.cn/uploads/csft/4.0/coreseek-4.1-beta.tar.gz

cd $CPWD

echo "Done"
