
DROP TABLE IF EXISTS spider_items;
CREATE TABLE spider_items (
    nid BIGINT NOT NULL AUTO_INCREMENT,
    rid VARCHAR(255),
    url VARCHAR(255) NOT NULL,
    url_digest VARCHAR(128),
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255),
    body TEXT NOT NULL,
    category VARCHAR(255),
    keywords VARCHAR(255),
    ctime INT(32) NOT NULL DEFAULT 0,
    view_count INT(32) NOT NULL DEFAULT 0,
    comment_count INT(32) NOT NULL DEFAULT 0,
    etag_time INT(32) NOT NULL DEFAULT NOW(),
    etag_hash VARCHAR(255),
    PRIMARY KEY (nid),
    UNIQUE KEY (url)
) ENGINE=InnoDB;

-- rid is the source id, maybe not correct
-- referer source url


-- url collection
DROP TABLE IF EXISTS spider_urls;
CREATE TABLE spider_urls (
    uid BIGINT NOT NULL AUTO_INCREMENT,
    url VARCHAR(255) NOT NULL,
    url_digest VARCHAR(255),
    ctime INT(32) NOT NULL DEFAULT 0,
    PRIMARY KEY (uid),
    UNIQUE KEY (url)
);

