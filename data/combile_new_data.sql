insert into spider_items (rid, referer, title, author, body, ctime, category, keywords, view_count, comment_count)
       select rid, referer, title, author, body, ctime, category, keywords, view_count, comment_count 
              from spider_items_el AS st
                   on duplicate key update rid=st.rid,
                                category=st.category, 
                                keywords=st.keywords;