
DROP TABLE IF EXISTS sph_counter;
CREATE TABLE sph_counter (
    counter_id INT(32) NOT NULL,
    master_max_mtime INT(32),
    delta_current_mtime INT(32),
    delta_max_mtime INT(32),
    unique key(counter_id)
);

INSERT INTO sph_counter VALUES(1, 0, 0, 2147483647);
INSERT INTO sph_counter VALUES(2, 0, 0, 2147483647);
